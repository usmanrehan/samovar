import UIKit
import Realm
import RealmSwift

class RealmString: Object {
    @objc dynamic var stringValue = ""
}



import UIKit
import Realm
import RealmSwift

class GeneralResponse : Object{
    @objc dynamic var status_code : Int = 0
    @objc dynamic var Message : String? = ""
}

class User: Object {
/*{
    "Message": "Success",
    "StatusCode": 200,
    "Result": {
    "Id": 52,
    "UserName": "zaid",
    "EmailAddress": "zaid@gamil.com",
    "Password": "123456",
    "PhoneNumber": "1234567890",
    "Location": null,
    "ProfileImageUrl": null,
    "CityId": 0,
    "IsNotificationOn": true,
    "Token": {
    "access_token": "dWPykAidgByB7xn3rkGetYQraT7TlcH71oQMmsRn69yjPfl2195pEpVJsJoLJxV8mvmGOkFQjNAfWDiZ5PQObm_tmBwSTfk1QUbiCoUs4pXYUqTlf2ucwysG1K8YsWU9zMendNIRxPfWLqbYtnOn2Xm_hRLcPaCR9NfJrdpU1JB8m7tNr0MCtvQR5t8qeruY8Vm4Ta4ma0P-G9JBMylvegTSpWB1aXvXJ7uaQ4MweG1oMS4zK4f-dNzJBW87vnAKd1fwYYSSEYINHQpWpTA3J0L4nkSDzxAfyB3bUaGzHyMgczyPWR3WpiEJqwK6HWxW",
    "token_type": "bearer",
    "expires_in": 86399,
    "userName": null,
    ".issued": null,
    ".expires": null,
    "refresh_token": null
    }
    }
}*/
    
    @objc dynamic var Id = 0
    
    @objc dynamic var UserName : String?
   
    @objc dynamic var EmailAddress : String?
   
    @objc dynamic var PhoneNumber : String?
    
    @objc dynamic var isExternalLogin : Bool = false
    
    
    
    @objc dynamic var Token : token? = token()
    
   
    
    
    
    
    
    
    
    
    override static func primaryKey() -> String? {
        return "Id"
    }
    
    
    
    
}

class basketSettings : Object
{
    
    @objc dynamic var Help : String? = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
   
    @objc dynamic var AboutUs : String? = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    
    @objc dynamic var DeliveryFee  = 0.0
    
    
    @objc dynamic var MinimumOrderPrice  = 0.0
    
    
    @objc dynamic var Currency : String? = ""
        {
        didSet
        {
            if Currency != nil
            {
                Constants.CURRENCY_STRING = Currency!
            }
        }
    }
    
    @objc dynamic var StandardWorkingHours : String? = ""
}


class token : Object
{
    /*{
    "access_token": "dWPykAidgByB7xn3rkGetYQraT7TlcH71oQMmsRn69yjPfl2195pEpVJsJoLJxV8mvmGOkFQjNAfWDiZ5PQObm_tmBwSTfk1QUbiCoUs4pXYUqTlf2ucwysG1K8YsWU9zMendNIRxPfWLqbYtnOn2Xm_hRLcPaCR9NfJrdpU1JB8m7tNr0MCtvQR5t8qeruY8Vm4Ta4ma0P-G9JBMylvegTSpWB1aXvXJ7uaQ4MweG1oMS4zK4f-dNzJBW87vnAKd1fwYYSSEYINHQpWpTA3J0L4nkSDzxAfyB3bUaGzHyMgczyPWR3WpiEJqwK6HWxW",
    "token_type": "bearer",
    "expires_in": 86399,
    "userName": null,
    ".issued": null,
    ".expires": null,
    "refresh_token": null
    }*/
    @objc dynamic var access_token : String? = ""
    
    @objc dynamic var token_type : String? = ""
    
//    @objc dynamic var expires_in : String? = ""
    
    @objc dynamic var refresh_token : String? = ""
}
class UserAddresses : Object
{
    
    @objc dynamic var Id = -1
    
    @objc dynamic var User_ID = -1
    
    @objc dynamic var Country : String? = ""
    
    @objc dynamic var City : String? = ""
    
    @objc dynamic var StreetName : String? = ""
    
    @objc dynamic var Floor : String? = ""
    
    @objc dynamic var Apartment : String? = ""
    
    @objc dynamic var NearestLandmark : String? = ""
    
    @objc dynamic var `Type` = -1
    
    @objc dynamic var IsDeleted = false
    
    @objc dynamic var IsPrimary = false
   
    @objc dynamic var BuildingName : String? = ""
    
    
}

