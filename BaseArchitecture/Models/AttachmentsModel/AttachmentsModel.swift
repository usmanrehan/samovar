//
//  NotificationModel.swift
//
//  Created by Hamza Hasan on 08/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class AttachmentsModel: Object, Mappable {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
    }
    
    @objc dynamic var attachmentId: String?
    @objc dynamic var fileData: Data?    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "attachmentId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
    }
}
