//
//  GetCategoriesModel.swift
//
//  Created by Hamza Hasan on 03/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class GetCategoriesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let categoryId = "category_Id"
    static let title = "title"
    static let imageIcon = "imageIcon"
    static let parentCategory = "parent_category"
  }

  // MARK: Properties
  @objc dynamic var categoryId = 0
  @objc dynamic var title: String? = ""
  @objc dynamic var imageIcon: String? = ""
  @objc dynamic var parentCategory = 0
  @objc dynamic var id: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    categoryId <- map[SerializationKeys.categoryId]
    title <- map[SerializationKeys.title]
    imageIcon <- map[SerializationKeys.imageIcon]
    parentCategory <- map[SerializationKeys.parentCategory]
  }


}
