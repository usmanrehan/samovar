//
//  GetSubCategoriesModel.swift
//
//  Created by Hamza Hasan on 03/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class GetSubCategoriesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productId = "product_id"
    static let arCarpetModel = "ar_carpet_model"
    static let descriptionValue = "description"
    static let quantity = "quantity"
    static let images = "images"
    static let imageIcon = "image_icon"
    static let title = "title"
    static let price = "price"
    static let isFavourite = "is_favourite"
    static let user_favourite = "user_favourite"
  }

  // MARK: Properties
  @objc dynamic var productId = 0
  @objc dynamic var arCarpetModel: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var quantity = 0
  var images = List<ListImages>()
  @objc dynamic var imageIcon: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var price = 0
  @objc dynamic var isFavourite = false
  @objc dynamic var id: String? = ""
    @objc dynamic var user_favourite = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    productId <- map[SerializationKeys.productId]
    arCarpetModel <- map[SerializationKeys.arCarpetModel]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    quantity <- map[SerializationKeys.quantity]
    images <- map[SerializationKeys.images]
    imageIcon <- map[SerializationKeys.imageIcon]
    title <- map[SerializationKeys.title]
    price <- map[SerializationKeys.price]
    isFavourite <- map[SerializationKeys.isFavourite]
    user_favourite <- map[SerializationKeys.user_favourite]

    var images: [String]? = nil
    images <- map["images"]

    images?.forEach { image in
        let value = ListImages()
        value.value = image
        self.images.append(value)
    }
  }


}

class ListImages:Object{
    @objc dynamic var value: String = ""
}
