//
//  UserLogin.swift
//
//  Created by Hamza Hasan on 02/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class UserLogin: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let userTypeId = "user_type_id"
        static let auth_token = "auth_token"
        static let userId = "user_id"
        static let email = "email"
        static let mobile_number = "mobile_number"
        static let user_name = "user_name"
        static let user_type_id = "user_type_id"
        static let isSocialLogin = "isSocialLogin"
    }
    
    // MARK: Properties
    @objc dynamic var userTypeId = 0
    @objc dynamic var auth_token: String? = ""
    @objc dynamic var userId = 0
    @objc dynamic var Id = 0
    @objc dynamic var user_name : String?
    @objc dynamic var email : String?
    @objc dynamic var user_type_id = 0
    @objc dynamic var mobile_number : String?
    @objc dynamic var isExternalLogin : Bool = false
    @objc dynamic var Token : token? = token()
    @objc dynamic var isSocialLogin = 0
    @objc dynamic var allowLocalNotification : Bool = false
    dynamic var arrFiles = List<AttachmentsModel>()
    
    
    
    
    
    
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    public override class func primaryKey() -> String? {
        return "Id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        userTypeId <- map[SerializationKeys.userTypeId]
        auth_token <- map[SerializationKeys.auth_token]
        userId <- map[SerializationKeys.userId]
        user_name <- map[SerializationKeys.user_name]
        email <- map[SerializationKeys.email]
        user_type_id <- map[SerializationKeys.user_type_id]
        mobile_number <- map[SerializationKeys.mobile_number]
        isSocialLogin <- map[SerializationKeys.isSocialLogin]
    }
    
    
}
