//
//  NotificationModel.swift
//
//  Created by Hamza Hasan on 08/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class NotificationModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let id = "id"
    static let body = "body"
    static let isRead = "is_read"
    static let notificationTypeId = "notification_type_id"
    static let referenceId = "reference_id"
    static let date = "date"
    static let title = "title"
    static let type = "type"
  }

  // MARK: Properties
  @objc dynamic var id = 0
  @objc dynamic var body: String? = ""
  @objc dynamic var isRead = false
  @objc dynamic var notificationTypeId = 0
  @objc dynamic var referenceId = 0
  @objc dynamic var date: String? = ""
  @objc dynamic var title: String? = ""
    @objc dynamic var type = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    id <- map[SerializationKeys.id]
    body <- map[SerializationKeys.body]
    isRead <- map[SerializationKeys.isRead]
    notificationTypeId <- map[SerializationKeys.notificationTypeId]
    referenceId <- map[SerializationKeys.referenceId]
    date <- map[SerializationKeys.date]
    title <- map[SerializationKeys.title]
    type <- map[SerializationKeys.type]
  }


}
