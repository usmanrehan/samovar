//
//  SearchCarpets.swift
//
//  Created by Hamza Hasan on 08/06/2020
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class SearchCarpets: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let categoriesResponse = "categories_response"
    static let productsResponse = "products_response"
  }

  // MARK: Properties
  var categoriesResponse = List<GetCategoriesModel>()
  var productsResponse = List<GetSubCategoriesModel>()
  @objc dynamic var id = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    public override class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    categoriesResponse <- (map[SerializationKeys.categoriesResponse], ListTransform<GetCategoriesModel>())
    productsResponse <- (map[SerializationKeys.productsResponse], ListTransform<GetSubCategoriesModel>())
  }


}
