//
//  UIView+Extension.swift
//  BaseArchitecture
//
//  Created by Mac Book on 08/05/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        
        let waterMark = WaterMark.instanceFromNib() as! WaterMark
        waterMark.tag = 1
        waterMark.center = CGPoint(x: self.frame.size.width/2 ,y: self.frame.size.height/2)
        self.addSubview(waterMark)
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let viewWithTag = self.viewWithTag(1) {
            viewWithTag.removeFromSuperview()
        }
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
