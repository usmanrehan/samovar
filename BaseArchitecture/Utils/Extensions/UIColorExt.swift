
import Foundation
import UIKit

extension UIColor {
    static var BADarkGray: UIColor {
        return UIColor(red: 0.2627, green: 0.2588, blue: 0.2588, alpha: 1.0)
    }
    static var BARed: UIColor {
        return UIColor(red: 199.0/255.0, green: 16.0/255.0, blue: 45.0/255.0, alpha: 1.0)
    }
    static var BAApp: UIColor {
        return UIColor(red: 197/255, green: 0/255, blue: 46/255, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
    
    convenience init(hex: Int, alpha: CGFloat) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: alpha)
    }
    static func colorWithHex(hex: Int) -> CGColor {
        return UIColor(hex: hex).cgColor
    }
}
