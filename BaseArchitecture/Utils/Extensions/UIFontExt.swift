
import UIKit
import Device

extension UIFont {
    @discardableResult
    func adjustedPointSize(pointSize size: CGFloat = 0) -> CGFloat {
        let maxW = CGFloat(Device.isPad() ? 768 : 375)
        
        let width = CGFloat(UIScreen.main.bounds.size.width)
        return (width / maxW) * self.pointSize
    }
    
    @discardableResult
    func adjust() -> UIFont {
        let size = adjustedPointSize(pointSize: pointSize)
        
        if let font = UIFont(name: fontName, size: size){
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    
    static func create(fontName name: String = "", fontSize size: CGFloat) -> UIFont{
        let maxW = CGFloat(Device.isPad() ? 768 : 375)
        
        let width = CGFloat(UIScreen.main.bounds.size.width)
        let newPointSize = (width / maxW) * size
        
        return UIFont(name: name, size: newPointSize) ??  UIFont.systemFont(ofSize: newPointSize)
    }
    
}

extension UILabel {
    @discardableResult
    func setFont(fontName name: String? = nil, fontSize size: CGFloat? = nil) -> UIFont {
        let fontName = name ?? font.fontName
        let pointSize = size ?? font.pointSize
        
        let newPointSize = font.adjustedPointSize(pointSize: pointSize)
        
        if let font = UIFont(name: fontName, size: newPointSize){
            self.font = font
        } else {
            self.font = UIFont.systemFont(ofSize: newPointSize)
        }
        
        return self.font!
    }
    func textHeight(withWidth width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.height(withWidth: width, font: font)
    }
    
    func attributedTextHeight(withWidth width: CGFloat) -> CGFloat {
        guard let attributedText = attributedText else {
            return 0
        }
        return attributedText.height(withWidth: width)
    }
}

extension UIButton {
    @discardableResult
    func setFont(fontName name: String? = nil, fontSize size: CGFloat? = nil) -> UIFont {
        let fontName = name ?? titleLabel!.font.fontName
        let pointSize = size ?? titleLabel!.font.pointSize
        
        let newPointSize = titleLabel!.font.adjustedPointSize(pointSize: pointSize)
        
        if let font = UIFont(name: fontName, size: newPointSize){
            self.titleLabel!.font = font
        } else {
            self.titleLabel!.font = UIFont.systemFont(ofSize: newPointSize)
        }
        
        return self.titleLabel!.font!
    }
}

extension UITextView {
    @discardableResult
    func setFont(fontName name: String? = nil, fontSize size: CGFloat? = nil) -> UIFont {
        let fontName = name ?? font!.fontName
        let pointSize = size ?? font!.pointSize
        
        let newPointSize = font!.adjustedPointSize(pointSize: pointSize)
        
        if let font = UIFont(name: fontName, size: newPointSize){
            self.font = font
        } else {
            self.font = UIFont.systemFont(ofSize: newPointSize)
        }
        
        return self.font!
    }
}
extension UITextField {
    @discardableResult
    func setFont(fontName name: String? = nil, fontSize size: CGFloat? = nil) -> UIFont {
        let fontName = name ?? font!.fontName
        let pointSize = size ?? font!.pointSize
        
        let newPointSize = font!.adjustedPointSize(pointSize: pointSize)
        
        if let font = UIFont(name: fontName, size: newPointSize){
            self.font = font
        } else {
            self.font = UIFont.systemFont(ofSize: newPointSize)
        }
        
        return self.font!
    }
}
