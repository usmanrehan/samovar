
import Foundation
//import DGElasticPullToRefresh



extension UITableView{
    //Nib name and reusable identifier should be same
    func dequeueReusableCell(ofType: Any) -> UITableViewCell{
        
        self.register(UINib(nibName: String(describing: ofType), bundle: nil), forCellReuseIdentifier: String(describing: ofType))
        
        self.tableFooterView = UIView()

        return self.dequeueReusableCell(withIdentifier: String(describing: ofType))!
    }
    
    func addPullToRefresh(loaderColor: UIColor,  target: Selector, _ on: Any){
       
        NotificationCenter.default.addObserver(on, selector: target, name: Notification.Name(target.description), object: nil)
        
        let circleView = DGElasticPullToRefreshLoadingViewCircle()
        
        circleView.tintColor = loaderColor
       
        self.dg_addPullToRefreshWithActionHandler({
            NotificationCenter.default.post(name: Notification.Name(target.description), object: nil)
        }, loadingView: circleView)
        
        self.dg_setPullToRefreshBackgroundColor(.clear)
        
        self.dg_setPullToRefreshFillColor(UIColor.BAApp)
    }
    
    func stopRefreshing(){
       
        self.dg_stopLoading()
    
    }
}

extension UITableView {
    //Nib name and reusable identifier should be same
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    func addRefreshControl(_ target: Any, action: Selector, tintColor: UIColor? = nil) {
        refreshControl = UIRefreshControl()
        refreshControl?.backgroundColor = UIColor.clear
        
        if tintColor != nil {
            refreshControl?.tintColor = tintColor!
        }
        refreshControl?.addTarget(target, action: action, for: .valueChanged)
    }
    
    func beginRefreshing() {
        self.refreshControl?.beginRefreshing()
    }
    func endRefreshing() {
        self.refreshControl?.endRefreshing()
    }
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}


