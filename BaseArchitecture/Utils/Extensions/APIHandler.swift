
import Foundation


extension Dictionary where Key == String {
    
    var statusCode: Int?{
        return self["statusCode"] as! Int!
    }
    
    var resultJson: Dictionary<String, Any>?{
        return self["result"] as? Dictionary<String, Any>
    }
    
    var resultJsonArray: [Dictionary<String, Any>]?{
        return self["result"] as? [Dictionary<String, Any>]
    }
    
    
    var resultErrorMessage: String{
        return (self["result"] as! Dictionary<String, Any>)["ErrorMessage"] as! String
    }
}

extension String{
    func showAsError(){
        Utility.showAlert(title: Strings.ERROR.localized, message: self)
    }
    func showAsSuccess(){
        Utility.showAlert(title: Strings.SUCCESS.localized, message: self)
    }
}

extension Int{
    
    var isSuccess: Bool{
        return self == 200 ? true : false
    }
}

enum RESPONSE_STATUS_IS: Int{
    case SUCCESS = 200
    case SERVER_ERROR = 500
}

