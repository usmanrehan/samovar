//
//  NSDictionaryExtension.swift
//  IngicChat
//
//  Created by zaidtayyab on 26/05/2018.
//  Copyright © 2018 zaidtayyab. All rights reserved.
//

import Foundation

extension NSDictionary {
    func boolFor(key: String) -> Bool {
        if let val = self[key] as? Bool {
            return val
        } else if let val = self[key] as? String {
            return val.lowercased() == "true" || val.intValue > 0
        } else if let val = self[key] as? Int {
            return val > 0
        } else if let val = self[key] as? Double {
            return val > 0
        }
        return false
    }
    
    func stringFor(key: String, defaultValue: String = "") -> String {
        if let val = self[key] as? String {
            return val
        } else if let val = self[key] as? Int {
            return String(val)
        } else if let val = self[key] as? Double {
            return String(val)
        } else if let val = self[key] as? Bool {
            return val == true ? "true" : "false"
        }
        return defaultValue
    }
    func doubleFor(key: String) -> Double {
        if let val = self[key] as? Double {
            return val
        } else if let val = self[key] as? String {
            return Double(val) != nil ? Double(val)! : 0
        } else if let val = self[key] as? Int {
            return Double(val)
        } else if let val = self[key] as? Bool {
            return val == true ? 1 : 0
        }
        return 0
    }
    func intFor(key: String) -> Int {
        if let val = self[key] as? Int {
            return val
        } else if let val = self[key] as? String {
            return Int(val) != nil ? Int(val)! : 0
        } else if let val = self[key] as? Double {
            return Int(val)
        } else if let val = self[key] as? Bool {
            return val == true ? 1 : 0
        }
        return 0
    }
    
    func setDefaultFor(key: String, value: Any) {
        if (self.allKeys as? [String])?.contains(key) == true {
            if self[key] is NSNull {
                self.setValue(value, forKey: key)
            }
        } else {
            self.setValue(value, forKey: key)
        }
    }
    
    func makeInt(key: String) {
        if (self.allKeys as? [String])?.contains(key) == true {
            self.setValue(self.intFor(key: key), forKey: key)
        } else {
            self.setValue(0, forKey: key)
        }
    }
    func makeBool(key: String) {
        if (self.allKeys as? [String])?.contains(key) == true {
            self.setValue(self.boolFor(key: key), forKey: key)
        } else {
            self.setValue(false, forKey: key)
        }
    }
}

