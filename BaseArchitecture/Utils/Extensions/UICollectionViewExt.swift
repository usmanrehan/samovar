
import Foundation
import UIKit

extension UICollectionView {
    //Nib name and reusable identifier should be same
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: Bundle.main), forCellWithReuseIdentifier: identifier)
    }
    //Class name, Nib name and reusable identifier should be same
    func registerCell(withReuseIdentifier identifier: String) {
        self.register(UINib(nibName: identifier, bundle: Bundle.main), forCellWithReuseIdentifier: identifier)
    }
    //Nib name and reusable identifier should be same
    func registerHeader(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: identifier)
    }
    
    func addRefreshControl(_ target: Any, action: Selector, tintColor: UIColor? = nil) {
        refreshControl = UIRefreshControl()
        refreshControl?.backgroundColor = UIColor.clear
        
        if tintColor != nil {
            refreshControl?.tintColor = tintColor!
        }
        refreshControl?.addTarget(target, action: action, for: .valueChanged)
    }
    
    func beginRefreshing() {
        self.refreshControl?.beginRefreshing()
    }
    func endRefreshing() {
        self.refreshControl?.endRefreshing()
    }
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Poppins-Regular", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        
    }
    
    func restore() {
        self.backgroundView = nil
        
    }
}
