import UIKit
extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String, size: Int, color: UIColor) -> NSMutableAttributedString {
        let attrs = [NSAttributedStringKey.font : UIFont(name: "Poppins-Regular", size: CGFloat(size))!,NSAttributedStringKey.foregroundColor: color]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func regular(_ text:String, size: Int, color: UIColor)->NSMutableAttributedString {
        let attrs = [NSAttributedStringKey.font : UIFont(name: "Poppins-Regular", size: CGFloat(size))!,NSAttributedStringKey.foregroundColor: color]
        let normalString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normalString)
        return self
    }
    @discardableResult func light(_ text:String, size: Int, color: UIColor)->NSMutableAttributedString {
        let attrs = [NSAttributedStringKey.font : UIFont(name: "Poppins-Light", size: CGFloat(size))!,NSAttributedStringKey.foregroundColor: color]
        let normalString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normalString)
        return self
    }
    @discardableResult func medium(_ text:String, size: Int, color: UIColor)->NSMutableAttributedString {
        let attrs = [NSAttributedStringKey.font : UIFont(name: "Poppins-Medium", size: CGFloat(size))!,NSAttributedStringKey.foregroundColor: color]
        let normalString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normalString)
        return self
    }
    @discardableResult func semiBold(_ text:String, size: Int, color: UIColor)->NSMutableAttributedString {
        let attrs = [NSAttributedStringKey.font : UIFont(name: "Poppins-SemiBold", size: CGFloat(size))!,NSAttributedStringKey.foregroundColor: color]
        let normalString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(normalString)
        return self
    }
}



