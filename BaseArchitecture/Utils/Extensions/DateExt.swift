
import Foundation


extension Date {
    func stringValue(format: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT"), isLowercasedAMPM: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        if isLowercasedAMPM {
            dateFormatter.amSymbol = "am"
            dateFormatter.pmSymbol = "pm"
        }
        
        return dateFormatter.string(from: self)
    }
    func age() -> Int {
        let now = Date()
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: self, to: now)
        let age = ageComponents.year
        return age ?? 0
    }
}
