
import Foundation
import UIKit

extension UILabel {
    func apply(attributesOn subString:String!, font:UIFont?, color:UIColor?) {
        if let range = self.text?.range(of: subString) {
            if let text = self.attributedText {
                let attr = NSMutableAttributedString(attributedString: text)
                
                let start = text.string.distance(from: text.string.startIndex, to: range.lowerBound)
                let end = text.string.distance(from: text.string.startIndex, to: range.upperBound)
                let length = end - start
                
                var attributes = [NSAttributedStringKey:Any]()
                if font != nil { attributes[.font] = font }
                if color != nil { attributes[.foregroundColor] = color }
                
                attr.addAttributes(attributes, range: NSMakeRange(start, length))
                self.attributedText = attr
            }
        }
    }
    var isTruncated: Bool {
        
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}
