
import CoreLocation


extension CLPlacemark {
    func getAddress() -> String {
        var address = ""
        if let addressDict = addressDictionary as? [String: Any] {
            if let city = addressDict["City"] as? String, !city.isEmpty {
                address = city
            }
            if let state = addressDict["State"] as? String, !state.isEmpty {
                address = address == "" ? state : (address + ", \(state)")
            }
            if let country = addressDict["Country"] as? String, !country.isEmpty {
                address = address == "" ? country : (address + ", \(country)")
            }
        }
        return address
    }
}
