
import UIKit

extension UIButton {
    func addAttibutes(withUnderline value:Int, titleFont font:UIFont, titleColor color: UIColor) {
        guard let currentTitle = currentTitle else { return   }
        
        let attributes: [NSAttributedStringKey: Any] = [.underlineStyle : value, .font : font, .foregroundColor: color]
        let attributedTitle = NSAttributedString(string: currentTitle, attributes: attributes)
        
        setAttributedTitle(attributedTitle, for: .normal)
    }
}
