
import UIKit

extension Double {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    func addTwoZeros() -> String {
        var quan = 0.00
        quan += self
        return  String(format: "%.2f", quan)
    }
    
}
