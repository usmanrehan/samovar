
import UIKit

extension UIWindow {
    func setRootViewController(_ controller: UIViewController, animationType: AnimationType) {
        if animationType == .flip {
            UIApplication.shared.isStatusBarHidden = true
            UIView.transition(with: self, duration: 0.75, options: .transitionFlipFromLeft, animations: {
                self.rootViewController = controller
            }, completion: { completed in
                UIApplication.shared.isStatusBarHidden = false
            })
        } else {
            let snapshot:UIView = (self.snapshotView(afterScreenUpdates: true))!
            controller.view.addSubview(snapshot);
            self.rootViewController = controller;
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0;
                snapshot.layer.transform = animationType == .fadeOut ? CATransform3DMakeScale(1.25, 1.25, 1.25): CATransform3DMakeScale(0.75, 0.75, 0.75)
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview();
            })
        }
    }
}
