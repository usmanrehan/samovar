
import UIKit

extension UIImage {
    func compressTo(size:CGFloat, quality:CGFloat) -> Data {
        return compressTo(width: size, height: size, quality: quality)
    }
    
    func compressTo(width:CGFloat, height:CGFloat, quality:CGFloat) -> Data {
        return UIImageJPEGRepresentation(resizeTo(width: width, height: height), quality)!
    }
    func resizeTo(size:CGFloat) -> UIImage {
        return resizeTo(width: size, height: size)
    }
    func resizeTo(width:CGFloat, height:CGFloat) -> UIImage {
        var actualHeight = self.size.height
        var actualWidth = self.size.width
        var imgRatio = actualWidth / actualHeight
        let maxRatio = width / height
        
        
        if actualHeight > height || actualWidth > width {
            if imgRatio < maxRatio {
                //adjust width according to height
                imgRatio = height / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = height
            }
            else if imgRatio > maxRatio {
                //adjust height according to width
                imgRatio = width / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = width
            }
            else {
                actualHeight = height
                actualWidth = width
            }
        }
        
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        
        
        if let img = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return img
        } else {
            return self
        }
    }
    
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x:0, y:0, width:self.size.width, height:self.size.height)
        context.clip(to: rect, mask: self.cgImage!)
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func transform(withNewColor color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.normal)
        
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        
        color.setFill()
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

