
import UIKit
//import Crashlytics

class Logger {
    static func logException(error:Error) {
        #if DEBUG
            print("Bolwala - Exception -> \(error.localizedDescription)")
        #endif
        
//        Crashlytics.sharedInstance().recordError(error)
    }
    
    static func logError(value:Any) {
        #if DEBUG
            print("Bolwala - Error -> \(value)")
        #endif
    }
    
    static func logWarning(value:Any) {
        #if DEBUG
            print("Bolwala - Warning -> \(value)")
        #endif
    }
    
    static func logInfo(value:Any) {
        #if DEBUG
            print("Bolwala - Info -> \(value)")
        #endif
    }
}
