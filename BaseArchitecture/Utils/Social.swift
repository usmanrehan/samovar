//
//  Social.swift
//  SocialLogin
//
//  Created by zuhairhussain on 13/02/2018.
//  Copyright © 2018 zuhairhussain. All rights reserved.
//

import UIKit
//import FacebookLogin
//import FacebookCore
import GoogleSignIn

class Social: NSObject{//, GIDSignInDelegate, GIDSignInUIDelegate {
    
    static let shared = Social()
    private override init() {}
    
    private var googleCompletion: ((_ status: Bool, _ message: String, _ data: UserSocialData?) -> Void)?
    private var presentedController: UIViewController?
    
    func loginWithFacebook(_ fromController: UIViewController, completion: @escaping (_ status: Bool, _ message: String, _ data: UserSocialData?) -> Void) {
//        let loginManager = LoginManager()
//        loginManager.logIn(readPermissions: [ReadPermission.publicProfile, ReadPermission.email], viewController: fromController) { (result) in
//            switch result {
//            case .failed(let error):
//                completion(false, error.localizedDescription, nil)
//            case .cancelled:
//                completion(false, "User cancelled the process", nil)
//            case .success( _, _, let token):
//
//                self.getFacebookProfile(token: token, completion: completion)
//            }
//        }
    }
    
//    func loginWithGoogle(_ fromController: UIViewController, completion: @escaping (_ status: Bool, _ message: String, _ data: UserSocialData?) -> Void) {
//        GIDSignIn.sharedInstance().signOut()
//        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
//        presentedController = fromController
//        googleCompletion = completion
//        GIDSignIn.sharedInstance().signIn()
//    }
    
    
//    private func getFacebookProfile(token: AccessToken, completion: @escaping (_ status: Bool, _ message: String, _ data: UserSocialData?) -> Void) {
//        GraphRequest(graphPath: "/me", parameters: ["fields": "picture, email,first_name,last_name, name"], accessToken: token, httpMethod: GraphRequestHTTPMethod.GET, apiVersion: GraphAPIVersion.defaultVersion).start { (httpURLResponse, requestResult) in
//            switch requestResult {
//            case .failed(let error):
//                completion(false, error.localizedDescription, nil)
//                break
//            case .success(let graphResponse):
//                if let responseDictionary = graphResponse.dictionaryValue {
//                    let sd = UserSocialData(fbData: responseDictionary)
//                    sd.token = token.authenticationToken
//
//                    completion(true, "",sd)
//                } else {
//                    completion(false, "Response object is nil", nil)
//                }
//            }
//        }
//    }
    
    
    // MARK: - Google Sign In Delegates
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            let userSocialData = UserSocialData()
            userSocialData.name = user.profile.name
            userSocialData.email = user.profile.email
            
            userSocialData.token = user.authentication.idToken
            userSocialData.id = user.userID
//            self.googleCompletion?(true, "", userSocialData)
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        //self.googleCompletion?(true, error.localizedDescription, nil)
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
//        presentedController?.present(viewController, animated: true, completion: nil)
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

class UserSocialData {
    var name = ""
    var email = ""
    var token = ""
    var id = ""
    init() {}
    init(fbData: [String: Any]) {
        
        self.name = fbData["name"] as? String != nil ? fbData["name"] as! String : ""
        self.email = fbData["email"] as? String != nil ? fbData["email"] as! String : ""
        self.id = fbData["id"] as? String != nil ? fbData["id"] as! String : ""
    }
    
}
