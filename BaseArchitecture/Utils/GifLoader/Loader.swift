//
//  Loader.swift
//  BaseArchitecture
//
//  Created by Abdul  Karim Khan on 05/05/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class Loader: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var viewContainer : UIView!
        
        override init(frame:CGRect) {
            super.init(frame: frame)
            loadViewFromNib()
            playGif()
            
        }
        
        required init(coder aCoder: NSCoder) {
            super.init(coder: aCoder)!
            loadViewFromNib()
        }
        func loadViewFromNib() {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: "Loader", bundle: bundle)
            let view =  nib.instantiate(withOwner: self, options: nil).first as! UIView
            view.frame = bounds
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //  view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1);
            self.addSubview(view);
            
        }
        
        
        private func playGif(){
            let jeremyGif = UIImage.gifImageWithName("Loader")
            let imageView = UIImageView(image: jeremyGif)
            let x = self.frame.width / 2
            let y = self.frame.height / 2
            let width = self.frame.width / 2
            let height = self.frame.height / 2
            imageView.frame = CGRect(x: 0, y: 0, width: width, height: height)
            imageView.contentMode = .scaleAspectFit
            imageView.center = CGPoint(x: x,y: y)
            self.viewContainer.addSubview(imageView)
        }

}
