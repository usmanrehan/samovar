
import UIKit

enum FontName: String {
    
    case ralewayBold = "Raleway-Bold"
    case ralewayBoldItalic = "Raleway-BoldItalic"
    case ralewayExtraBold = "Raleway-ExtraBold"
    case ralewayExtraBoldItalic = "Raleway-ExtraBoldItalic"
    case ralewayExtraLight = "Raleway-ExtraLight"
    case ralewayExtraLightItalic = "Raleway-ExtraLightItalic"
    case ralewayHeavy = "Raleway-Heavy"
    case ralewayItalic = "Raleway-Italic"
    case ralewayLight = "Raleway-Light"
    case ralewayLightItalic = "Raleway-LightItalic"
    case ralewayMedium = "Raleway-Medium"
    case ralewayMediumItalic = "Raleway-MediumItalic"
    case ralewayRegular = "Raleway-Regular"
    case ralewaySemiBold = "Raleway-SemiBold"
    case ralewaySemiBoldItalic = "Raleway-SemiBoldItalic"
    case ralewayThin = "Raleway-Thin"
    case ralewayThinItalic = "Raleway-ThinItalic"
    case poppinsRegular = "Poppins-Regular"
    case poppinsLight  = "Poppins-Light"
    case poppinsBold = "Poppins-Bold"
    case poppinsMedium = "Poppins-Medium"
    case semiBold = "Poppins-SemiBold"
    /*
    case robotoBlack = "Roboto-Black"
    case robotoBlackItalic = "Roboto-BlackItalic"
    case robotoBold = "Roboto-Bold"
    case robotoBoldItalic = "Roboto-BoldItalic"
    case robotoItalic = "Roboto-Italic"
    case robotoLight = "Roboto-Light"
    case robotoLightItalic = "Roboto-LightItalic"
    case robotoMedium = "Roboto-Medium"
    case robotoMediumItalic = "Roboto-MediumItalic"
    case robotoRegular = "Roboto-Regular"
    case robotoThin = "Roboto-Thin"
    case robotoThinItalic = "Roboto-ThinItalic"
    */
    
    @discardableResult
    func font(withSize size: CGFloat? = nil) -> UIFont {
        let s = size ?? 9
        return UIFont(name: self.rawValue, size: s) ?? UIFont.systemFont(ofSize: s)
    }
    
    @discardableResult
    func relativeFont(withSize size: CGFloat) -> UIFont {
        let s = size * Constants.wRatio
        return UIFont(name: self.rawValue, size: s) ?? UIFont.systemFont(ofSize: s)
    }
}
