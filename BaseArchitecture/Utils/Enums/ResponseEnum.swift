
import Foundation

enum Response : Int{
    case success = 200
    case serverError = 500
    case undefined = 0
}
