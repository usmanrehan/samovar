
import Foundation

enum SocialLoginStatus: Int {
    case canceled = 0
    case success
    case error
}
