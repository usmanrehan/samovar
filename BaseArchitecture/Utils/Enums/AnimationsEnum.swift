
import Foundation

// MARK: - Animations
enum AnimationType {
    case fadeIn, fadeOut, flip
}

enum TransitionType {
    case fade, moveIn, push, reveal, flip, box
}
