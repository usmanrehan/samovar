
import UIKit

enum Strings: String {
    //MARK:- Properties Text
    
    case next = "next"
    case finish = "finish"
    case skip = "skip"
    case error = "error"
    case submit = "submit"
    case loading = "loading"
    case cancel = "cancel"
    case settings = "settings"
    
    case homeTitle = "home_title"
    case exploreTitle = "explore_title"
    case hashtagTitle = "hashtag_title"
    case rewardsTitle = "rewards_title"
    case profileTitle = "profile_title"
    
    case welcomeTo = "welcome_to"
    case pakistanBiggest = "pakistan_biggest"
    case applicationBolwala = "application_bolwala"
    case dontWorry = "dont_worry"
    case bolwalaBrings = "bolwala_brings"
    case discountOnEverything = "discount_on_verything"
    case shareBolwalaFriends = "share_bolwala_friends"
    case earnReward = "earn_reward"
    
    case emailMobileNo = "email_mobile_no"
    case password = "password"
    case forgotPassword = "forgot_pass"
    
    case emailAddress = "email_address"
    case passwordResetMessage = "password_reset_msg"
    
    case resetCodeViaEmailMsg = "reset_code_via_email_msg"
    case resetCodeViaSmsMsg = "reset_code_via_sms_msg"
    case resetCodeSmsTag = "reset_code_sms_tag"
    case expireCodeMesage = "expire_code_msg"
    case resendCode = "resend_code"
    case countDownMessage = "countdown_msg"
    case countDownTAG = "TIME"
    
    case resetPassSuccessMsg = "reset_pass_success_msg"
    case resetPassInfoMsg = "reset_pass_info_msg"
    case login = "login"
    
    case joinUsWith = "join_us_with"
    case facebook = "facebook"
    case email = "emaail"
    case existingAccount = "existing_account"
    case signInNow = "signin_now"
    
    case currentLocation = "current_location"
    case lastSearched = "last_searched"
    case newLocation = "new_location"
    
    case yourLocation = "your_location"
    case selectLocation = "select_location"
    case searchLocation = "search_location"
    case searchNearLocation = "search_near_location"
    case searchBolwala = "search_bolwala"
    
    case trending = "trending"
    case featured = "featured"
    case recent = "recent"
    
    //MARK:- Fields Validation Messages
    case validateNumber = "valid_number"
    case matchPassword = "match_password"
    case minPassLength = "min_pass_length"
    
    //MARK:- Error Messages
    case noNetwork = "no_network"
    case timeOut = "time_out"
    case errorOccured = "error_occured"
    case badRequest = "bad_request"
    case locationDenied = "location_denied"
    case locationDeniedMsg = "location_denied_msg"
    
    var localized: String {
        return self.rawValue.localized()
    }
    
    case ALERT = "Alert"
    case SAMOVAR = "Samovar Carpets"
    case ERROR = "Error"
    case UNKNOWN_ERROR = "Unknown error"
    case YES = "Yes"
    case NO = "No"
    case OK = "Ok"
    case SUCCESS = "Success"
    case Confirmation = "Confirmation"
    case DOWNLOADING_MODEL = "Downloading Model"
    case INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE = "Internet connection seems to be offline"
    case CATEGORIES = "CATEGORIES"
    case SOMETHING_WENT_WRONG = "Something went wrong. Please try again later."
    case RESET_PASSWORD = "RESET PASSWORD"
    case FIELD_REQUIRED = "This field is required"
    case PASSWORD_TOO_SHORT = "This password is too short"
    case PWD_NOT_MATCH = "Passwords do not match."
    case PWD_CHANGED_SUCCESS = "Password changed successfully"
    case ENTERED_CODE_INVALID = "Entered code is invalid"
    case EMAIL_VERIFICATION = "EMAIL VERIFICATION"
    case CODE_SENT_SUCCESS = "Code Sent Successfully"
    case SAVED_IMAGES = "SAVED IMAGES"
    case SETTINGS = "SETTINGS"
    case LOGIN_TO_CONTINUE = "Please login to continue"
}
