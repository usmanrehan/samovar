import Foundation
import UIKit
import MapKit
import AVFoundation
import Alamofire
import NVActivityIndicatorView


open class Utility {
    
    static let main = Utility()
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
        
    }
    static func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    static func topViewController(base: UIViewController? = (AppDelegate.shared).window?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    static func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Strings.OK.localized, style: .default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    static func showAlert(title:String?, message:String?, buttonTitle: String? = "Ok") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(buttonTitle!, comment: ""), style: .default) { _ in })
        Utility.topViewController()?.present(alert, animated: true)
    }
    static func showAlertYesNo(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Return", style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        controller.present(alertController, animated: true, completion: nil)
        
    }
    static func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
          
          let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

          alertController.addAction(UIAlertAction(title: "Share", style: .default){ (alertActionYES) in
              completionHandler(alertActionYES, nil)
          })
          
          alertController.addAction(UIAlertAction(title: "Ok", style: .cancel){ (alertActionNO) in
              completionHandler(nil, alertActionNO)
          })
          controller.present(alertController, animated: true, completion: nil)
          
      }
    static func showGeneralAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Login Now", style: .default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "Later", style: .cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        controller.present(alertController, animated: true, completion: nil)
        
    }
    static func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    static func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
 
    static func emptyTableViewMessage(message:String,image: String? = nil, tableView: UITableView) {
        
        let emptyView = EmptyTableViewBackgroundView.instanceFromNib()
        if let _ = image{
            emptyView.imageView.image = UIImage(named: image!)
        }
        emptyView.messageLabel.text = message
        emptyView.noResultLabel.text = message
        tableView.backgroundView = emptyView
        tableView.separatorStyle = .none
    }
    
    static func emptycollectionViewMessage(message:String, image: String? = nil, collectionView: UICollectionView) {
        let emptyView = EmptyTableViewBackgroundView.instanceFromNib()
        if let _ = image{
            emptyView.imageView.image = UIImage(named: image!)
        }
        emptyView.messageLabel.text = message
        emptyView.backgroundColor = .clear
        collectionView.backgroundView = emptyView
    }
    
    
    
    class func utcToLocal(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: dateFormatter.string(from: date))
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "hh:mm a"
        
        return dateFormatter.string(from: dt!)
        
        
    }
    
    static func getStarImage(_ starNumber: Double, _  rating: Double) -> UIImage {
        
        if round(rating) >= starNumber {
            return #imageLiteral(resourceName: "rate_yellowstar")
        } else {
            return #imageLiteral(resourceName: "rate_whitestar")
        }
    }
    
    static func timeParser(_ str: String!) ->String{
        
        if let _ =  str{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dater = dateFormatter.date(from: "2017-08-11 "+str)
            
            dateFormatter.dateFormat = "h:mm a"
            let dateString = dateFormatter.string(from: dater!)
            return dateString.lowercased()
        }
        else{
            return ""
        }
        
    }
    static func getErrorMessage(_ result: Dictionary<String,AnyObject>) -> String {
        return result["message_to_show"] as? String ?? ""
        
    }
    static func getErrorMessage1(_ result: Dictionary<String,AnyObject>) -> String {
        return result["error_internal"] as? String ?? ""
        
    }
    static func dateFormatterWithFormat(_ str: String!, withFormat: String) -> String{
        if let _ = str{

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dater = dateFormatter.date(from: str)
            
            dateFormatter.dateFormat = withFormat
            let dateString = dateFormatter.string(from: dater!)
            return dateString.lowercased()
        }
        else{
            return ""
        }
        
    }
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = dateFormat
            let date = dateFormatter.date(from: dateStr)
            dateFormatter.dateFormat = formatteddate
    //        dateFormatter.timeZone = TimeZone.current
    //        dateFormatter.locale = Locale.current
    //        if LanguageManager.sharedInstance.getSelectedLocale() == "en"{
    //            dateFormatter.locale = Locale(identifier: "en_US")
    //        }
    //        else{
    //            dateFormatter.locale = Locale(identifier: "ar_SA")
    //        }
            return dateFormatter.string(from: date ?? Date())
        }
    
    static func getAddressFromPlacemark(placemarks: CLPlacemark) -> String{
        return (placemarks.addressDictionary?["FormattedAddressLines"] as! NSArray).componentsJoined(by: ", ")
        
        
    }
    static func stringToDate (_ str: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        let date = dateFormatter.date(from: str)
        return date!
    }
    

    
    
    static func applyBlurEffectToView(toView: UIView) -> UIView? {
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            toView.backgroundColor = UIColor.clear
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = toView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurEffectView.alpha = 0.85
            toView.addSubview(blurEffectView)
            
            return blurEffectView
        } else {
            toView.backgroundColor = UIColor.black
            return nil
        }
    }
    
    
    static func toggleNotification(_ toggle: Bool){
//        let parameters: Parameters = [
//            "UserId": AppStateManager.sharedInstance.loggedInUser.Id.string,
//            "On": toggle.string,
//            "SignInType": AppStateManager.sharedInstance.loggedInUser.SignInType.string
//        ]
//
//        let successClosure: DefaultAPISuccessClosure = {
//            (result) in
//
//            print(result)
//            if (result.statusCode?.isSuccess)! {
//                print("- Notificaiton " + toggle.string)
//            }
//            else
//            {
//                (result["Message"] as! String).showAsError()
//
//            }
//        }
//        let failureClosure: DefaultAPIFailureClosure = {
//            (error) in
//            print(error)
//        }

//        APIManager.sharedInstance.toggleNotification(parameters: parameters, success: successClosure, failure: failureClosure)
    }






    static func registerPushNotification(device_apn: String){
        /*let parameters: Parameters = [
            "DeviceName": "iPhone",
            "UDID": UIDevice.current.identifierForVendor!.uuidString,
            "IsAndroidPlatform": "false",
            "IsPlayStore": "false",
            "User_Id": AppStateManager.sharedInstance.loggedInUser.Id.string,
            "IsProduction": false,
            "AuthToken": device_apn,
            "SignInType": AppStateManager.sharedInstance.loggedInUser.SignInType.string
        ]

        let successClosure: DefaultAPISuccessClosure = {
            (result) in

            if (result.statusCode?.isSuccess)!
            {
                print("-> Push notification registered SUCCESSFULLY")
                let jsonResult = result.resultJson
                let deviceId = jsonResult!["Id"] as! Int
                UserDefaults.standard.setValue(deviceId, forKey: "device_id")
            }


        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            print("-> Push notification registered FAILED")

            if(error.code == -1009)
            {
                print("No Internet")
            }
        }


        if AppStateManager.sharedInstance.loggedInUser.IsNotificationsOn
        {
//            API.shared.authenticateUserWith(email: <#T##String#>, password: <#T##String#>, success: <#T##DefaultAPISuccessClosure##DefaultAPISuccessClosure##(Dictionary<String, AnyObject>) -> Void#>, failure: <#T##DefaultAPIFailureClosure##DefaultAPIFailureClosure##(NSError) -> Void#>, errorPopup: <#T##Bool#>)
//            API.shared.authenticateUserWith(email: <#T##String#>, password: <#T##String#>, success: <#T##DefaultAPISuccessClosure##DefaultAPISuccessClosure##(Dictionary<String, AnyObject>) -> Void#>, failure: <#T##DefaultAPIFailureClosure##DefaultAPIFailureClosure##(NSError) -> Void#>)
//            APIManager.sharedInstance.registerPushNotification(parameters: parameters, success:successClosure , failure: failureClosure)

        }*/

    }
    
    static func markUserInactive()
    {
        var deviceId = "-1"
        if UserDefaults.standard.object(forKey: "device_id") != nil
        {
            deviceId = ( UserDefaults.standard.object(forKey: "device_id") as! Int).description
            
        }
        let parameters: Parameters = [
            "UserId": AppStateManager.sharedInstance.loggedInUser.Id.string ,
            "DeviceId" : deviceId.description
        ]
        
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            if (result.statusCode?.isSuccess)!{
                
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            
            if(error.code == -1009)
            {
            }
        }
        
//        APIManager.sharedInstance.markDeviceInactive(parameters: parameters, success:successClosure , failure: failureClosure)
    }
    
    
  
    
    static func markNotificationRead(notificationId: String)
    {
        let parameters: Parameters = [
            "NotificationId": notificationId
        ]
        
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            
            if (result.statusCode?.isSuccess)!
            {
                print("- Marked Notification as read.")
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            
            if(error.code == -1009)
            {
                // No internet
            }
        }
        
//        APIManager.sharedInstance.markNotificationRead(parameters: parameters, success:successClosure , failure: failureClosure)
    }
    


    
    
    
    
//    static func startLoading() {
//        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 30, width: 150, height: 100))
//        activityIndicator.backgroundColor = UIColor.clear
//        activityIndicator.layer.cornerRadius = 6
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.activityIndicatorViewStyle = .whiteLarge
//        activityIndicator.startAnimating()
//
//
//
//
//
//
//        let strLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
//        let messageFrame = UIView()
//        strLabel.text = NSLocalizedString("Loading", comment: "")
//        strLabel.textColor = UIColor.white
//        strLabel.textAlignment = .center
//        messageFrame.layer.cornerRadius = 15
//        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
//        messageFrame.frame = CGRect(x: 0, y: 0, width: 150, height: 130)
//        var viewCenter = (Utility.topViewController()?.view.center)!
//        viewCenter.y = (viewCenter.y - messageFrame.frame.size.height)
//        messageFrame.center = viewCenter
//
//
//        messageFrame.tag = 100
//
//        messageFrame.addSubview(activityIndicator)
//        messageFrame.addSubview(strLabel)
//
//
//
//        for subview in (Utility.topViewController()?.view.subviews)! {
//            if subview.tag == 100 {
//                return
//            }
//        }
//        Utility.topViewController()?.view.isUserInteractionEnabled = false
//
//        Utility.topViewController()?.view.addSubview(messageFrame)
//    }
    
//    static func stopLoading() {
//        let activityIndicator = Utility.topViewController()?.view.viewWithTag(100)
//        DispatchQueue.main.async {
//            
//            activityIndicator?.removeFromSuperview()
//            Utility.topViewController()?.view.isUserInteractionEnabled = true
//        }
//        
//        
//    }

    static func dateFromString(dateString:String, format : String) -> NSDate {
        
        var truncated: String? = " "
        
        if dateString.characters.contains(".") {
            let word = dateString
            if let index = word.range(of: ".")?.lowerBound {
                let substring = word[..<index]
                truncated = String(substring)
            }
        }
            
        else {
            truncated = dateString
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let newFormat = DateFormatter()
        newFormat.dateFormat = format
        newFormat.timeZone = .current
        
        
        if let date = dateFormatter.date(from: truncated!) {
            return date as NSDate
        }
        else {
            return Date() as NSDate
        }
        
    }
    
    static func showLoader() {
        let appDelegate = AppDelegate.shared
        let lastIndex = appDelegate.window?.subviews.endIndex
        let pop : Loader = Loader(frame: appDelegate.window!.bounds)
        DispatchQueue.main.async {
            //guard let _ = appDelegate.window?.subviews.last as? Loader else {return}
            appDelegate.window?.insertSubview(pop, at: lastIndex! + 1 )
        }
    }

    static func hideLoader() {
        let appDelegate = AppDelegate.shared
            DispatchQueue.main.async {
                guard let _ = appDelegate.window?.subviews.last as? Loader else {return}
                appDelegate.window?.subviews.last?.removeFromSuperview()
        }
    }
   
}
//MARK:- INDICATOR
extension Utility{
    
    static func startLoading() {
        CustomLoader.sharedInstance.startAnimation()
//        UIApplication.shared.isNetworkActivityIndicatorVisible = true
//        let size = CGSize(width: 50, height: 50)
//        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballPulse, color: .systemOrange, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
//            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        
    }
    
    static func startLoadingText(text:String) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: text, messageFont: UIFont.systemFont(ofSize: 14), type: .ballGridBeat, color: .systemOrange, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: .systemOrange)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    static func stopLoading() {
        CustomLoader.sharedInstance.stopAnimation()
    }
    static func stopLoadingText() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
