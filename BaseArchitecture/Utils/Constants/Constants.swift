

var INTERNET_IS_RUNNING =  (AppStateManager.reachability?.isReachable)!



struct Global{
    static let LOGGED_IN_USER                = AppStateManager.sharedInstance.loggedInUser
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
}


struct Constants{
    static let appName = "Samovar"
    static let bundleId = "com.ingic.architecture"
    
    /** BASE URL */
    static let baseURL = "http://samovarapi.ingicweb.com/api"           // Live
//    static let baseURL = "http://10.100.28.14:804"
    // Local
    
    
    //Facebook And Google Keys
   static let facebookAppId = "2120500911524461"
   static let googleClientId = "902732401014-f1ap9krdndenv0hv3ke6sr17jh5gjpue.apps.googleusercontent.com"
   static var token = ""
   static var deviceToken = ""

   static var carpetsData = [
        [
            "title": "Black Heart",
            "image": "carpets-1",
            "time": "AED 1200",
            "isFav" : false
        ],
        [
            "title": "Green Heart",
            "image": "Rug1",
            "time": "AED 600",
            "isFav" : false
        ],
        [
            "title": "Blue Heart",
            "image": "Rug2",
            "time": "AED 1100",
            "isFav" : false
        ],
        [
            "title": "White Heart",
            "image": "Rug3",
            "time": "AED 800",
            "isFav" : false
        ],
        [
            "title": "Yellow Heart",
            "image": "Rug4",
            "time": "AED 900",
            "isFav" : false
        ],
        [
            "title": "Brown Heart",
            "image": "carpets-6",
            "time": "AED 1100",
            "isFav" : false
        ],
        [
            "title": "Black Heart",
            "image": "carpets-1",
            "time": "AED 1200",
            "isFav" : false
        ],
        [
            "title": "Green Heart",
            "image": "carpets-2",
            "time": "AED 600",
            "isFav" : false
        ],
        [
            "title": "Blue Heart",
            "image": "carpets-3",
            "time": "AED 1100",
            "isFav" : false
        ]
    ]
    
    /** Default Currency is AED */
    static var CURRENCY_STRING = "AED".localized()
    
    /** Application Delegate */
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    /** UIScreen.main.bounds.height */
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    
    /** UIScreen.main.bounds.width */
    static let screenWidth: CGFloat = UIScreen.main.bounds.width
    static let appTextColor = UIColor(red:0.19, green:0.23, blue:0.38, alpha:1.0)
    /** Height ratio with iPhone 6 **/
    static var hRatio: CGFloat {
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            return UIScreen.main.bounds.width / 667
        }
        
        return UIScreen.main.bounds.height / 667
    }
    
    /** Width ratio with iPhone 6  **/
    static var wRatio: CGFloat {
        if UIScreen.main.bounds.width > UIScreen.main.bounds.height {
            return UIScreen.main.bounds.height / 375
        }
        
        return UIScreen.main.bounds.width / 375
    }
    
    static let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
    
}

