
struct ErrorMessage {
    private init() {}
    
    static let network = NeworkAlerts()
    static let validation = ValidationAlerts()
    static let popups = PopupAlerts()
}


struct NeworkAlerts {
    fileprivate init() {}
    
    var noNetwork: String {
        return "No internet connection!"
    }
    var timeOut: String {
        return "Connection Timeout."
    }
    var errorOccured: String {
        return "An error occurred. Please try again."
    }
    var badRequest: String {
        return "Bad Request."
    }
}

struct ValidationAlerts {
    fileprivate init() {}
    
    var emailExist: String {
        return "User with entered email address already exists"
    }
    var usernameExist: String {
        return "This username is already taken, please try another"
    }
    var rsernameValidity: String {
        return "Please enter a valid username"
    }
    var emailValidity: String {
        return "Please enter a valid email address"
    }
    var shortPassword: String {
        return "This password is too short"
    }
    var newOldPasswordMatch: String {
        return "New password cannot be same as old password."
    }
    var passwordMisMatch: String {
        return "Passwords do not match."
    }
    
}

struct PopupAlerts {
    fileprivate init() {}
    
    var passwordChanged: String {
        return "An email has been sent to your account with new password."
    }
    var passwordChangedSuccess: String {
        return "Password changed successfully."
    }
    var internetOffline: String {
        return "Internet connection seems to be offline, Please try again."
    }
    var invalidEmailPassword: String {
        return  "Invalid email or password."
    }
    var wrongEmailInForgotPassword: String {
        return "User with entered email doesn’t exist."
    }
    
}
