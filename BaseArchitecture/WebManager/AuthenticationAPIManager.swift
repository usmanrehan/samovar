

import UIKit
import Alamofire
import SwiftyJSON

class AuthenticationAPIManager: APIManagerBase {


    func authenticateUserWith(email: String,password: String,
                          success:@escaping DefaultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
    
        
       
        let route: URL = POSTURLforRoute(route: Route.Login.rawValue)!
        
        
        let parameters: Parameters = [
            "email_address": email,
            "password": password
        ]
        
        
      self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    
    
    
    func userSignupWith(parameters: Parameters,
                              success:@escaping DefaultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.Register.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func googleSignupWith(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.googleSignIn.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func facebookSignupWith(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.facebookSignin.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }

    
    
    func updateDevicetoken(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.DeviceToken.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    
    
    func updateUserInfo(parameters: Parameters,
                           success:@escaping DefaultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.UpdateProfile.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure , errorPopup: errorPopup)
    }
    

    
    
    func forgotUserPassword(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.ForgotPassword.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    
    
    func resetForgotPassword(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.forgotPass.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func verifyEmail(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        let route : URL = GETURLfor(route: Route.forgotOne.rawValue, parameters: parameters)!
//        let route: URL = POSTURLforRoute(route: Route.ContactUS.rawValue)!
        
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func verifyCode(parameters: Parameters,
                      success:@escaping DefaultAPISuccessClosure,
                      failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        let route : URL = GETURLfor(route: Route.verifyCode.rawValue, parameters: parameters)!
        //        let route: URL = POSTURLforRoute(route: Route.ContactUS.rawValue)!
        
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func getContacts(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.ContactUS.rawValue)!
        
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    func getTerms(parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        let route: URL = GETURLfor(route: Route.Terms.rawValue)!
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }

    func getCategories(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.GetCategories.rawValue)!
        
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    
    
    func getTermsAndCondition(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.GetTermsAndCondition.rawValue)!
        
        self.getRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func refreshDeviceToken(parameters: Parameters,
                              success:@escaping DefaultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.RefreshDeviceToken.rawValue)!
     
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func changePassword(parameters: Parameters,
                            success:@escaping DefaultAPISuccessClosure,
                            failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.ChangePassword.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func editProfile(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.EditProfile.rawValue)!
        
        
        
        
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func getSubCategories(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.GetSubCategories.rawValue)!
        print(route)
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func getFavourites(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.GetFavourites.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func markFavourite(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.MarkFavourite.rawValue)!
        print(route)
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func markUnFavourite(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = POSTURLforRoute(route: Route.MarkUnFavourite.rawValue)!
        print(route)
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func getSearchCarpet(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.GetSearchCarpet.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func getNotifications(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.GetUserNotification.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func markNotificationsRead(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.MarkNotificationRead.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func removeAllNotifications(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.RemoveAllNotifications.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func getProductById(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.GetProductById.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
    func socialLogin(parameters: Parameters,
                              success:@escaping DefaultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL =  POSTURLforRoute(route: Route.SocialLogin.rawValue)!
        print(route)
        self.postRequestWith(route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    func forgetPassword(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure, errorPopup: Bool){
        
        
        
        let route: URL = URLforRoute(route: Route.ForgetPassword.rawValue + id, params: [:])! as URL
        print(route)
        self.getRequestWith(route: route, parameters: [:], success: success, failure: failure, errorPopup: errorPopup)
    }
}
