import UIKit
import Alamofire
import SwiftyJSON


class APIManagerBase: NSObject {
    var alamoFireManager : SessionManager!
    let defaultRequestHeader = ["Content-Type": "application/json"]
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func getAuthorizationHeader () -> Dictionary<String,String>{
        let language_Id = "1"
        if(AppStateManager.sharedInstance.isUserLoggedIn()){
            if let token = APIManager.instance.serverToken {
                var str = "Bearer "
                str.append(token)
                return ["auth_token":str,"Accept":"application/json","language_Id":language_Id]
            }
        }
        return ["Content-Type": "application/json", "language_Id":language_Id]
//        return defaultRequestHeader
    }
    
    func getVerificationHeader () -> Dictionary<String,String>{
        
        if(AppStateManager.sharedInstance.isUserLoggedIn()){
            if let token = APIManager.instance.serverToken {
                var str = "bearer "
                str.append(token)
                return ["Authorization":str,"Accept":"application/json"]
            }
        }
        return defaultRequestHeader
        
    }
    
    
    func getErrorFromResponseData(data: Data) -> NSError? {
        do{
            let result = try JSONSerialization.jsonObject(with: data,options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Dictionary<String,AnyObject>>
            if let message = result?[0]["message"] as? String{
                let error = NSError(domain: "GCError", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
                return error;
            }
        }catch{
            NSLog("Error: \(error)")
        }
        
        return nil
    }
    
    
    func URLforRoute(route: String,params:Parameters) -> NSURL? {
        
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.baseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params{
                queryItems.append(NSURLQueryItem(name:key,value:"\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            
            return components.url as NSURL?
        }
        
        return nil;
    }
    
    
    func POSTURLforRoute(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.baseURL+route)){
            return components.url! as URL
        }
        
        return nil
    }
    
    
    func GETURLfor(route:String) -> URL?{
        
        if let components: NSURLComponents = NSURLComponents(string: (Constants.baseURL+route)){
            return components.url! as URL
        }
        
        return nil
        
    }
    
    // Pass paramaters same as post request. (But in string)
    func GETURLfor(route:String, parameters: Parameters) -> URL?{
        var queryParameters = ""
        for key in parameters.keys {
            if queryParameters.isEmpty {
                queryParameters =  "?\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            } else {
                queryParameters +=  "&\(key)=\((String(describing: (parameters[key]!))).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            }
            queryParameters =  queryParameters.trimmingCharacters(in: .whitespaces)
            
        }
        if let components: NSURLComponents = NSURLComponents(string: (Constants.baseURL+route+queryParameters)){
            return components.url! as URL
        }
        return nil
    }
    
    
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure,
                         errorPopup: Bool){
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            
            guard response.result.error == nil else{
                
                print("👹 Error in calling post request")
                
//                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error! as NSError)
                return;
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
                
            }
        }
        
    }
    
    func postRequestWithBearer(route: URL,parameters: Parameters,
                               success:@escaping DefaultAPISuccessClosure,
                               failure:@escaping DefaultAPIFailureClosure,
                               errorPopup: Bool){
        
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getVerificationHeader()).responseJSON{
            response in
            
            guard response.result.error == nil else{
                
                print("👹 Error in calling post request")
                
                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error! as NSError)
                return;
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
        }
        
    }
    
    func postVerificationRequestWith(route: URL,parameters: Parameters,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure,
                                     errorPopup: Bool){
        
        alamoFireManager.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getVerificationHeader()).responseJSON{
            response in
            
            guard response.result.error == nil else{
                
                print("👹 Error in calling post request")
                
                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error as! NSError)
                return;
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
        }
    }
    
    //self.showErrorWith(message: ErrorMessage.popups.internetOffline)
    
    func showErrorMessage(error: Error){
        
        switch (error as NSError).code {
            
        case -1001:
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.network.timeOut)
        case -1009:
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.network.noNetwork)
        case 4:
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.network.badRequest)
        case -1005:
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.network.noNetwork)
        default:
            Utility.showAlert(title: Strings.ERROR.localized, message: (error as NSError).localizedDescription)
            
        }
    }
    
    
    func getRequestWith(route: URL, parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
            response in
            
            print(self.getAuthorizationHeader())
            guard response.result.error == nil else{
                print("👹 Error in calling post request")
                
                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error! as NSError)
                
                return;
            }
            
            if response.result.isSuccess {
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
            
        }
    }
    
    
    func putRequestWith(route: URL,parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        Alamofire.request(route, method: .put, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            
            guard response.result.error == nil else{
                
                print("👹 Error in calling post request")
                
//                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error! as NSError)
                return;
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
            
        }
        
    }
    
    
    func deleteRequestWith(route: URL,parameters: Parameters,
                           success:@escaping DefaultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure,
                           errorPopup: Bool){
        
        Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            guard response.result.error == nil else{
                
                print("👹 Error in calling post request")
                
                errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                
                failure(response.result.error! as NSError)
                
                return
            }
            
            if let value = response.result.value {
                print (value)
                if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                    success(jsonResponse)
                } else {
                    success(Dictionary<String, AnyObject>())
                }
            }
        }
    }
    
    func requestWithMultipart(URLSTR: URLRequest, route: URL,parameters: Parameters,
                              success:@escaping DefaultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure,
                              errorPopup: Bool){
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if parameters.keys.contains("photobase64") {
                let fileURL = URL(fileURLWithPath: parameters["photobase64"] as! String)
                multipartFormData.append(fileURL, withName: "profile_picture", fileName: "image.png", mimeType: "image/png")
            }
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                if ((keys[i] != "photobase64") && (keys[i] != "images")) {
                    subParameters[keys[i]] = values[i] as AnyObject
                }
            }
            
            if parameters.keys.contains("images") {
                let images = parameters["images"] as! Array<String>
                for i in 0  ..< images.count {
                    let fileURL = URL(fileURLWithPath: images[i])
                    multipartFormData.append(fileURL, withName: "image\(i+1)", fileName: "image\(i).png", mimeType: "image/png")
                }
            }
            
            for (key, value) in subParameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                //debug
                print(value)
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("👹 Error in calling post request")
                        
                        errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                        
                        failure(response.result.error! as NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                            success(jsonResponse)
                        } else {
                            success(Dictionary<String, AnyObject>())
                        }
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}
                
                failure(encodingError as NSError)
            }
        })
    }
    
    func putRequestWithMultipart_General(route: URL,parameters: Parameters,
                                         success:@escaping DefaultAPISuccessClosure,
                                         failure:@escaping DefaultAPIFailureClosure,
                                         errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.put, headers: getAuthorizationHeader())
        
        requestWithMultipart(URLSTR: URLSTR, route: route, parameters: parameters, success: success, failure: failure , errorPopup: errorPopup)
    }
    
    func postRequestWithMultipart_General(route: URL,parameters: Parameters,
                                          success:@escaping DefaultAPISuccessClosure,
                                          failure:@escaping DefaultAPIFailureClosure,
                                          errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        requestWithMultipart(URLSTR: URLSTR, route: route, parameters: parameters, success: success, failure: failure, errorPopup: errorPopup)
    }
    
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure,
                                  errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getAuthorizationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    
                    multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        print("👹 Error in calling post request")
                        
                        errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                        
                        failure(response.result.error as! NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                            success(jsonResponse)
                        } else {
                            success(Dictionary<String, AnyObject>())
                        }
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}
                
                failure(encodingError as NSError)
            }
        })
    }
    
    func postRequestWithMultipartWithBearer(route: URL,parameters: Parameters,
                                            success:@escaping DefaultAPISuccessClosure,
                                            failure:@escaping DefaultAPIFailureClosure,
                                            errorPopup: Bool){
        
        let URLSTR = try! URLRequest(url: route.absoluteString, method: HTTPMethod.post, headers: getVerificationHeader())
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            var subParameters = Dictionary<String, AnyObject>()
            let keys: Array<String> = Array(parameters.keys)
            let values = Array(parameters.values)
            
            for i in 0..<keys.count {
                //                if ((keys[i] != "file") && (keys[i] != "images")) {
                subParameters[keys[i]] = values[i] as AnyObject
            }
            
            
            for (key, value) in subParameters {
                if let data:Data = value as? Data {
                    
                    multipartFormData.append(data, withName: "file", fileName: "image.png", mimeType: "image/png")
                } else {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
        }, with: URLSTR, encodingCompletion: {result in
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    guard response.result.error == nil else{
                        
                        print("👹 Error in calling post request")
                        
                        errorPopup ? self.showErrorMessage(error: response.result.error!) : nil
                        
                        failure(response.result.error as! NSError)
                        return;
                    }
                    
                    
                    
                    if let value = response.result.value {
                        print (value)
                        if let jsonResponse = response.result.value as? Dictionary<String, AnyObject>{
                            success(jsonResponse)
                        } else {
                            success(Dictionary<String, AnyObject>())
                        }
                        
                    }
                    
                }
            case .failure(let encodingError):
                if errorPopup {self.showErrorMessage(error: encodingError)}
                failure(encodingError as NSError)
            }
        })
    }
    
    fileprivate func multipartFormData(parameters: Parameters) {
        let formData: MultipartFormData = MultipartFormData()
        if let params:[String:AnyObject] = parameters as [String : AnyObject]? {
            for (key , value) in params {
                
                if let data:Data = value as? Data {
                    
                    formData.append(data, withName: "profile_picture", fileName: "image.png", mimeType: "image/png")
                } else {
                    formData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            
            print("\(formData)")
        }
    }
}
