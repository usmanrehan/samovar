
import UIKit

typealias DefaultAPIFailureClosure = (NSError) -> Void
typealias DefaultAPISuccessClosure = (Dictionary<String,AnyObject>) -> Void
typealias DefaultBoolResultAPISuccesClosure = (Bool) -> Void


protocol APIErrorHandler {
    func handleErrorFromResponse(response: Dictionary<String,AnyObject>)
    func handleErrorFromERror(error:NSError)
}

class APIManager: NSObject {
    
    static let instance = APIManager()
    
    var serverToken: String? {
        get{
            return AppStateManager.sharedInstance.loggedInUser.auth_token
        }
    }
    
    let authenticationManagerAPI = AuthenticationAPIManager()
}



