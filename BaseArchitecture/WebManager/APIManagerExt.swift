
import UIKit
import Alamofire
import SwiftyJSON


extension APIManager{
    
    
    
    
    
    func userSignupWith(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
    
        authenticationManagerAPI.userSignupWith(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func googleSignupWith(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        authenticationManagerAPI.googleSignupWith(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func facebookSignupWith(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        authenticationManagerAPI.facebookSignupWith(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func verifyEmail(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
        authenticationManagerAPI.verifyEmail(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func verifyCode(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        
        authenticationManagerAPI.verifyCode(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func resetForgotPassword(parameters: Parameters,
                    success:@escaping DefaultAPISuccessClosure,
                    failure:@escaping DefaultAPIFailureClosure,
                    errorPopup: Bool){
        
        authenticationManagerAPI.resetForgotPassword(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func forgotUserPassword(parameters: Parameters,
                            success:@escaping DefaultAPISuccessClosure,
                            failure:@escaping DefaultAPIFailureClosure,
                            errorPopup: Bool){
    
        authenticationManagerAPI.forgotUserPassword(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func getContacts(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getContacts(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    
    
    func updateDevicetoken(parameters: Parameters,
                           success:@escaping DefaultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure,
                           errorPopup: Bool){
        
        
        authenticationManagerAPI.updateDevicetoken(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }

   

    
    
    func updateUserInfo(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
        
      authenticationManagerAPI.updateUserInfo(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    
    func getTerms(parameters: Parameters,
                  success:@escaping DefaultAPISuccessClosure,
                  failure:@escaping DefaultAPIFailureClosure,
                  errorPopup: Bool){
        
        authenticationManagerAPI.getTerms(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    

    
    func authenticateUserWith(email: String,password: String,
                          success:@escaping DefaultAPISuccessClosure,
                          failure:@escaping DefaultAPIFailureClosure,
                          errorPopup: Bool){
        
        authenticationManagerAPI.authenticateUserWith(email: email, password: password, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func getCategories(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getCategories(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func getSubCategories(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getSubCategories(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func getTermsAndCondition(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getTermsAndCondition(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    
    func refreshDeviceToken(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
    
        authenticationManagerAPI.refreshDeviceToken(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func changePassword(parameters: Parameters,
                            success:@escaping DefaultAPISuccessClosure,
                            failure:@escaping DefaultAPIFailureClosure,
                            errorPopup: Bool){
        
        authenticationManagerAPI.changePassword(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func editProfile(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        
        authenticationManagerAPI.editProfile(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func getFavourite(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getFavourites(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
    func markFavourite(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.markFavourite(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func markUnFavourite(parameters: Parameters,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.markUnFavourite(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func getSearchCarpet(id: String,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
           authenticationManagerAPI.getSearchCarpet(id: id, success: success, failure: failure,errorPopup: errorPopup)
       }
    func getNotifications(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getNotifications(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
    func markNotificationsRead(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.markNotificationsRead(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
    func removeAllNotifications(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.removeAllNotifications(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
    func getProductById(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.getProductById(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
    func socialLogin(parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        errorPopup: Bool){
    
        authenticationManagerAPI.socialLogin(parameters: parameters, success: success, failure: failure,errorPopup: errorPopup)
    }
    func forgetPassword(id: String,
                     success:@escaping DefaultAPISuccessClosure,
                     failure:@escaping DefaultAPIFailureClosure,
                     errorPopup: Bool){
        authenticationManagerAPI.forgetPassword(id: id, success: success, failure: failure,errorPopup: errorPopup)
    }
}
