//
//  EditProfileView.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 14/03/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
//import SwiftValidator

protocol EditProfileDelegate {
    func cancelTapped()
    func saveTapped(value: String, isName: Bool)
}
class EditProfileView: UIView {
    
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var textField: IGTextField!
    @IBOutlet weak var titleLabel: UILabel!
    var delegate : EditProfileDelegate?
    let validator = Validator()
    var isName = true
    var view: UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup(){
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask
            = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        saveBtn.isUserInteractionEnabled = false
        saveBtn.backgroundColor = UIColor.init(red: 239/255, green: 206/255, blue: 81/255, alpha: 0.5)
        setupTextField ()
//        textField.textField.inputView = UIView()
    }
    func setupTextField () {
       
        textField.textField.addTarget(self, action: #selector(self.didChangeTextFieldText(_:)), for: .editingChanged)
        
    }
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    func setUpNameValidation(){
        validator.registerField(
            textField: textField.textField,
            errorLabel: textField.errorLabel,
            rules:[
                RequiredRule(message: "This field is required"),
                MinLengthRule(length: 3, message: "Please enter a valid Name"),
                MaxLengthRule(length: 35, message: "Please enter a valid Name")
            ]
        )
        isName = true
    }
    func setupPhoneValidation(){
        validator.registerField(
            textField: textField.textField,
            errorLabel: textField.errorLabel,
            rules: [
                RequiredRule(message: "This field is required"),
                MinLengthRule(length: 8, message: "Please enter a valid phone number."),
                MaxLengthRule(length: 15, message: "Please enter a valid phone number.")
            ]
        )
        isName = false
    }
    @IBAction func cancelTapped(_ sender: Any) {
        delegate?.cancelTapped()
    }
    @IBAction func saveTapped(_ sender: Any) {
        self.textField.resetError()
         validator.validate(delegate: self)
//        delegate?.saveTapped()
    }
    @objc func didChangeTextFieldText(_ textField: UITextField) {
        if !textField.text!.isBlank() {
            saveBtn.isUserInteractionEnabled = true
            saveBtn.backgroundColor = UIColor.init(red: 239/255, green: 206/255, blue: 81/255, alpha: 1.0)
        } else {
            saveBtn.isUserInteractionEnabled = false
            saveBtn.backgroundColor = UIColor.init(red: 239/255, green: 206/255, blue: 81/255, alpha: 0.5)
        }
    }
}

extension EditProfileView: ValidationDelegate {
    
    func validationSuccessful() {
        
        if INTERNET_IS_RUNNING{
//            doSignIn()
            delegate?.saveTapped(value: textField.textField.text!, isName: self.isName)
            //            Utility.startLoading()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.popups.internetOffline)
        }
        
    }
    
    
    
    func validationFailed(errors:[UITextField:ValidationError]) {
        // turn the fields to red
        
        for (_, error) in errors {
            // if let field = field as? UITextField {
            // field.layer.borderColor = UIColor.red.cgColor
            //   field.layer.borderWidth = 1.0
            // }
            //   DDLogError(error.errorMessage)
            
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

