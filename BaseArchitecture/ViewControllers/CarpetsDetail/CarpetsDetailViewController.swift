//
//  CarpetsDetailViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class CarpetsDetailViewController: BaseViewController {
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var sku: UILabel!
    @IBOutlet weak var articaleNumber: UILabel!
    @IBOutlet weak var carpetTtitle: UILabel!
    @IBOutlet weak var carpetPrice: UILabel!
    @IBOutlet weak var carpetDetails: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var data = [String:Any]()
    
    var index : Int?
    var isFav = false
    var imageArray = [UIImage]()
    
    var categoryDetail: GetSubCategoriesModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewEssentails()
    }
    override func viewDidAppear(_ animated: Bool) {
        AppDelegate.shared.orientationLock = .portrait
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.shared.orientationLock = .portrait
        self.carpetTtitle.text = self.categoryDetail?.title ?? ""
        self.carpetPrice.text = "AED \(self.categoryDetail?.price ?? 0)"
        self.carpetDetails.text = self.categoryDetail?.descriptionValue ?? ""
    }
    
    @IBAction func onBtnAddToRoom(_ sender: UIButton) {
        if AppStateManager.sharedInstance.isUserLoggedIn(){
            let dest = ARViewer(nibName: "ARViewer", bundle: nil)
            dest.modelUrl = self.categoryDetail?.arCarpetModel ?? ""
            self.navigationController?.pushViewController(dest, animated: false)
        }else{
            Utility.showGeneralAlert(message: Strings.LOGIN_TO_CONTINUE.localized, title: Strings.SAMOVAR.localized, controller: self) { (yes, no) in
                if yes != nil {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    @IBAction func pageControlTapped(_ sender: UIPageControl) {
        let indexPath = IndexPath(item: sender.currentPage, section: 0)
        self.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
        
    }
}

//MARK:-Helper Method
extension CarpetsDetailViewController{
    func setupInfo()  {
        self.carpetTtitle.text = data["title"] as? String
    }
    func viewEssentails () {
        setupNavigation()
        //        setupInfo()
        setupPageControl()
        setupCollectionView()
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
        if AppStateManager.sharedInstance.isUserLoggedIn(){
            self.addFavButton()
        }
        self.setFavButton(value: self.categoryDetail?.isFavourite ?? false)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.title = (self.categoryDetail?.title ?? "").uppercased()
    }
    override func favTapped(sender: UIBarButtonItem) {
        let productId = self.categoryDetail?.productId
        guard let categoryDetail = self.categoryDetail else {return}
        if self.categoryDetail?.isFavourite ?? false{
            self.setFavButton(value: false)
            self.markUnFavourite(productId: productId)
        }else{
            self.setFavButton(value: true)
            self.markFavourite(productId: productId)
        }
        categoryDetail.isFavourite = !categoryDetail.isFavourite
    }
    func setupPageControl()  {
        pageControl.numberOfPages = self.categoryDetail?.images.count ?? 0
        pageControl.currentPage = 1
        
    }
    func setupCollectionView()  {
        self.collectionView.layoutIfNeeded()
        let nib = UINib(nibName: "CarpetsDetailCollectionViewCell", bundle:nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "CarpetsDetailCollectionViewCell")
        //
        collectionView.delegate = self
        collectionView.dataSource = self
        self.collectionView.reloadData()
        
        //        let screenWidth = UIScreen.main.bounds.width
        //        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //        let offset : CGFloat = 1
        //        let dynamicOffset = offset * (UIScreen.main.bounds.width / 375)
        //        layout.sectionInset = UIEdgeInsets(top: 0, left: dynamicOffset, bottom: 0, right: dynamicOffset)
        //        layout.itemSize = CGSize(width: screenWidth, height: collectionView.frame.height)
        //        layout.minimumInteritemSpacing = 0
        //        layout.minimumLineSpacing = 0
        //        collectionView!.collectionViewLayout = layout
        
    }
}

extension CarpetsDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize(width: self.collectionView.frame.width, height: self.collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryDetail?.images.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarpetsDetailCollectionViewCell", for: indexPath) as! CarpetsDetailCollectionViewCell
        let pictureCategory = self.categoryDetail?.images[indexPath.row].value ?? ""
        if let imageURL = URL(string: pictureCategory){
            cell.imageView?.sd_setImage(with: imageURL, completed: nil)
        }else{
            cell.imageView.image = UIImage(named: "Carpetplaceholder")
        }
        return cell
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(round(scrollView.contentOffset.x / scrollView.bounds.width))
        
    }
    
}


//MARK:- Services
extension CarpetsDetailViewController{
    private func markFavourite(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            if let resultString = result["message_to_show"] as? String{
                print(resultString)
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        
        let product_id = "\(productId ?? 0)"
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        
        let params: [String:Any] = ["user_id": user_id,
                                    "product_id": product_id]
        
        APIManager.instance.markFavourite(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func markUnFavourite(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            if let resultString = result["message_to_show"] as? String{
                print(resultString)
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        
        let product_id = "\(productId ?? 0)"
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        
        let params: [String:Any] = ["user_id": user_id,
                                    "product_id": product_id]
        
        APIManager.instance.markUnFavourite(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
