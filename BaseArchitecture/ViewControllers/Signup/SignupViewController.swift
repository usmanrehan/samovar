
import UIKit
import Device
//import SwiftValidator
import Alamofire
import RealmSwift
import Realm

class SignupViewController: BaseViewController {
    @IBOutlet weak var viewWidth: NSLayoutConstraint!
    
    @IBOutlet var viewHeight: [NSLayoutConstraint]!
    @IBOutlet var heightConstraint: [NSLayoutConstraint]!
    
    @IBOutlet weak var fullName: IGTextField!
    @IBOutlet weak var phoneNumber: IGTextField!
    @IBOutlet weak var email: IGTextField!
    @IBOutlet weak var password: IGTextField!
    @IBOutlet weak var cPassword: IGTextField!
 
    let validator = Validator()
    
    var isTermAccepted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.navigationController?.isNavigationBarHidden = false
        self.setupFields()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupTransparentNavBar()
        self.addBackButtonToNavigationBar()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sizeAdjustment()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func doSignup(_ sender: Any) {
        self.resetFields()
        validator.validate(delegate: self)
    }
    @IBAction func termButtonTapped(_ sender: Any) {
        let dest = PrivacyPolicyViewController(nibName: "PrivacyPolicyViewController", bundle: nil)
        dest.selectedTitle = "Terms of Use"
        self.navigationController?.pushViewController(dest, animated: true)
    }
    @IBAction func policyButtonTapped(_ sender: Any) {
        let dest = PrivacyPolicyViewController(nibName: "PrivacyPolicyViewController", bundle: nil)
        dest.selectedTitle = "Privacy Policy"
        self.navigationController?.pushViewController(dest, animated: true)
    }
    @IBAction func showTC(_ sender: Any) {
        
        
    }
    
}

//MARK:- Helper Method
extension SignupViewController{
    func setupFields(){
        self.email.textField.keyboardType = .emailAddress
        self.phoneNumber.textField.keyboardType = .numberPad
        validator.registerField(
            textField: email.textField,
            errorLabel: email.errorLabel,
            rules: [
                RequiredRule(message: "This field is required"),
                EmailRule(message: "Please Enter a valid Email Address")
            ]
        )
        
        validator.registerField(
            textField: fullName.textField,
            errorLabel: fullName.errorLabel,
            rules:[
                RequiredRule(message: "This field is required"),
                MinLengthRule(length: 3, message: "Please enter a valid Name"),
                MaxLengthRule(length: 35, message: "Please enter a valid Name")
            ]
        )
        
        validator.registerField(
            textField: password.textField,
            errorLabel: password.errorLabel,
            rules:[RequiredRule(message: "This field is required"),
                   MinLengthRule(length: 6, message: "This password is too short")
            ]
        )
        
        validator.registerField(
            textField: cPassword.textField,
            errorLabel: cPassword.errorLabel,
            rules: [
                RequiredRule(message: "This field is required"),
                ConfirmationRule(
                    confirmField:password.textField,
                    message: "Passwords do not match."
                )
            ]
        )
        
        validator.registerField(
            textField: phoneNumber.textField,
            errorLabel: phoneNumber.errorLabel,
            rules: [
                RequiredRule(message: "This field is required"),
                MinLengthRule(length: 8, message: "Please enter a valid phone number."),
                MaxLengthRule(length: 15, message: "Please enter a valid phone number.")
            ]
        )
    }
    func resetFields(){
        [fullName, email, password, cPassword, phoneNumber].forEach({ $0?.resetError() })
    }
    func sizeAdjustment(){
        switch Device.size() {
        case .screen4_7Inch:
            print("iPhone 6")
            setHeight(height: 50.0, viewH: 310, viewW: 300)
        case .screen5_5Inch:
            setHeight(height: 60.0, viewH: 360, viewW: 336)
            print("iPhone 6 plus")
        case .screen5_8Inch:
            setHeight(height: 60.0, viewH: 360, viewW: 336)
            print("iPhone X")
        default:
            setHeight(height: 60.0, viewH: 360, viewW: 336)
            print("iPhone X Max")
        }
    }
    func setHeight(height: CGFloat, viewH: CGFloat, viewW: CGFloat){
        for item in heightConstraint{
            item.constant = height
        }
        for item in viewHeight{
            item.constant = viewH
        }
        viewWidth.constant = viewW
    }
    
}
extension SignupViewController: ValidationDelegate {
    func validationSuccessful() {
        if INTERNET_IS_RUNNING{
            doSignUp()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
        }
    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        for (_, error) in errors {
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:- Services
extension SignupViewController{
    private func doSignUp(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["email_address":email.textField.text!,
                                      "password":password.textField.text!,
                                      "user_name":fullName.textField.text!,
                                      "phone_number":phoneNumber.textField.text!]
        print(parameters)
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                Utility.showAlert(title: Strings.SUCCESS.localized, message: Utility.getErrorMessage(result))
                self.email.textField.text = ""
                self.password.textField.text = ""
                self.fullName.textField.text = ""
                self.phoneNumber.textField.text = ""
                self.cPassword.textField.text = ""
                AppDelegate.shared.loadLoginUser()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                self.email.textField.text = ""
                self.password.textField.text = ""
                self.fullName.textField.text = ""
                self.phoneNumber.textField.text = ""
                self.cPassword.textField.text = ""
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.userSignupWith(parameters: parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}




