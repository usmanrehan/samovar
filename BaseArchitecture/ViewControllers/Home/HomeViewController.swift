//
//  HomeViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 14/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import SceneKit
import ARKit



class HomeViewController: BaseViewController {

    @IBOutlet weak var addIcon: UIImageView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var savedImageView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var favView: UIView!
    @IBOutlet weak var catCollectionView: UICollectionView!
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var unSupportedView: UIView!
    @IBOutlet weak var CatViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet var sceneView: ARSCNView!
    var startingFrame : CGPoint?
    var grids = [Grid]()
    var object: SCNNode!
    var catViewStatus : Bool!
    

    var carpetTitles = ["Abstract","Alen's Fever","Art","Bachtiari","Beloved","Hammam Deluxe","Illusion B&W","Rug","Sky Classic","Splash","Winter"]
    var carpetImages = [#imageLiteral(resourceName: "Abstract"), #imageLiteral(resourceName: "rug1"),#imageLiteral(resourceName: "rug2"),#imageLiteral(resourceName: "rug3"),#imageLiteral(resourceName: "rug4"),#imageLiteral(resourceName: "Hammam_Deluxe"),#imageLiteral(resourceName: "Illusion_Black&White"),#imageLiteral(resourceName: "Rug"),#imageLiteral(resourceName: "Sky Classic"),#imageLiteral(resourceName: "Splash"),#imageLiteral(resourceName: "Winter")]
//    var cells: [LiquidFloatingCell] = []
//    var floatingActionButton: LiquidFloatingActionButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewEssentials()
        
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        headerHeightConstraint.constant = self.topbarHeight
    }
    func viewEssentials(){
        UIApplication.shared.statusBarStyle = .default
        self.dismiss(animated: true, completion: nil)
        setUpCategoryView()
        setUpAR()
        sceneView.showsStatistics = false
        if !(ARConfiguration.isSupported){
            unSupportedView.isHidden = false
            sceneView.isHidden = true
            addIcon.isHidden = true
            return
        }
        
        
    }
    func setUpCategoryView(){
        let pan = UIPanGestureRecognizer(target : self , action : #selector(handleSwipe(gestureRecognizer:)))
        categoryView.addGestureRecognizer(pan)
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        let nib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        catCollectionView.register(nib, forCellWithReuseIdentifier: "cell")
        catCollectionView.delegate = self
        catCollectionView.delegate = self
        catCollectionView.contentInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    }
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
      
       // let transalation  = recognizer.translation(in: categoryView)
        if recognizer.state == .recognized {
              moveView()
           // startingFrame = recognizer.view?.center
        }
//        } else if recognizer.state == .ended {
//            if let view = recognizer.view {
//                if let center = startingFrame {
//                    view.center = center
//                   // moveView()
//                }
//
//            }
//            recognizer.setTranslation(CGPoint.zero, in: categoryView)
//        } else {
//            if categoryView.frame.minX >= UIScreen.main.bounds.width / 2 {
//                if let view = recognizer.view {
//                    if let center = startingFrame {
//                        view.center = center
//
//                        moveView()
//                    }
//
//                }
//                recognizer.setTranslation(CGPoint.zero, in: categoryView)
//
//            } else {
//                let transalation  = recognizer.translation(in: categoryView)
//                if let view = recognizer.view {
//                    view.center = CGPoint(x: view.center.x , y: view.center.y + transalation.y)
//                    //print(view.center)
//                }
//
//                //print(swipeView.center)
//                recognizer.setTranslation(CGPoint.zero, in: categoryView)
//            }
//
//        }
        
    }
    func moveView(){
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.CatViewLeadingConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
        catViewStatus = true
        UIView.animate(withDuration: 0.3, animations: {
            self.menuView.alpha = 0
            self.menuIcon.image = #imageLiteral(resourceName: "Menu_icon_yellow")
        })
         UIApplication.shared.statusBarStyle = .default
    }
    @objc func handleSwipe(gestureRecognizer : UIPanGestureRecognizer) {
        closeView()
    }
    func closeView(){
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.CatViewLeadingConstraint.constant = -1000
            self.view.layoutIfNeeded()
        })
//        UIApplication.shared.statusBarStyle = .lightContent
        catViewStatus = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        UIApplication.shared.statusBarStyle = .lightContent
        navigationController?.navigationBar.isHidden = true
        //AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        configurationAR()
    }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
         UIApplication.shared.statusBarStyle = .default
        navigationController?.navigationBar.isHidden = false
        sceneView.session.pause()
//        sceneView.session.configuration.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setActionButton(){
       
    }
    @IBAction func favouriteTapped(_ sender: Any) {
        let VC = FavouriteViewController(nibName: "FavouriteViewController", bundle: nil)
        self.navigationController?.pushViewController(VC, animated: true)
        toggleMenu()
    }
    @IBAction func settingtapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            let navContr = UINavigationController(rootViewController: preferenceVC)
            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }
    @IBAction func categoryTapped(_ sender: Any) {
        toggleMenu()
        moveView()
    }
    @IBAction func notificationTapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            let navContr = UINavigationController(rootViewController: preferenceVC)
            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = NotificationViewController(nibName: "NotificationViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }
    @IBAction func searchTapped(_ sender: Any) {
        toggleMenu()
        let VC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func savedImageTapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            let navContr = UINavigationController(rootViewController: preferenceVC)
            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = SavedImagesViewController(nibName: "SavedImagesViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }
    
    @IBAction func menuTapped(_ sender: Any) {
        toggleMenu()
    }
    
    func toggleMenu(){
        if menuIcon.image == #imageLiteral(resourceName: "Menu_icon_yellow"){
            UIView.animate(withDuration: 0.3, animations: {
                self.menuView.alpha = 1
                self.menuIcon.image = #imageLiteral(resourceName: "Cross_ison_yellow")
            })
            return
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.menuView.alpha = 0
            self.menuIcon.image = #imageLiteral(resourceName: "Menu_icon_yellow")
        })
    }

}
extension HomeViewController{
    func configurationAR(){
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        // Run the view's session
        sceneView.session.run(configuration)
    }
    func setUpAR(){
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
//        sceneView.showsStatistics = true
//        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        // Create a new scene
        let scene = SCNScene()//(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = scene
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(didPinch(_:)))
        sceneView.addGestureRecognizer(pinchGesture)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        sceneView.addGestureRecognizer(gestureRecognizer)
//        sceneView.session.currentFrame?.
    }
    @objc func didPinch(_ gesture: UIPinchGestureRecognizer) {
        guard let _ = object else { return }
        var originalScale = object?.scale
        
        switch gesture.state {
        case .began:
            originalScale = object?.scale
            gesture.scale = CGFloat((object?.scale.x)!)
        case .changed:
            guard var newScale = originalScale else { return }
            if gesture.scale < 0.5{ newScale = SCNVector3(x: 0.5, y: 0.5, z: 0.5) }else if gesture.scale > 2{
                newScale = SCNVector3(2, 2, 2)
            }else{
                newScale = SCNVector3(gesture.scale, gesture.scale, gesture.scale)
            }
            object?.scale = newScale
        case .ended:
            guard var newScale = originalScale else { return }
            if gesture.scale < 0.5 { newScale = SCNVector3(x: 0.5, y: 0.5, z: 0.5) }else if gesture.scale > 2{
                newScale = SCNVector3(2, 2, 2)
            }else{
                newScale = SCNVector3(gesture.scale, gesture.scale, gesture.scale)
            }
            object?.scale = newScale
            gesture.scale = CGFloat((object?.scale.x)!)
        default:
            gesture.scale = 1.0
            originalScale = nil
        }
    }
    @objc func tapped(gesture: UITapGestureRecognizer) {
        // Get exact position where touch happened on screen of iPhone (2D coordinate)
       /* let touchPosition = gesture.location(in: sceneView)
        // 2.
        //        let hitTestResult = sceneView.hitTest(touchPosition, types: .featurePoint)
        let hitTestResult = sceneView.hitTest(touchPosition, types: .estimatedHorizontalPlane)
        if !hitTestResult.isEmpty {
            
            guard let hitResult = hitTestResult.first else {
                return
            }
            
            addObject(hitTestResult: hitResult)
        }*/
    }
    func addObject(hitTestResult: ARHitTestResult) {
        let scene = SCNScene(named: "art.scnassets/Carpet_mesh.obj")!
        let grassNode = scene.rootNode.childNode(withName: "_material_1", recursively: true)
        //        grassNode?.position = SCNVector3(hitTestResult.worldTransform.columns.3.x)
        //        grassNode?.simdPosition = float3(planeAnchor.center.x, planeAnchor.center.y, planeAnchor.center.z)
        grassNode?.position = SCNVector3(hitTestResult.worldTransform.columns.3.x,hitTestResult.worldTransform.columns.3.y, hitTestResult.worldTransform.columns.3.z)
        // 2.
        self.sceneView.scene.rootNode.addChildNode(grassNode!)
    }
}

extension HomeViewController: ARSCNViewDelegate{
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        // Called when any node has been added to the anchor
        let grid = Grid(anchor: anchor as! ARPlaneAnchor)
        self.grids.append(grid)
        node.addChildNode(grid)
    }
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        // This method will help when any node has been removed from sceneview
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        // Called when any node has been updated with data from anchor
        let grid = self.grids.filter { grid in
            return grid.anchor.identifier == anchor.identifier
            }.first
        
        guard let foundGrid = grid else {
            return
        }
        
        foundGrid.update(anchor: anchor as! ARPlaneAnchor)
    }
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        // help us inform the user when the app is ready
        switch camera.trackingState {
        case .normal :
            print("Move the device to detect horizontal surfaces.")
        case .notAvailable:
            print("Tracking not available.")
        case .limited(.excessiveMotion):
            print("Tracking limited - Move the device more slowly.")
            
        case .limited(.insufficientFeatures):
            print("Tracking limited - Point the device at an area with visible surface detail.")
        case .limited(.initializing):
            print("Initializing AR session.")
            
        default:
            print("Default")
        }
    }
    func sessionWasInterrupted(_ session: ARSession) {
        print("Session was interrupted")
//        infoLabel.text = "Session was interrupted"
    }
    func sessionInterruptionEnded(_ session: ARSession) {
//        infoLabel.text = "Session interruption ended"
        print("Session interruption ended")
        resetTracking()
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
//        infoLabel.text = "Session failed: \(error.localizedDescription)"
        print("Session failed: \(error.localizedDescription)")
        resetTracking()
    }
    func resetTracking() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell

        cell.displayImage.image = carpetImages[indexPath.row]
        cell.titleLabel.text = carpetTitles[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carpetImages.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.catCollectionView.frame.size.width/3 - 16, height: 200.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let VC = CarpetsViewController(nibName: "CarpetsViewController", bundle: nil)
        VC.selectedTitle = carpetTitles[indexPath.row]
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension UIViewController {
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}
