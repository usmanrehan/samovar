//
//  CategoryCollectionViewCell.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 16/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var displayImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func roate(){
//        displayImage.image = image
        displayImage.rotate(angle: 45.0)
//        titleLabel.text = title
    }
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder:aDecoder)
//        //You Code here
////        displayImage.rotate(angle: -45.0)
//    }
    override func layoutSubviews() {
        super.layoutSubviews()
//        displayImage.rotate(angle: -45.0)
    }
}
extension UIImageView {
    
    /**
     Rotate a view by specified degrees
     
     - parameter angle: angle in degrees
     */
    func rotate(angle: CGFloat) {
        let radians = angle / 180.0 * CGFloat.pi
        
        let rotation = self.transform.rotated(by: radians)//CGAffineTransformRotate(self.transform, radians);
        self.transform = rotation
    }
    
}
