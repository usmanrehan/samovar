//
//  FavouriteViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 13/03/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class FavouriteViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var carpetsData = [[String : Any]]()
    var numberOfIndex = [Int]()
    var favourites = [GetSubCategoriesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewEssentails ()
        AppDelegate.shared.orientationLock = .portrait
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Favourites".uppercased()
        
        navigationController?.navigationBar.topItem?.title = "Favourites".uppercased()
        self.carpetsData.removeAll()
        for (index,data) in Constants.carpetsData.enumerated() {
            if data["isFav"] as! Bool {
                carpetsData.append(data)
                numberOfIndex.append(index)
            }
            
        }
        self.getFavourites()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewEssentails () {
        setupNavigation()
        setupCollectionView()
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
        self.addSearchButtonToNavigationBar()
        
        
    }
    func setupCollectionView() {
        self.collectionView.registerCell(withReuseIdentifier: String(describing: CarpetsCollectionViewCell.self))
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
    }
}
extension FavouriteViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.frame.width/2) , height: ((self.collectionView.frame.width/2) * 1.2) + 20)
        
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favourites.count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CarpetsCollectionViewCell.self), for: indexPath) as! CarpetsCollectionViewCell
        cell.title.text = self.favourites[indexPath.row].title ?? ""
        let pictureCategory = self.favourites[indexPath.row].imageIcon ?? ""
        if let imageURL = URL(string: pictureCategory){
            cell.imageView?.sd_setImage(with: imageURL, completed: nil)
        }else{
            cell.imageView.image = UIImage(named: "Carpetplaceholder")
        }
        cell.time.text = "AED \(self.favourites[indexPath.row].price)"
        cell.btnFavorite.tag = indexPath.row
        
        cell.btnFavorite.isSelected = true
        cell.btnFavorite.addTarget(self, action: #selector(self.onBtnFavPressed(_:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnFavPressed(_ sender: UIButton){
        let productId = self.favourites[sender.tag].productId
        self.markUnFavourite(productId: productId)
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
        self.favourites[indexPath.row].isFavourite = true
        dest.categoryDetail = self.favourites[indexPath.row]
        self.navigationController?.pushViewController(dest, animated: false)
    }
}

//MARK:- Services
extension FavouriteViewController{
    private func getFavourites(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let favouriteCarpets = result["response_data"] as? [[String:Any]] else {return}
                self.favourites = Mapper<GetSubCategoriesModel>().mapArray(JSONArray: favouriteCarpets)
                self.collectionView.reloadData()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        let userID = AppStateManager.sharedInstance.loggedInUser.userId
        APIManager.instance.getFavourite(id: String(userID), success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func markUnFavourite(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            if let resultString = result["message_to_show"] as? String{
                print(resultString)
                self.getFavourites()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        let product_id = productId ?? 0
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        let params: [String:Any] = ["user_id": user_id,
                                    "product_id": product_id]
        APIManager.instance.markUnFavourite(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
