//
//  ChangePasswordViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 05/03/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
//import SwiftValidator
import Alamofire
import RealmSwift
import Realm

class ChangePasswordViewController: BaseViewController {
    @IBOutlet weak var newPassField: IGTextField!
    @IBOutlet weak var conPassField: IGTextField!
    @IBOutlet weak var currentPassField: IGTextField!
    let validator = Validator()
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        
        self.title = "CHANGE PASSWORD"
        setUpFields()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpFields(){
        validator.registerField(
            textField: currentPassField.textField,
            errorLabel: currentPassField.errorLabel,
            rules:[RequiredRule(message: "This field is required"),
                   MinLengthRule(length: 6, message: "This password is too short")
            ]
        )
        validator.registerField(
            textField: newPassField.textField,
            errorLabel: newPassField.errorLabel,
            rules:[RequiredRule(message: "This field is required"),
                   MinLengthRule(length: 6, message: "This password is too short")
            ]
        )
        
        validator.registerField(
            textField: conPassField.textField,
            errorLabel: conPassField.errorLabel,
            rules: [
                RequiredRule(message: "This field is required"),
                ConfirmationRule(
                    confirmField:newPassField.textField,
                    message: "Passwords do not match."
                )
            ]
        )
    }
    func resetFields(){
        [currentPassField, newPassField, conPassField].forEach({ $0?.resetError() })
        
        
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        self.resetFields()
        
        validator.validate(delegate: self)
        //        self.loadVerifyNumber()
    }
    
    func invalidOldPassword(message: String){
        currentPassField.errorLabel.isHidden = false
        currentPassField.errorLabel.text = "Invalid Current Password"
    }
}

//MARK:- Validation Helper Methods
extension ChangePasswordViewController : ValidationDelegate {
    func validationSuccessful() {
        
        if INTERNET_IS_RUNNING {
            if currentPassField.textField.text == newPassField.textField.text {
                Utility.showAlert(title: Strings.ERROR.localized, message: "Current password and new password cannot be same")
            } else {
              changePassAPI()
            }
            
//            doSignUp()
            
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
        }
    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        // turn the fields to red
        
        for (_, error) in errors {
            // if let field = field as? UITextField {
            // field.layer.borderColor = UIColor.red.cgColor
            //   field.layer.borderWidth = 1.0
            // }
            //   DDLogError(error.errorMessage)
            
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:- Services
extension ChangePasswordViewController{
    private func changePassAPI(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["old_password":currentPassField.textField.text!,
                                      "new_password":newPassField.textField.text!,
                                      "user_id": AppStateManager.sharedInstance.loggedInUser.userId,
                                      "platform_id": 1]
        print(parameters)
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                
                self.navigationController?.popViewController(animated: true)
                Utility.showAlert(title: Strings.SUCCESS.localized, message: "Password changed successfully.")
                AppStateManager.sharedInstance.markUserLogout()
            }
            else if(response.status_code == 403){
                self.invalidOldPassword(message: Utility.getErrorMessage(result))
            }
            else{
                if result["response_data"] != nil {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
                } else {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                }
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.changePassword(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}

