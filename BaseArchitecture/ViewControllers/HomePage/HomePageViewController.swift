import UIKit
//import DGElasticPullToRefresh
import Device

class HomePageViewController: BaseViewController {
    //MARK:- Props
    @IBOutlet weak var tableView: UITableView!
    
    deinit { tableView.dg_removePullToRefresh()}
    
    var dataArray = ["A","B","C","D","E"] {
        didSet {
            tableView.reloadData()
            
        }
    }
    
    //MARK:- Main
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        
        setup()
    }
    
    func setup(){
        title =  "Home".localized()
        
        setupColorNavBar()
        setMediaButton()
        addMenuButtonToNavigationBar()
        
        tableView.addPullToRefresh(loaderColor: .white, target: #selector(didStartRefreshing), self)
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }
    @objc func didStartRefreshing(){
        tableView.stopRefreshing()
    }
}

//MARK:- Utility Methods
extension HomePageViewController {
    func setMediaButton(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAddMediaButton(sender:)))
    }
    @objc func didTapAddMediaButton(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Choose option", message: nil, preferredStyle: .actionSheet)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        alert.addAction(UIAlertAction.init(title: "Gallery", style: .default, handler: { (action) in self.openPicker(isCamera: false) }))
        alert.addAction(UIAlertAction.init(title: "Camera", style: .default, handler: { (action) in self.openPicker(isCamera: true) }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in }))
        
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    func openPicker(isCamera : Bool) {
        let failure:onCancel = {}
        let success:onMediaSelection = { (videoUrl, image) in
            if let videoUrl = videoUrl {
                if let image = Utility.thumbnailForVideoAtURL(url: videoUrl) {
                    let imageView = UIImageView(image: image)
                    imageView.contentMode = .scaleAspectFit
                    self.navigationItem.titleView = imageView
                }
            }
            
            if let image = image {
                let imageView = UIImageView(image: image)
                imageView.contentMode = .scaleAspectFit
                self.navigationItem.titleView = imageView
            }
            
            Utility.delay(delay: 5, closure: {
                self.navigationItem.titleView = nil
            })
        }
        
        let picker = MediaPickerHandler(controller: self,
                                        type: .all,
                                        source: isCamera ? .camera : .gallery,
                                        onSuccess: success,
                                        onCancel: failure)
        picker.show()
    }
}


//MARK:- UITableView Datasource
extension HomePageViewController:  UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if dataArray.count == 0 {
            Utility.emptyTableViewMessage(message: "No Data".localized(), tableView: tableView) }
        else{
            tableView.backgroundView = UIView()
        }
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: SideMenuCell.self) as! SideMenuCell
        
        cell.titleLabel.text = dataArray[indexPath.row]
        
        return cell
    }
}



//MARK:- UITableView Delegate
extension HomePageViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        IGTextField().showAsPopup() // Just an example of IGPopup
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 * Constants.wRatio
    }
}
