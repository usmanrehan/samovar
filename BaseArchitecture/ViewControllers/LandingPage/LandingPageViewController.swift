
import UIKit
import EAIntroView
import Device
class LandingPageViewController: UIViewController {
    
    @IBOutlet weak var buttonHeight: NSLayoutConstraint!
    
    var introView: EAIntroView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.navigationController?.isNavigationBarHidden = true
        self.hideTutorial()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        buttonHeight.constant = 50 * Constants.wRatio
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
    }
    
    @IBAction func skipTour(_ sender: Any) {
        self.hideTutorial()
    }
    @IBAction func loadTour(_ sender: Any) {
        
        self.showTutorial()
        
    }
    @IBAction func navigateToHome(_ sender: Any) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.loadHomePage()
        
        
    }
    
    func showTutorial(){
    }
    func hideTutorial(){
        let signInVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
        self.navigationController?.pushViewController(signInVC, animated: false)
        
    }
    
}

