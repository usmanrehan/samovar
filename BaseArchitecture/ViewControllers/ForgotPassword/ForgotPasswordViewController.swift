
import UIKit
import Device
//import SwiftValidator
class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet weak var buttonHeight: NSLayoutConstraint!
    
    @IBOutlet weak var email: IGTextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.navigationController?.isNavigationBarHidden = false
        
        self.addBackButtonToNavigationBar()
        
        self.title = "Forgot Password".localized()
        
        self.setupTextFields()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        buttonHeight.constant = 50 * Constants.wRatio
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        validator.validate(delegate: self)
    }
    
    func setupTextFields(){
        
        self.email.textField.keyboardType = .emailAddress
        
        validator.registerField(textField: email.textField,errorLabel: email.errorLabel, rules: [RequiredRule(message: NSLocalizedString("This field is required", comment: "")),
                                                                                                 EmailRule(message: NSLocalizedString("Please Enter a valid Email Address", comment: ""))])
    }
}

extension ForgotPasswordViewController: ValidationDelegate {
    func validationSuccessful() {
        
        if INTERNET_IS_RUNNING{
            self.forgetPassword()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.popups.internetOffline)
        }
    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        // turn the fields to red
        
        for (_, error) in errors {
            // if let field = field as? UITextField {
            // field.layer.borderColor = UIColor.red.cgColor
            //   field.layer.borderWidth = 1.0
            // }
            //   DDLogError(error.errorMessage)
            
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:- Services
extension ForgotPasswordViewController{
    func forgetPassword(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                Utility.showAlert(title: Strings.SUCCESS.localized, message: Utility.getErrorMessage(result))
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.forgetPassword(id: "\(self.email.textField.text ?? "")", success: successClosure, failure: failureClosure, errorPopup: true)
        }
}
