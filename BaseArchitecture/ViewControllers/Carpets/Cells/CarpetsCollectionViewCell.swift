//
//  CarpetsCollectionViewCell.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class CarpetsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnCarpetImage: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}
