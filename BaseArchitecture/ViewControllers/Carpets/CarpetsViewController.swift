//
//  CarpetsViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

struct DataSet{
    var name: String?
    var icon: UIImage?
}

class CarpetsViewController : BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var VCType = viewControllerType.ARView
    
    var selectedTitle = ""
    var sampleDataType = SampleDataType.images
    
    var categoryId: Int?
    var subCategories = [GetSubCategoriesModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewEssentails ()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.shared.orientationLock = .portrait
        super.viewWillAppear(animated)
        self.getSubCategories()
        self.title = selectedTitle.uppercased()
        navigationController?.navigationBar.topItem?.title = selectedTitle.uppercased()
        self.navigationController?.navigationBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewEssentails () {
        setupNavigation()
        setupCollectionView()
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
        self.addSearchButtonToNavigationBar()
        
        
    }
    func setupCollectionView() {
        collectionView.registerCell(withReuseIdentifier: String(describing: CarpetsCollectionViewCell.self))
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
}
extension CarpetsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionView.frame.width/2) , height: ((self.collectionView.frame.width/2)))
        
    }
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subCategories.count
    }
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CarpetsCollectionViewCell.self), for: indexPath) as! CarpetsCollectionViewCell
        cell.title.text = self.subCategories[indexPath.row].title ?? ""
        let pictureCategory = self.subCategories[indexPath.row].imageIcon ?? ""
        if let imageURL = URL(string: pictureCategory){
            cell.imageView?.sd_setImage(with: imageURL, completed: nil)
        }else{
            cell.imageView.image = UIImage(named: "Carpetplaceholder")
        }
        cell.time.text = "AED \(self.subCategories[indexPath.row].price)"
        cell.btnFavorite.tag = indexPath.row
        if !AppStateManager.sharedInstance.isUserLoggedIn(){
            cell.btnFavorite.isHidden = true
        }
        cell.btnFavorite.isSelected = self.subCategories[indexPath.row].user_favourite
        cell.btnFavorite.addTarget(self, action: #selector(self.onBtnFavPressed(_:)), for: .touchUpInside)
        cell.btnCarpetImage.tag = indexPath.row
        cell.btnCarpetImage.addTarget(self, action: #selector(self.onBtnCarpetImage(_:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnFavPressed(_ sender: UIButton){
        let productId = self.subCategories[sender.tag].productId
        print(productId)
        if self.subCategories[sender.tag].user_favourite{
            self.markUnFavourite(productId: productId)
        }else{
            self.markFavourite(productId: productId)
        }
    }
    @objc func onBtnCarpetImage(_ sender: UIButton){
        switch VCType.self{
        case .ARView:
            if AppStateManager.sharedInstance.isUserLoggedIn(){
               let dest = ARViewer(nibName: "ARViewer", bundle: nil)
                dest.modelUrl?.removeAll()
                dest.modelUrl = self.subCategories[sender.tag].arCarpetModel ?? ""
                self.navigationController?.pushViewController(dest, animated: false)
            }else{
                Utility.showGeneralAlert(message: Strings.LOGIN_TO_CONTINUE.localized, title: Strings.SAMOVAR.localized, controller: self) { (yes, no) in
                    if yes != nil {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
        case .detail:
            let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
            dest.categoryDetail = self.subCategories[sender.tag]
            self.navigationController?.pushViewController(dest, animated: false)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        switch VCType.self{
        //        case .ARView:
        //            let dest = ARViewer(nibName: "ARViewer", bundle: nil)
        //            dest.modelUrl?.removeAll()
        //            dest.modelUrl = self.subCategories[indexPath.row].arCarpetModel ?? ""
        //            self.navigationController?.pushViewController(dest, animated: false)
        //        case .detail:
        //            let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
        //            dest.data = Constants.carpetsData.first as! [String : Any]
        //            dest.categoryDetail = self.subCategories[indexPath.row]
        //            self.navigationController?.pushViewController(dest, animated: false)
        //        }
        
    }
}

//MARK:- Services
extension CarpetsViewController{
    private func getSubCategories(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let subCategories = result["response_data"] as? [[String:Any]] else {return}
                self.subCategories = Mapper<GetSubCategoriesModel>().mapArray(JSONArray: subCategories)
                self.collectionView.reloadData()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        var user_id = 0
        if AppStateManager.sharedInstance.isUserLoggedIn(){
            user_id = AppStateManager.sharedInstance.loggedInUser.userId
        }
        let category_id = "\(self.categoryId ?? 0)"
        let params: [String:Any] = ["user_id": user_id,
                                    "category_id": category_id]
        APIManager.instance.getSubCategories(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func markFavourite(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            if let resultString = result["message_to_show"] as? String{
                print(resultString)
                self.getSubCategories()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        let product_id = productId ?? 0
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        
        let params: [String:Any] = ["user_id": user_id,
                                    "product_id": product_id]
        print(params)
        APIManager.instance.markFavourite(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func markUnFavourite(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            if let resultString = result["message_to_show"] as? String{
                print(resultString)
                self.getSubCategories()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        let product_id = productId ?? 0
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        let params: [String:Any] = ["user_id": user_id,
                                    "product_id": product_id]
        print(params)
        APIManager.instance.markUnFavourite(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
