//
//  OverlayHomeController.swift
//  BaseArchitecture
//
//  Created by Abdul  Karim Khan on 04/05/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import OverlayContainer

class HomeOverlayController: OverlayContainerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.setViews()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    
}
//MARK:- Setup views
extension HomeOverlayController{
    private func setViews(){
        self.delegate = self
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let ARSceneView = storyboard.instantiateViewController(withIdentifier: "ViewControllerAR") as! ViewControllerAR
        ARSceneView.plusTapped = {
            self.moveOverlay(toNotchAt: 1, animated: true)
        }
        let categoryController = CategoryController(nibName: "CategoryController", bundle: nil)
        self.viewControllers.append(ARSceneView)
        self.viewControllers.append(categoryController)
    }
    func addBackBarButtonItem() {
        let image = UIImage(named: "backArrow2")
        let backItem = UIBarButtonItem(image: image,
                                       style: .plain,
                                       target: self,
                                       action: #selector(self.onBtnBack))
        self.navigationItem.leftBarButtonItem = backItem
    }
    @objc func onBtnBack() {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Methods for SamovarPreview
extension HomeOverlayController{

}
extension HomeOverlayController: OverlayContainerViewControllerDelegate{
    enum OverlayNotch: Int, CaseIterable {
        case minimum, maximum
    }
    func numberOfNotches(in containerViewController: OverlayContainerViewController) -> Int {
        return OverlayNotch.allCases.count
    }
    func overlayContainerViewController(_ containerViewController: OverlayContainerViewController,
                                        heightForNotchAt index: Int,
                                        availableSpace: CGFloat) -> CGFloat {
        switch OverlayNotch.allCases[index] {
        case .maximum:
            return availableSpace * 3.75 / 4
        case .minimum:
            return availableSpace * 0.3 / 4
        }
    }
    func overlayContainerViewController(_ containerViewController: OverlayContainerViewController, didMoveOverlay overlayViewController: UIViewController, toNotchAt index: Int) {
        print(overlayViewController)
    }
}
