//
//  ViewController.swift
//  ARCarpet
//
//  Created by Abdul  Karim Khan on 09/04/2020.
//  Copyright © 2020 Abdul Karim Khan. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation


class ViewControllerAR: UIViewController, ARSCNViewDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var savedImageView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var favView: UIView!

    @IBOutlet weak var sceneView: ARSCNView!
    
    let configuration = ARWorldTrackingConfiguration()
    var plusTapped: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sceneView.session.run(configuration)
        AppDelegate.shared.orientationLock = .portrait
    }
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.shared.orientationLock = .portrait
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.shared.orientationLock = .all
    }
//MARK:- MENU ACTIONS
    @IBAction func plusTapped(_ sender: Any) {
        self.plusTapped?()
    }
    @IBAction func favouriteTapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            self.navigationController?.pushViewController(preferenceVC, animated: true)
        }else{
            let VC = FavouriteViewController(nibName: "FavouriteViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    @IBAction func settingtapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            self.navigationController?.pushViewController(preferenceVC, animated: true)

//            let navContr = UINavigationController(rootViewController: preferenceVC)
//            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    @IBAction func categoryTapped(_ sender: Any) {
        toggleMenu()
        let VC = CarpetCategories(nibName: "CarpetCategories", bundle: nil)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func notificationTapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            self.navigationController?.pushViewController(preferenceVC, animated: true)
//            let navContr = UINavigationController(rootViewController: preferenceVC)
//            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = NotificationViewController(nibName: "NotificationViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    @IBAction func searchTapped(_ sender: Any) {
        toggleMenu()
        let VC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func savedImageTapped(_ sender: Any) {
        toggleMenu()
        if AppStateManager.sharedInstance.isGuestUser{
            let preferenceVC = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
            preferenceVC.isGuest = true
            self.navigationController?.pushViewController(preferenceVC, animated: true)
//
//            let navContr = UINavigationController(rootViewController: preferenceVC)
//            navigationController?.present(navContr, animated: true, completion: nil)
        }
        else{
            let VC = SavedImagesViewController(nibName: "SavedImagesViewController", bundle: nil)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }
    @IBAction func menuTapped(_ sender: Any) {
        toggleMenu()
    }
    
    func toggleMenu(){
        if menuIcon.image == #imageLiteral(resourceName: "Menu_icon_yellow"){
            UIView.animate(withDuration: 0.3, animations: {
                self.menuView.alpha = 1
                self.menuIcon.image = #imageLiteral(resourceName: "Cross_ison_yellow")
            })
            return
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.menuView.alpha = 0
            self.menuIcon.image = #imageLiteral(resourceName: "Menu_icon_yellow")
        })
    }
}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi/180}
}
