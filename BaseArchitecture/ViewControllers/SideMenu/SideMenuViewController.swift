
import UIKit

class SideMenuViewController: UIViewController {

    
    

    @IBOutlet weak var tableView: UITableView!
    
    var menu = [Menu].init(arrayLiteral: Menu(image: "menu_map", title: NSLocalizedString("Map & Hours", comment: "")),
                           
                           Menu(image: "menu_profile",title:NSLocalizedString("My Profile", comment: "")),
                           
                           Menu(image: "menu_tour", title: NSLocalizedString("Tour App", comment: "")),
                           
                           Menu(image: "menu_notifications",title:NSLocalizedString("Notifications", comment: "")),
                           
                           Menu(image: "menu_settings", title: NSLocalizedString("Settings", comment: "")),
                          
                           Menu(image: "menu_support",title:NSLocalizedString("Support", comment: "")),
                           
                           Menu(image: "menu_subscribe", title: NSLocalizedString("Subscription", comment: "")),
                           
                           Menu(image: "menu_parking",title:NSLocalizedString("Parking History", comment: "")),
                           
                           Menu(image: "menu_feedback",title:NSLocalizedString("Feedback", comment: "")),
                           
                           Menu(image: "menu_about",title:NSLocalizedString("About Us", comment: "")),
                           
                           Menu(image: "menu_logout",title:NSLocalizedString("Logout", comment: ""))
        
        
    )
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}

extension SideMenuViewController: UITableViewDelegate{

/*
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 5:
        let vc = SupportViewController(nibName: "SupportViewController", bundle: nil)
            let navigationController = UINavigationController(rootViewController: vc)
            self.sideMenuViewController.setContentViewController(navigationController, animated: true)
            self.sideMenuViewController.hideViewController()
        default:
            print("Oh")
        }
    } 
  */  
    
}


extension SideMenuViewController: UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sideCell = self.tableView.dequeueReusableCell(ofType: SideMenuCell.self) as! SideMenuCell
        sideCell.data =  menu[indexPath.row]
        return sideCell
        
    }
    
    
    
}
