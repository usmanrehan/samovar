
import UIKit
import ESTabBarController_swift

extension TabBarController {
    func homeController() -> UINavigationController {
        let title = "Tab 1"
        let item = ESTabBarItem.init(ExampleBouncesContentView(), title: title, image: #imageLiteral(resourceName: "tab_home_unselected"), selectedImage: #imageLiteral(resourceName: "tab_home_selected"))
        
        let controller = HomePageViewController(nibName: String(describing: HomePageViewController.self), bundle: nil)
        controller.view.backgroundColor = UIColor(hex: 0xd2d2d2)
        controller.tabBarItem = item
        controller.title = title
        return UINavigationController(rootViewController: controller)
    }
}

extension TabBarController {
    func setControllers(selectedIndex: Int = 0) {
        viewControllers = [homeController(), homeController(), homeController(), homeController(), homeController()]
        self.selectedIndex = selectedIndex
    }
}

class TabBarController: ESTabBarController {
    var statusBarStyle:UIStatusBarStyle = .default
    
    override func loadView() {
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setControllers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
}

