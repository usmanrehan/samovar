//
//  ForgotTwoViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 28/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import KWVerificationCodeView
import Alamofire

class ForgotTwoViewController: BaseViewController {
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var verificationCodeView: KWVerificationCodeView!
    
    var email : String!
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        viewEssential ()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = Strings.EMAIL_VERIFICATION.localized
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        self.verifyCode()
    }
    @IBAction func resetCodeTapped(_ sender: Any) {
        self.resendCode()
    }
    
}

//MARK:- Helper Method
extension ForgotTwoViewController{
    func viewEssential () {
        setResendTitle()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        self.title = Strings.EMAIL_VERIFICATION.localized
        self.verificationCodeView.delegate = self
        self.btnContinue.backgroundColor = UIColor.init(red: 239.0/255, green: 206.0/255, blue: 81.0/255, alpha: 0.5)
        self.btnContinue.isUserInteractionEnabled = false
    }
    func setResendTitle(){
        let italicAttribute = [NSAttributedStringKey.font:FontName.poppinsRegular.font(withSize: 15),NSAttributedStringKey.foregroundColor: Constants.appTextColor]
        let nextDropString = NSAttributedString(string: "Didn't get your code? ", attributes: italicAttribute)
        let colorFontAttributes = [NSAttributedStringKey.font:FontName.poppinsMedium.font(withSize: 15) , NSAttributedStringKey.foregroundColor: Constants.appTextColor, NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue] as [NSAttributedStringKey : Any]
        let timerString = NSAttributedString(string: "Resend", attributes: colorFontAttributes)
        let labelString = NSMutableAttributedString(attributedString: nextDropString)
        labelString.append(timerString)
        self.resendButton.setAttributedTitle(labelString, for: .normal)
    }
}

//MARK:- Delegate
extension ForgotTwoViewController : KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        if verificationCodeView.hasValidCode() {
            self.btnContinue.backgroundColor = UIColor.init(red: 239.0/255, green: 206.0/255, blue: 81.0/255, alpha: 1.0)
            self.btnContinue.isUserInteractionEnabled = true
        } else {
            self.btnContinue.backgroundColor = UIColor.init(red: 239.0/255, green: 206.0/255, blue: 81.0/255, alpha: 0.5)
            self.btnContinue.isUserInteractionEnabled = false
        }
    }
}

//MARK:- Services
extension ForgotTwoViewController{
    func resendCode(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["email":self.email!]
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                print(result)
                Utility.showAlert(title: Strings.SUCCESS.localized, message: Strings.CODE_SENT_SUCCESS.localized)
            }
            else{
                if result["Result"] != nil {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
                } else {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                }
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.verifyEmail(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
    func verifyCode(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["email":email!,
                                      "VerificationCode":verificationCodeView.getVerificationCode()]
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                print(result)
                let forgotVC = ResetPasswordViewController(nibName: "ResetPasswordViewController", bundle: nil)
                forgotVC.email = self.email!
                self.navigationController?.pushViewController(forgotVC, animated: true)
            }
            else if(response.status_code == 404) {
                Utility.showAlert(title: Strings.ERROR.localized, message: Strings.ENTERED_CODE_INVALID.localized)
            }
            else{
                if result["Result"] != nil {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
                } else {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                }
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.verifyCode(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
