//
//  SavedImagesCollectionViewCell.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
protocol SavedImagesDelegate {
    func selectedTapped (sender : UIButton , index : Int)
    func unSelectedTapped (sender : UIButton,index : Int)
}

class SavedImagesCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var saveImage: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var selectedOverlay: UIView!
    
    var delegate : SavedImagesDelegate?
    
    var isCellSelected = false
    var selectedIndex : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupLongPressGesture()
    }
    
    @IBAction func selectTapped(_ sender: UIButton) {
        if !isSelected {
            delegate?.selectedTapped(sender: sender , index: selectedIndex!)
            selectedCell ()
        } else {
            delegate?.unSelectedTapped(sender: sender, index: selectedIndex!)
            unSelectedCell ()
        }
    }
}

//MARK:- HelperMethod
extension SavedImagesCollectionViewCell{
    func selectedCell () {
        selectedOverlay.isHidden = false
        isSelected = true
    }
    func unSelectedCell () {
        selectedOverlay.isHidden = true
        isSelected = false
    }
    func setupLongPressGesture(){
        let longPressGestureRecognizer : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGestureRecognizer.minimumPressDuration = 0.5
        longPressGestureRecognizer.delaysTouchesBegan = true
        self.saveImage?.addGestureRecognizer(longPressGestureRecognizer)
    }
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        if (gestureRecognizer.state != UIGestureRecognizerState.ended){return}
        print(self.saveImage.tag)
        Utility.showAlertYesNo(message: "This image will be delete from saved images.", title: Strings.SAMOVAR.localized, controller: Utility.topViewController() ?? UIViewController()) { (yes, no) in
            return
        }
    }
}
