//
//  SavedImagesViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class SavedImagesViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        viewEssentails()
        self.title = Strings.SAVED_IMAGES.localized
        self.navigationController?.navigationBar.topItem?.title = Strings.SAVED_IMAGES.localized
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func shareTapped(_ sender: UIButton) {
        var shareImages = [UIImage]()
        guard  let indexPaths = self.collectionView.indexPathsForSelectedItems else {
            menuView.isHidden = true
            return
        }
        for index in indexPaths {
            let imageData = AppStateManager.sharedInstance.loggedInUser.arrFiles[index.row].fileData ?? Data()
            shareImages.append(UIImage(data: imageData) ?? UIImage())
        }
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareImages, applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func deleteTapped(_ sender: UIButton) {
        deleteAlert()
    }
}

//MARK:-Helper Methods
extension SavedImagesViewController{
    func deleteAlert()  {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to delete?", preferredStyle: .alert)
        let no = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction) in
        }
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (action:UIAlertAction) in
            self.deleteData ()
        }
        alertController.addAction(yes)
        alertController.addAction(no)
        self.present(alertController, animated: true, completion: nil)
    }
    func deleteData() {
        guard  let indexPaths = self.collectionView.indexPathsForSelectedItems else {
            menuView.isHidden = true
            return
        }
        menuView.isHidden = true
        for index in indexPaths {
            collectionView.deselectItem(at: index, animated: false)
            
            try! Global.APP_REALM?.write(){
                print("Realm Writed")
                let user = AppStateManager.sharedInstance.loggedInUser
                user?.arrFiles.remove(at: index.row)
                AppStateManager.sharedInstance.loggedInUser = user
                Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
            }
        }
        self.collectionView.reloadData()
    }
    func viewEssentails()  {
        collectionViewEssentails()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        menuView.isHidden = true
    }
    func collectionViewEssentails () {
        let nib = UINib(nibName: "SavedImagesCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        collectionView.allowsMultipleSelection = true
        collectionView.contentInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    }
}

//MARK:- CollectionView Delegate
extension SavedImagesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SavedImagesCollectionViewCell
        cell.selectedIndex = indexPath.row
        cell.saveImage.tag = indexPath.row
        cell.saveImage.image = UIImage(data: AppStateManager.sharedInstance.loggedInUser.arrFiles[indexPath.row].fileData ?? Data())
        cell.unSelectedCell()
        cell.delegate = self
        
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if AppStateManager.sharedInstance.loggedInUser.arrFiles.count > 0 {
            self.collectionView.restore()
            return 1
        }
        self.collectionView.setEmptyMessage("No Images Found")
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AppStateManager.sharedInstance.loggedInUser.arrFiles.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (indexPath.row/2 == 0)  {
            return CGSize(width: self.collectionView.frame.size.width/2 - 16, height: self.collectionView.frame.size.height/2 - 100)
        }
        return CGSize(width: self.collectionView.frame.size.width - 20, height: self.collectionView.frame.size.height/2 - 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}

//MARK:- SavedImage Delegate
extension SavedImagesViewController : SavedImagesDelegate {
    func selectedTapped (sender : UIButton , index : Int) {
        let selectedIndex = IndexPath.init(row: index, section: 0)
        self.collectionView.selectItem(at: selectedIndex, animated: false, scrollPosition: .centeredHorizontally)
        guard  let indexPaths = self.collectionView.indexPathsForSelectedItems else {
            menuView.isHidden = true
            return
        }
        if indexPaths.count > 0 {
            menuView.isHidden = false
        } else {
            menuView.isHidden = true
        }
    }
    func unSelectedTapped (sender : UIButton,index : Int) {
        let selectedIndex = IndexPath.init(row: index, section: 0)
        collectionView.deselectItem(at: selectedIndex, animated: false)
        guard  let indexPaths = self.collectionView.indexPathsForSelectedItems else {
            menuView.isHidden = true
            return
        }
        if indexPaths.count > 0 {
            menuView.isHidden = false
        } else {
            menuView.isHidden = true
        } 
    }
}
