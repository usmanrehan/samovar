//
//  ViewController.swift
//  ARCarpet
//
//  Created by Abdul  Karim Khan on 09/04/2020.
//  Copyright © 2020 Abdul Karim Khan. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import RealmSwift


enum viewControllerType{
    case ARView
    case detail
}

class ARViewer: BaseViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var btnClear: UIButton!
    
    var modelUrl: String?
    var fileUrl = String()

    var carpetPosition = SCNVector3()

    let configuration = ARWorldTrackingConfiguration()

    var horizontalSurfaceGrid = [Grid]()
    
    var singleNodeFlag = false
    var nodeNull = true
        
    var PCoordx: Float = 0.0
    var PCoordy: Float = 0.0
    var PCoordz: Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.downloadModel()
        self.registerGestureRecognizers()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        AppDelegate.shared.orientationLock = .portrait
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSaveScreenShot(_ sender: UIButton) {
        self.sceneView.session.pause()
        
        let image = self.sceneView.takeScreenshot()
        
        try! Global.APP_REALM?.write(){
            let attachment = AttachmentsModel()
            attachment.attachmentId = UUID().uuidString
            attachment.fileData = UIImagePNGRepresentation(image) ?? Data()
            
            print("Realm Writed")
            let user = AppStateManager.sharedInstance.loggedInUser
            user?.arrFiles.append(attachment)
            AppStateManager.sharedInstance.loggedInUser = user
            Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
        }
        DispatchQueue.main.async{
            Utility.showAlert(message: "Screenshot saved to application gallery", title: "Samovar Carpets", controller: self) { (share,ok) in
                if share != nil{
                    let shareVC = ShareVC(nibName: "ShareVC", bundle: nil)
                    shareVC.shouldResumeARRendering = {_ in
                        self.sceneView.session.run(self.configuration)
                    }
                    shareVC.images = [image]
                    shareVC.modalPresentationStyle = .overCurrentContext
                    shareVC.modalTransitionStyle = .crossDissolve
                    self.present(shareVC, animated: true, completion: nil)
                }
                if ok != nil{
                    self.sceneView.session.run(self.configuration)
                }
            }
        }
    }
    @IBAction func onBtnShare(_ sender: UIButton) {
        self.sceneView.session.pause()
        
        let image = self.sceneView.takeScreenshot()
        let shareVC = ShareVC(nibName: "ShareVC", bundle: nil)
        shareVC.shouldResumeARRendering = {_ in
            self.sceneView.session.run(self.configuration)
        }
        shareVC.images = [image]
        shareVC.modalPresentationStyle = .overCurrentContext
        shareVC.modalTransitionStyle = .crossDissolve
        self.present(shareVC, animated: true, completion: nil)
    }
    @IBAction func onBtnNextCarpet(_ sender: UIButton) {
        let node = sceneView.scene.rootNode.childNode(withName: "CarpetNode", recursively: true)
        let material = node?.geometry?.material(named: "BackgroundImage")
        material?.diffuse.contents = UIImage(named: "Modular.png")
    }
    @IBAction func onBtnResetView(_ sender: UIButton) {
        self.sceneView.scene.rootNode.enumerateChildNodes { (existingNode, _) in
            if existingNode.name == "CarpetNode"{
                existingNode.removeFromParentNode()
            }
        }
        self.nodeNull = true
        self.singleNodeFlag = false
        Utility.showLoader()
    }
}
//MARK:- Helper Method
extension ARViewer{
    private func setDelegateScene(){
        self.configuration.planeDetection = [.horizontal]
        self.configuration.isLightEstimationEnabled = true
        self.sceneView.automaticallyUpdatesLighting = true
        self.sceneView.delegate = self
        self.sceneView.autoenablesDefaultLighting = true
        self.sceneView.session.run(self.configuration)
    }
    private func registerGestureRecognizers() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        let dragGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinch))
        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotateNode(sender:)))
        
        self.sceneView.addGestureRecognizer(pinchGestureRecognizer)
        self.sceneView.addGestureRecognizer(tapGestureRecognizer)
        self.sceneView.addGestureRecognizer(dragGestureRecognizer)
        self.sceneView.addGestureRecognizer(rotationGestureRecognizer)
    }
    private func resetTracking() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    private func downloadModel(){
        if self.modelUrl == nil {
            Utility.showAlert(message: "Carpet not available.", title: "Samovar Carpets", controller: self) {
                self.navigationController?.popViewController(animated: true)
            }
            return
        }
        DispatchQueue.main.async {
            Utility.startLoadingText(text: "Please wait!\nCarpet is downloading...")
        }
        DispatchQueue.global().async {
            
            FileDownloader.loadFileSync(urlString: self.modelUrl ?? "") { (filePath, error) in
                if error != nil{
                    print(error?.localizedDescription ?? "error")
                    return
                }
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0, animations: {
                        Utility.stopLoadingText()
                    }) { (_) in
                        Utility.showLoader()
                        
                        guard let filePath = filePath else {return}
                        self.fileUrl = filePath
                        self.setDelegateScene()
                    }
                }
            }
        }
    }
    private func addItem(hitTestResult: ARHitTestResult) {
//    private func addItem(anchor: ARAnchor) {

        guard let filePathURL = URL(string: self.fileUrl) else {return}
        
        let scene = try! SCNScene(url: filePathURL, options: [.checkConsistency: true])
        print(scene)
        
        let mdlAsset = MDLAsset(url: filePathURL)
        let assetName = mdlAsset.object(at: 0).name
        guard let node = scene.rootNode.childNode(withName: assetName, recursively: true) else {
            Utility.showAlert(message: "Carpet not available.", title: "Samovar Carpets", controller: self) {
                self.navigationController?.popViewController(animated: true)
            }
            return
        }
        node.scale = SCNVector3(0, 0, 0)
        
        let loadAction = SCNAction.scale(to: 0.15, duration: 2)
        
        node.runAction(loadAction)
        node.name = "CarpetNode"
        let transform = hitTestResult.worldTransform
        
        let thirdColumn = transform.columns.3
        node.position = SCNVector3(thirdColumn.x, thirdColumn.y, thirdColumn.z)
//        node.position = SCNVector3(anchor.transform.columns.3.x, anchor.transform.columns.3.y, anchor.transform.columns.3.z)

        self.sceneView.scene.rootNode.addChildNode(node)
    }
}
//MARK:- Gestures to interact with ARModel
extension ARViewer{
    @objc func panGesture(_ gesture: UIPanGestureRecognizer) {
        
        let node = sceneView.scene.rootNode.childNode(withName: "CarpetNode", recursively: true)
        
        switch gesture.state {
        case .began:
            guard let hitNode = self.sceneView.hitTest(gesture.location(in: self.sceneView),
                                                       options: nil).first else {return}
            self.PCoordx = (hitNode.worldCoordinates.x)
            self.PCoordy = (hitNode.worldCoordinates.y)
            self.PCoordz = (hitNode.worldCoordinates.z)
        case .changed:
            let hitNode = sceneView.hitTest(gesture.location(in: sceneView), options: nil)
            
//            let location = gesture.location(in: sceneView)

//            guard let hitTestResult = sceneView.hitTest(location, types: [.existingPlaneUsingGeometry, .estimatedVerticalPlane, .estimatedHorizontalPlane]).first else {return}
//
//            guard let planeAnchor = hitTestResult.anchor as? ARPlaneAnchor else {return}
//
//            if planeAnchor.alignment == .vertical {
//                if let coordx = hitNode.first?.worldCoordinates.x,
//                    let coordy = hitNode.first?.worldCoordinates.y,
//                    let coordz = hitNode.first?.worldCoordinates.z {
//                    let action = SCNAction.moveBy(x: (CGFloat(coordx - PCoordx)),
//                                                  y: 0.0,//CGFloat(coordy - PCoordy),
//                                                  z: -(CGFloat(coordy - PCoordy)),
//                                                  duration: 0.0)
//                    node?.runAction(action)
//
//                    self.PCoordx = coordx
//                    self.PCoordy = coordy
//                    self.PCoordz = coordz
//                }
//
//            }else{
                if let coordx = hitNode.first?.worldCoordinates.x,
                    let coordy = hitNode.first?.worldCoordinates.y,
                    let coordz = hitNode.first?.worldCoordinates.z {
                    let action = SCNAction.moveBy(x: CGFloat(coordx - PCoordx),
                                                  y: CGFloat(coordy - PCoordy),
                                                  z: CGFloat(coordz - PCoordz),
                                                  duration: 0.0)
                    node?.runAction(action)
                    
                    self.PCoordx = coordx
                    self.PCoordy = coordy
                    self.PCoordz = coordz
                }
//            }
            
            
            
            
            gesture.setTranslation(CGPoint.zero, in: self.sceneView)
        case .ended:
            self.PCoordx = 0.0
            self.PCoordy = 0.0
            self.PCoordz = 0.0
        default:
            break
        }
    }
    @objc func tapped(sender: UITapGestureRecognizer) {
        let sceneView = sender.view as! ARSCNView
        let tapLocation = sender.location(in: sceneView)
        
        guard let hitTest = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent).first else {return}
        if !self.singleNodeFlag {
            self.singleNodeFlag = true
            self.nodeNull = false
            let anchor = ARAnchor(transform: hitTest.worldTransform)

            self.addItem(hitTestResult: hitTest)
//            self.addItem(anchor: anchor)
//            self.sceneView.session.add(anchor: anchor)

        }
    }
    @objc func pinch(sender: UIPinchGestureRecognizer) {
        let sceneView = sender.view as! ARSCNView
        let pinchLocation = sender.location(in: sceneView)
        let hitTest = sceneView.hitTest(pinchLocation)
        
        let node = sceneView.scene.rootNode.childNode(withName: "CarpetNode", recursively: true)
        
        if !hitTest.isEmpty {
            let pinchAction = SCNAction.scale(by: sender.scale, duration: 0)
            print(sender.scale)
            DispatchQueue.main.async {
                node?.runAction(pinchAction)
            }
            sender.scale = 1.0
        }
    }
    @objc func rotateNode(sender: UIRotationGestureRecognizer) {
        let sceneView = sender.view as! ARSCNView
        let holdLocation = sender.location(in: sceneView)
        let hitTest = sceneView.hitTest(holdLocation)
        let node = sceneView.scene.rootNode.childNode(withName: "CarpetNode", recursively: true)
        
        var currentAngleY: Float = 0.0
        if !hitTest.isEmpty {
            let rotation = Float(sender.rotation)
            
            if sender.state == .changed{
                node?.eulerAngles.y = currentAngleY - rotation
            }
            if(sender.state == .ended) {
                currentAngleY = node?.eulerAngles.y ?? 0.0
            }
        }
    }
}
//MARK:- ARSceneViewDelegate
extension ARViewer{
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        
        if !self.nodeNull{
            

        }else{            
//            let grid = Grid(anchor: anchor as! ARPlaneAnchor)
//            self.horizontalSurfaceGrid.append(grid)
//            node.addChildNode(grid)
            Utility.hideLoader()
        }
        
    }
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        // This method will help when any node has been removed from sceneview
    }
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        
//        guard let planeAnchor = anchor as? ARPlaneAnchor
//        else { return }
        
//        let grid = self.horizontalSurfaceGrid.filter { grid in
//            return grid.anchor.identifier == anchor.identifier
//        }.first
        
//        guard let foundGrid = grid else {return}
        if self.singleNodeFlag{
//            foundGrid.removeFromParentNode()
//            DispatchQueue.main.async{
//                Utility.hideLoader()
//            }
        }
        DispatchQueue.main.async{
            if self.singleNodeFlag{
                self.btnClear.isHidden = false
                return
            }
            self.btnClear.isHidden = true
//            Utility.showLoader()
        }
        DispatchQueue.main.async{
            Utility.hideLoader()
        }
//        foundGrid.update(anchor: anchor as! ARPlaneAnchor)
    }
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        // help us inform the user when the app is ready
        
        switch camera.trackingState {
        case .normal:
            print("Move towards horizontal plane")
        case .notAvailable:
            print("Tracking not available.")
        case .limited(.excessiveMotion):
            print("Tracking limited - Move the device more slowly.")
            
        case .limited(.insufficientFeatures):
            print("Tracking limited - Point the device at an area with visible surface detail.")
        case .limited(.initializing):
            print("Initializing AR session.")
        default:
            print("Default")
        }
    }
    func sessionWasInterrupted(_ session: ARSession) {
        print("Session was interrupted")
        //        infoLabel.text = "Session was interrupted"
    }
    func sessionInterruptionEnded(_ session: ARSession) {
        //        infoLabel.text = "Session interruption ended"
        print("Session interruption ended")
        //            resetTracking()
    }
    func session(_ session: ARSession, didFailWithError error: Error) {
        //        infoLabel.text = "Session failed: \(error.localizedDescription)"
        print("Session failed: \(error.localizedDescription)")
        //resetTracking()
    }
}
