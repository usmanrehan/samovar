//
//  CarpetCategories.swift
//  BaseArchitecture
//
//  Created by Mac Book on 08/05/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class CarpetCategories: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = [GetCategoriesModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigation ()
        AppDelegate.shared.orientationLock = .portrait
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "CategoryCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "cell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = Strings.CATEGORIES.localized
        self.getCategories()
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
    }
}
extension CarpetCategories: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell
        let pictureCategory = self.categories[indexPath.row].imageIcon ?? ""
        if let imageURL = URL(string: pictureCategory){
            cell.displayImage?.sd_setImage(with: imageURL, completed: nil)
        }else{
            cell.displayImage.image = UIImage(named: "Carpetplaceholder")
        }
        cell.titleLabel.text = self.categories[indexPath.row].title ?? ""
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.size.width/2, height: 250.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let categoryId = self.categories[indexPath.row].categoryId
        let categoryTitle = self.categories[indexPath.row].title ?? ""
        let VC = CarpetsViewController(nibName: "CarpetsViewController", bundle: nil)
        VC.categoryId = categoryId
        VC.selectedTitle = categoryTitle
        VC.VCType = .detail
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


//MARK:- Services
extension CarpetCategories{
    private func getCategories(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let categories = result["response_data"] as? [[String:Any]] else {return}
                self.categories = Mapper<GetCategoriesModel>().mapArray(JSONArray: categories)
                self.collectionView.reloadData()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.getCategories(parameters: [:], success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
