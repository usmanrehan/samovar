//
//  PrivacyPolicyViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 28/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import  ObjectMapper

class PrivacyPolicyViewController: BaseViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    var selectedTitle = ""
    var termsAndCondition = [TermsAndConditionModel]()

    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        self.viewEssentails()
    }
}

//MARK:- Helper Method
extension PrivacyPolicyViewController{
    func viewEssentails()  {
        self.getTermsandCondition()
        self.setupNavigation ()
    }
    func setupTextView()  {
        textView.contentInset = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
        for item in self.termsAndCondition {
            if item.title == "privacy policy" && self.selectedTitle == "Privacy Policy" {
                textView.text = item.descriptionValue ?? ""
                break
            }
            if item.title == "Term and Condition" && self.selectedTitle == "Terms of Service" {
                textView.text = item.descriptionValue ?? ""
            }
        }
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
        title = selectedTitle.uppercased()
    }
}

//MARK:- Services
extension PrivacyPolicyViewController{
    private func getTermsandCondition(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let terms = result["response_data"] as? [[String:Any]] else {return}
                self.termsAndCondition = Mapper<TermsAndConditionModel>().mapArray(JSONArray: terms)
                self.setupTextView()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.getTermsAndCondition(parameters: [:], success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
