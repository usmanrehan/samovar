
import UIKit
import Device
//import SwiftValidator
import Realm
import RealmSwift
import ObjectMapper

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet var heightConstraints: [NSLayoutConstraint]!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var signinTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var guestUserSignIn: UILabel!
    
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var forgotPasswordLabel: IGUILabel!
    @IBOutlet weak var buttonHeight: NSLayoutConstraint!
    @IBOutlet weak var email: IGTextField!
    @IBOutlet weak var password: IGTextField!
    
    let validator = Validator()
    
    var isDismiss = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.orientationLock = .portrait
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.email.textField.text = "samuels@gmail.com"
        self.password.textField.text = "samuels123"
        super.viewWillAppear(animated)
        addSkipButton()
        self.setupTransparentNavBar()
        self.addBackButtonToNavigationBar()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if (isDismiss){
            //            self.dismiss(animated: false, completion: nil)
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sizeAdjustment()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tourApp(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func doSignIn(_ sender: Any) {
    self.view.endEditing(true)
    
    self.resetValidators()
    
    validator.validate(delegate: self)
    
}
    @IBAction func resetButtonTapped(_ sender: Any) {
        //        ForgotOneViewController
        let VC = ForgotOneViewController(nibName: "ForgotOneViewController", bundle: nil)
        self.navigationController?.pushViewController(VC, animated: false)
    }
}

//MARK:- Helper Method
extension SignInViewController{
    func resetValidators(){
        self.email.resetError()
        self.password.resetError()
        
    }
    func setResetButtonText(){
        let oneAttr = NSMutableAttributedString()
        let twoAttr = NSMutableAttributedString()
        oneAttr.regular("Forgot your password? ", size: 17, color: Constants.appTextColor)
        twoAttr.bold("Reset Now", size: 17, color: Constants.appTextColor)
        oneAttr.append(twoAttr)
        //        resetButton.attributedTitle(for: .normal) = oneAttr
        resetButton.setAttributedTitle(oneAttr, for: .normal)
    }
    func sizeAdjustment(){
        switch Device.size() {
        case .screen4_7Inch:
            print("iPhone 6")
            setHeight(height: 50.0, viewH: 130)
        case .screen5_5Inch:
            setHeight(height: 60.0, viewH: 150)
            print("iPhone 6 plus")
        case .screen5_8Inch:
            setHeight(height: 60.0, viewH: 150)
            print("iPhone X")
        default:
            setHeight(height: 60.0, viewH: 150)
            print("iPhone X Max")
        }
    }
    func setHeight(height: CGFloat, viewH: CGFloat){
        for item in heightConstraints{
            item.constant = height
        }
        viewHeight.constant = viewH
        //        viewWidth.constant = viewW
    }
    func setupUI(){
        //        setResetButtonText()
        setupSignupLabel()
        //        forgotPasswordLabel.isUserInteractionEnabled = true
        //        signupLabel.isUserInteractionEnabled = true
        //        guestUserSignIn.isUserInteractionEnabled = true
        //        setupResetLabel()
        //        self.setupSignupLabel()
        //        self.setupGestures()
        self.setupTextFields()
    }
    func setupResetLabel(){
        //        oneAttr.normal("Forgot your password? ", size: 17)
        //        twoAttr.bold("Reset Now", size: 17)
        let italicAttribute = [NSAttributedStringKey.font:FontName.poppinsLight.font(withSize: 15)]
        let nextDropString = NSAttributedString(string: "Forgot your password? ", attributes: italicAttribute)
        
        let colorFontAttributes = [NSAttributedStringKey.font:FontName.poppinsBold.font(withSize: 15) , NSAttributedStringKey.foregroundColor: UIColor.BARed]
        let timerString = NSAttributedString(string: "Guest User", attributes: colorFontAttributes)
        
        
        let labelString = NSMutableAttributedString(attributedString: nextDropString)
        
        labelString.append(timerString)
        
        self.resetButton.titleLabel?.attributedText = labelString
        
    }
    func setupSignupLabel(){
        
        let italicAttribute = [NSAttributedStringKey.font:FontName.poppinsRegular.font(withSize: 15),NSAttributedStringKey.foregroundColor: Constants.appTextColor]
        let nextDropString = NSAttributedString(string: "Forgot your password? ", attributes: italicAttribute)
        
        let colorFontAttributes = [NSAttributedStringKey.font:FontName.poppinsMedium.font(withSize: 15) , NSAttributedStringKey.foregroundColor: Constants.appTextColor]
        let timerString = NSAttributedString(string: "Reset Now", attributes: colorFontAttributes)
        
        
        let labelString = NSMutableAttributedString(attributedString: nextDropString)
        
        labelString.append(timerString)
        self.resetButton.setAttributedTitle(labelString, for: .normal)
    }
    func setupGestures(){
        
        let forgotGesture = UITapGestureRecognizer(target: self, action: #selector(loadForgotPassword))
        self.forgotPasswordLabel.addGestureRecognizer(forgotGesture)
        
        let guestGesture = UITapGestureRecognizer(target: self, action: #selector(loadGuestUser))
        self.guestUserSignIn.addGestureRecognizer(guestGesture)
        let signupGesture = UITapGestureRecognizer(target: self, action: #selector(loadSignup))
        self.signupLabel.addGestureRecognizer(signupGesture)
        
        
        
    }
    func setupTextFields(){
        
        self.email.textField.keyboardType = .emailAddress
        self.email.textField.textContentType = .emailAddress
        validator.registerField(textField: email.textField,errorLabel: email.errorLabel, rules: [RequiredRule(message: NSLocalizedString("This field is required", comment: "")), EmailRule(message: NSLocalizedString("Please Enter a valid Email Address", comment: ""))])
        validator.registerField(textField: password.textField,errorLabel: password.errorLabel,rules: [RequiredRule(message: NSLocalizedString("This field is required", comment: "")),MinLengthRule(length: 6, message: "This password is too short")])
    }
    @objc func loadForgotPassword(){
        let forgotVC = ForgotPasswordViewController(nibName: "ForgotPasswordViewController", bundle: nil)
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    @objc func loadGuestUser(){
        
        //SignupViewController
        
        
    }
    @objc func loadSignup(){
        let signupVC = SignupViewController(nibName: "SignupViewController", bundle: nil)
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
}

//MARK:- Validation
extension SignInViewController: ValidationDelegate {
    func validationSuccessful() {
        
        if INTERNET_IS_RUNNING{
            doSignIn()
            //            Utility.startLoading()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
        }
        
    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        // turn the fields to red
        
        for (_, error) in errors {
            // if let field = field as? UITextField {
            // field.layer.borderColor = UIColor.red.cgColor
            //   field.layer.borderWidth = 1.0
            // }
            //   DDLogError(error.errorMessage)
            
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:- Services
extension SignInViewController{
    private func doSignIn(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let userProfile = result["response_data"] as? [[String:Any]] else {return}
                let user = Mapper<UserLogin>().mapArray(JSONArray: userProfile)
                Constants.token = user.first?.auth_token ?? ""
                AppStateManager.sharedInstance.loggedInUser = user.first
                AppDelegate.shared.refreshDeviceToken()
                self.isDismiss = true
                do {
                    let realm = try Realm()
                    try realm.write() {
                        print("Realm Writed")
                        realm.add(AppStateManager.sharedInstance.loggedInUser!, update: .all)
                    }
                    let VC = HomeOverlayController()
                    self.navigationController?.pushViewController(VC, animated: false)
                    AppStateManager.sharedInstance.changeRootViewController()
                    
                } catch let error {
                    Logger.logException(error: error)
                }
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.authenticateUserWith(email: email.textField.text!, password: password.textField.text!, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
