//
//  SearchViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 27/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    
    var categories = [GetCategoriesModel]()
    var subCategories = [GetSubCategoriesModel]()
    
    var searchResult = SearchCarpets()
    
    var categoryId = Int()
    var filteredCarpetData = [[String:String]]()
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        viewEssentails()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        
    }
    @IBAction func backTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Helper Method
extension SearchViewController{
    func viewEssentails () {
        viewEssentialDidLoad()
        setupTextField()
    }
    func setupTextField() {
        searchTextField.addTarget(self, action: #selector(self.didChangeTextFieldText(_:)), for: .editingChanged)
    }
    func viewEssentialDidLoad(){
        let header = UINib(nibName: "NotificationHeaderTableViewCell", bundle: nil)
        tableView.register(header, forCellReuseIdentifier: "NotificationHeaderTableViewCell")
        
        let footer = UINib(nibName: "FooterTableViewCell", bundle: nil)
        tableView.register(footer, forCellReuseIdentifier: "FooterTableViewCell")
        
        let cell = UINib(nibName: "NotificationTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.searchResult.categoriesResponse.count
        case 1:
            return self.searchResult.productsResponse.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderTableViewCell") as! NotificationHeaderTableViewCell
        switch section {
        case 0:
            headerCell.lblHeading.text = "Categories"
        case 1:
            headerCell.lblHeading.text = "Products"
        default:
            return headerCell
        }
        return headerCell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        cell.sideView.layer.cornerRadius = cell.sideView.frame.size.height / 2
        if indexPath.row % 2 == 0{
            cell.sideView.backgroundColor = UIColor(red:0.63, green:0.87, blue:0.41, alpha:1.0)
        }
        switch indexPath.section {
        case 0:
            cell.lblMessage.text = self.searchResult.categoriesResponse[indexPath.row].title ?? "-"
            let pictureCategory = self.searchResult.categoriesResponse[indexPath.row].imageIcon ?? ""
            if let imageURL = URL(string: pictureCategory){
                cell.img?.sd_setImage(with: imageURL, completed: nil)
            }else{
                cell.img.image = UIImage(named: "Carpetplaceholder")
            }
            cell.lblHour.isHidden = true
            cell.viewIsRead.isHidden = true
        case 1:
            cell.lblMessage.text = self.searchResult.productsResponse[indexPath.row].title ?? "-"
            let pictureCategory = self.searchResult.productsResponse[indexPath.row].imageIcon ?? ""
            if let imageURL = URL(string: pictureCategory){
                cell.img?.sd_setImage(with: imageURL, completed: nil)
            }else{
                cell.img.image = UIImage(named: "Carpetplaceholder")
            }
            cell.lblHour.isHidden = true
            cell.viewIsRead.isHidden = true
        default:
            return cell
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterTableViewCell") as! FooterTableViewCell
        return footerCell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if self.searchResult.categoriesResponse.isEmpty{
                return 0
            }
        case 1:
            if self.searchResult.productsResponse.isEmpty{
                return 0
            }
        default:
            print(0)
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if self.searchResult.categoriesResponse.isEmpty{
                return 0
            }
        case 1:
            if self.searchResult.productsResponse.isEmpty{
                return 0
            }
        default:
            print(0)
        }
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            let categoryTitle = self.searchResult.categoriesResponse[indexPath.row].title ?? ""
            let categoryId = self.searchResult.categoriesResponse[indexPath.row].categoryId
            let VC = CarpetsViewController(nibName: "CarpetsViewController", bundle: nil)
            VC.categoryId = categoryId
            VC.selectedTitle = categoryTitle
            VC.VCType = .detail
            self.navigationController?.pushViewController(VC, animated: true)
        case 1:
            let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
            dest.categoryDetail = self.searchResult.productsResponse[indexPath.row]
            self.navigationController?.pushViewController(dest, animated: false)
        default:
            print(0)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isTracking { return }
        cell.contentView.alpha = 0.3
        cell.layer.transform = CATransform3DMakeScale(0.7, 0.7, 0.7)
        // Simple Animation
        UIView.animate(withDuration: 0.5) {
            cell.contentView.alpha = 1
            cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view : UIView, forSection section: Int) {
        if tableView.isTracking { return }
        view.alpha = 0.3
        view.layer.transform = CATransform3DMakeScale(0.7, 0.7, 0.7)
        // Simple Animation
        UIView.animate(withDuration: 0.5) {
            view.alpha = 1
            view.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if tableView.isTracking { return }
        view.alpha = 0.3
        view.layer.transform = CATransform3DMakeScale(0.7, 0.7, 0.7)
        // Simple Animation
        UIView.animate(withDuration: 0.5) {
            view.alpha = 1
            view.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}



//MARK:- Textfield Change
extension SearchViewController  {
    @objc func didChangeTextFieldText(_ textField: UITextField) {
        if searchTextField.text?.isEmpty ?? false {return}
        self.getSearchData(id: searchTextField.text ?? "")
    }
}

//MARK:- Services
extension SearchViewController{
    private func getSearchData(id: String?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let responseData = result["response_data"] as? [[String:Any]] else {return}
                print(responseData)
                guard let response = responseData.first else {return}
                self.searchResult = Mapper<SearchCarpets>().map(JSON: response) ?? SearchCarpets()
                self.tableView.reloadData()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.getSearchCarpet(id: "\(id ?? "")", success: successClosure, failure: failureClosure, errorPopup: true)
    }
}


