//
//  WaterMark.swift
//  BaseArchitecture
//
//  Created by Abdul  Karim Khan on 13/06/2020.
//  Copyright © 2020 Ingic. All rights reserved.
//

import UIKit

class WaterMark: UIView {
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "WaterMark", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
