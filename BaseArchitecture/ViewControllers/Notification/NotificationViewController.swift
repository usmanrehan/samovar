//
//  NotificationViewController.swift
//  SaloonIK_IOS
//
//  Created by aneelahmed on 11/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Alamofire
import DZNEmptyDataSet
import RealmSwift
import ObjectMapper

class NotificationViewController: BaseViewController {
    @IBOutlet weak var internetView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var heading : [String]!
    
    var notifications = [NotificationModel]()
    var subCategories = GetSubCategoriesModel()
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        viewEssentialDidLoad()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        heading = ["TODAY","Yesterday"]
        self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = "NOTIFICATIONS"
        super.viewWillAppear(animated)
        if !(INTERNET_IS_RUNNING){
            tableView.isHidden = true
            internetView.isHidden = false
        }else{
            self.getNotifications()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func retryTapped(_ sender: Any) {
        Utility.startLoading()
        if !(INTERNET_IS_RUNNING){
            tableView.isHidden = true
            internetView.isHidden = false
            Utility.stopLoading()
            return
        }
        tableView.isHidden = false
        internetView.isHidden = true
        Utility.stopLoading()
    }
    @IBAction func btnCloseClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Helper Method
extension NotificationViewController{
    func viewEssentialDidLoad(){
        let header = UINib(nibName: "NotificationHeaderTableViewCell", bundle: nil)
        tableView.register(header, forCellReuseIdentifier: "NotificationHeaderTableViewCell")
        
        let footer = UINib(nibName: "FooterTableViewCell", bundle: nil)
        tableView.register(footer, forCellReuseIdentifier: "FooterTableViewCell")
        
        let cell = UINib(nibName: "NotificationTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
    }
    func updateDateby(day:Int) -> String{
        
        let formatter = DateFormatter()
        
        let date = Calendar.current.date(byAdding: .day, value: day, to: Date())!
        
        
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
        
    }
}
//MARK:- TableViewDelegate, Datasource
extension NotificationViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as! NotificationTableViewCell
        let data = self.notifications[indexPath.row]
        cell.setData(data: data)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterTableViewCell") as! FooterTableViewCell
        if self.notifications.count == 0 {
            footerCell.crossTapped.isHidden = true
        }
        footerCell.crossTapped.addTarget(self, action: #selector(reset), for: .touchUpInside)
        return footerCell
    }
    @objc func reset(){
        self.removeAllNotifications()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView.isTracking { return }
        
        cell.contentView.alpha = 0.3
        cell.layer.transform = CATransform3DMakeScale(0.7, 0.7, 0.7)
        
        // Simple Animation
        UIView.animate(withDuration: 0.5) {
            cell.contentView.alpha = 1
            cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationType = self.notifications[indexPath.row].type
        let notificationID = self.notifications[indexPath.row].id
        self.markNotificationsRead(id: notificationID)
        switch notificationType {
        case 2:
            let categoryId = self.notifications[indexPath.row].referenceId
            let categoryTitle = self.notifications[indexPath.row].title ?? "Default Title"
            let VC = CarpetsViewController(nibName: "CarpetsViewController", bundle: nil)
            VC.categoryId = categoryId
            VC.selectedTitle = categoryTitle
            VC.VCType = .detail
            self.navigationController?.pushViewController(VC, animated: true)
        case 1:
            let productId = self.notifications[indexPath.row].referenceId
            self.getProductById(productId: productId)
        default:
            print(0)
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if tableView.isTracking { return }
        
        view.alpha = 0.3
        view.layer.transform = CATransform3DMakeScale(0.7, 0.7, 0.7)
        
        
        // Simple Animation
        UIView.animate(withDuration: 0.5) {
            view.alpha = 1
            view.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

//MARK:- DZNEmptySetDelegate, Datasource
extension NotificationViewController: DZNEmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = NSLocalizedString("No Notification(s)", comment: "")
        let attribs = [NSAttributedStringKey.foregroundColor: UIColor.black]
        return NSAttributedString(string: text, attributes: attribs)
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = ""
        
        let para = NSMutableParagraphStyle()
        para.lineBreakMode = NSLineBreakMode.byWordWrapping
        para.alignment = NSTextAlignment.center
        
        let attribs = [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.paragraphStyle: para
        ]
        
        return NSAttributedString(string: text, attributes: attribs)
    }
}

extension NotificationViewController: DZNEmptyDataSetDelegate {
    func emptyDataSetDidTapButton(_ scrollView: UIScrollView!) {
        // getUserNotificationAPI(removeData: true)
    }
}

//MARK:- Services
extension NotificationViewController{
    private func getNotifications(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let categories = result["response_data"] as? [[String:Any]] else {
                    self.notifications.removeAll()
                    self.tableView.reloadData()
                    return
                }
                self.notifications = Mapper<NotificationModel>().mapArray(JSONArray: categories)
                self.tableView.reloadData()
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        let user = AppStateManager.sharedInstance.loggedInUser
        APIManager.instance.getNotifications(id: "\(user?.userId ?? 0)", success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func markNotificationsRead(id: Int){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.markNotificationsRead(id: "\(id)", success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func removeAllNotifications(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                self.getNotifications()
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        let user = AppStateManager.sharedInstance.loggedInUser
        APIManager.instance.removeAllNotifications(id: "\(user?.userId ?? 0)", success: successClosure, failure: failureClosure, errorPopup: true)
    }
    private func getProductById(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                
                guard let subCategories = result["response_data"] as? [[String:Any]] else {return}
                
                if subCategories.isEmpty{
                    Utility.showAlert(title: Strings.SAMOVAR.localized, message: "Category might be deleted.")
                    return
                }
                
                self.subCategories = Mapper<GetSubCategoriesModel>().map(JSONObject: subCategories.first) ?? GetSubCategoriesModel()
                let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
                dest.categoryDetail = self.subCategories
                self.navigationController?.pushViewController(dest, animated: false)
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.getProductById(id: "\(productId ?? 0)", success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
