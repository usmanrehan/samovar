//
//  NotificationTableViewCell.swift
//  SaloonIK_IOS
//
//  Created by aneelahmed on 11/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var viewIsRead: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//MARK:- HelperMethod
extension NotificationTableViewCell{
     func setData(data: NotificationModel){
         self.lblMessage.text = data.title ?? "Default Title"
         let dateString = data.date ?? ""
         self.lblHour.text = Utility.stringDateFormatter(dateStr: dateString, dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS", formatteddate: "EEEE, MMM d, yyyy")
         self.viewIsRead.isHidden = data.isRead
     }
}
