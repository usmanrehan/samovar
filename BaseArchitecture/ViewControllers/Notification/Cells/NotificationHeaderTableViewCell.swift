//
//  NotificationHeaderTableViewCell.swift
//  SaloonIK_IOS
//
//  Created by aneelahmed on 11/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NotificationHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeading: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
