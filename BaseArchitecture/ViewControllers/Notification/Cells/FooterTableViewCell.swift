//
//  FooterTableViewCell.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 28/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class FooterTableViewCell: UITableViewCell {
    @IBOutlet weak var crossTapped: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
