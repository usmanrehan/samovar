//
//  PreferenceViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 13/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import Device
import Alamofire
import RealmSwift
import Realm
import ObjectMapper
import GoogleSignIn
import FBSDKLoginKit

class PreferenceViewController: BaseViewController {
    
    @IBOutlet weak var lblVersionNumber: UILabel!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet var heightConstraint: [NSLayoutConstraint]!
    
    var isGuest : Bool = false
    var isDismiss : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.lblVersionNumber.text = version()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupTransparentNavBar()
        AppDelegate.shared.orientationLock = .portrait
        self.setGoogleSignInPrerequisites()
        if (isGuest){
            addSkipButton()
        }
        else{
            addSkipButton()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.shared.orientationLock = .all
        
        if (isDismiss){
            //            self.dismiss(animated: false, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sizeAdjustment()
    }
    private func version() -> String {
           let dictionary = Bundle.main.infoDictionary!
           let version = dictionary["CFBundleShortVersionString"] as! String
           let build = dictionary["CFBundleVersion"] as! String
           return "Version: \(version)(\(build))"
       }

    func sizeAdjustment(){
        switch Device.size() {
        case .screen4_7Inch:
            print("iPhone 6")
            setHeight(height: 50.0)
        case .screen5_5Inch:
            setHeight(height: 60.0)
            print("iPhone 6 plus")
        case .screen5_8Inch:
            setHeight(height: 60.0)
            print("iPhone X")
        default:
            setHeight(height: 60.0)
            print("iPhone X Max")
        }
    }
    func setHeight(height: CGFloat){
        for item in heightConstraint{
            item.constant = height
        }
    }
    @IBAction func loginWithFacebookTapped(_ sender: Any) {
//        self.signInWithFaceBook()
        Utility.showAlert(title: Strings.SAMOVAR.localized, message: "Will be implemented later")
    }
    @IBAction func loginWithGoogleTapped(_ sender: Any) {
//        self.signInWithGoogle()
        Utility.showAlert(title: Strings.SAMOVAR.localized, message: "Will be implemented later")
    }
    @IBAction func signUpTapped(_ sender: Any) {
        let signInVC = SignInViewController(nibName: "SignInViewController", bundle: nil)
        self.navigationController?.pushViewController(signInVC, animated: false)
    }
    @IBAction func signinTapped(_ sender: Any) {
        let signInVC = SignupViewController(nibName: "SignupViewController", bundle: nil)
        self.navigationController?.pushViewController(signInVC, animated: false)
    }
    
    private func signInWithGoogle(){
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    private func setGoogleSignInPrerequisites(){
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    private func signInWithFaceBook(){
           let loginManager = LoginManager()
           loginManager.logIn(permissions: ["public_profile"], from: self, handler: { result, error in

               guard let result = result else {
                   print("No result found")
                   return
               }
               if result.isCancelled {
                   print("Cancelled \(error?.localizedDescription ?? "")")
               } else if let error = error {
                   print("Process error \(error.localizedDescription)")
               } else {
                   print("Logged in")
                   self.getFBUserData()
               }
           })
       }
    public func getFBUserData(){
        if AccessToken.current != nil{
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    guard let res = result as? NSDictionary else {return}
                    let user_name = (res["name"] as? String) ?? ""
                    let provider_key = (res["id"] as? String) ?? ""
                    var profile_image = ""
                    if let profileImage = res["picture"] as? NSDictionary{
                        if let data = profileImage["data"] as? NSDictionary{
                            if let url = data["url"] as? String{
                                profile_image = url
                            }
                        }
                    }
                    let email_address = (res["email"] as? String) ?? ""
                    let device_token = Constants.deviceToken
                    let social_provider_id = 1

                    
                    var params:[String:Any] = ["provider_key":provider_key,
                                               "device_token":device_token,
                                               "social_provider_id": social_provider_id]
                    if !((res["name"] as? String) ?? "").isEmpty{
                        params["user_name"] = user_name
                    }
                    if !profile_image.isEmpty{
                        params["profile_image"] = profile_image
                    }
                    if !((res["email"] as? String) ?? "").isEmpty{
                        params["email_address"] = email_address
                    }

                    self.socialLogin(params: params)
                }
                else{
                    let errorMessage = error?.localizedDescription ?? "error"
                    print(errorMessage)
                }
                Utility.stopLoading()
            })
        }
    }
    
}

//MARK: - Google Signin
extension PreferenceViewController: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let user_name = user.profile.name ?? ""
            let provider_key = user.userID ?? ""
            let profile_image = user.profile.imageURL(withDimension: 200)?.absoluteString ?? ""
            let email_address = user.profile.email ?? ""
            let device_token = Constants.deviceToken
            let social_provider_id = 2
            
            var params:[String:Any] = ["provider_key":provider_key,
                                       "device_token":device_token,
                                       "social_provider_id": social_provider_id]
            
            print(params)
            
            if !(user.profile.name).isEmpty{
                params["user_name"] = user_name
            }
            if !(user.profile.imageURL(withDimension: 200)?.absoluteString ?? "").isEmpty{
                params["profile_image"] = profile_image
            }
            if !(user.profile.email ?? "").isEmpty{
                params["email_address"] = email_address
            }

            
            self.socialLogin(params: params)
        } else {
            print("\(error.localizedDescription)")
            let errorMessage = error.localizedDescription
            print(errorMessage)
            //            Utility.main.showAlert(message: errorMessage, title: Strings.ERROR.text)
        }
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
              withError error: Error!) {
    }
}

//Mark:- Services
extension PreferenceViewController {
    func socialLogin(params: [String:Any]){
        print(params)
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let userProfile = result["response_data"] as? [[String:Any]] else {return}
                let user = Mapper<UserLogin>().mapArray(JSONArray: userProfile)
                
                Constants.token = user.first?.auth_token ?? ""
                user.first?.isSocialLogin = 1
                AppStateManager.sharedInstance.loggedInUser = user.first
                
                self.isDismiss = true
                try! Global.APP_REALM?.write(){
                        print("Realm Writed")
                        Global.APP_REALM?.add(AppStateManager.sharedInstance.loggedInUser!, update: .all)
                    }
                    let VC = HomeOverlayController()
                    self.navigationController?.pushViewController(VC, animated: false)
                    AppStateManager.sharedInstance.changeRootViewController()
            }else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        print(params)
        APIManager.instance.socialLogin(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
