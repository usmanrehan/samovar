//
//  Settings2TableViewCell.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

class Settings2TableViewCell: UITableViewCell {

    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var title: UILabel!
    var selectedIndex  = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
