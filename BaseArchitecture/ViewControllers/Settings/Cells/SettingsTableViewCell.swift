//
//  SettingsTableViewCell.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit

protocol SettingsDelegate {
    func toggleTapped(sender : UIButton , index : Int)
}

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var btnToggle: UISwitch!
    @IBOutlet weak var detail: UILabel!
    @IBOutlet weak var title: UILabel!
    var delegate : SettingsDelegate?
    var selectedIndex  = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
