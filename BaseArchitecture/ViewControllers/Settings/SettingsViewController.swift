//
//  SettingsViewController.swift
//  BaseArchitecture
//
//  Created by Hassan Dad Khan on 26/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import ObjectMapper

class SettingsViewController: BaseViewController {
    
    @IBOutlet weak var editMAinView: UIView!
    @IBOutlet weak var editView: EditProfileView!
    @IBOutlet weak var tableView: UITableView!
    
    var alertController : UIAlertController!
    var saveAction: UIAlertAction!
    var cancelAction : UIAlertAction!
    
    var titleArray = [
        "Name",
        "Mobile Number",
        "Email",
        "Password",
        "Notifications",
        "Privacy Policy",
        "Terms of Service",
        "Clear Cache",
        "Clear Search History",
        "Logout"
    ]
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        viewEssentails()
        editMAinView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.6)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK:- Helper Method
extension SettingsViewController{
    func viewEssentails()  {
        tableViewEssetials()
        setupNavigation ()
    }
    func setupNavigation () {
        self.setupColorNavBar()
        self.addBackButtonToNavigationBar()
        title = Strings.SETTINGS.localized
    }
    func tableViewEssetials(){
        let settingCell1 = UINib(nibName: "SettingsTableViewCell", bundle: nil)
        tableView.register(settingCell1, forCellReuseIdentifier: "SettingsTableViewCell")
        let settingCell2 = UINib(nibName: "Settings2TableViewCell", bundle: nil)
        tableView.register(settingCell2, forCellReuseIdentifier: "Settings2TableViewCell")
        let SettingsHeader = UINib(nibName: "SettingsHeaderTableViewCell", bundle: nil)
        tableView.register(SettingsHeader, forCellReuseIdentifier: "SettingsHeaderTableViewCell")
        
        self.tableView.contentInset = UIEdgeInsetsMake(-24, 0, 0, 0);
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
    }
    func logoutAlert()  {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to logout?", preferredStyle: .alert)
        let no = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction) in
        }
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (action:UIAlertAction) in
            AppStateManager.sharedInstance.markUserLogout()
        }
        alertController.addAction(yes)
        alertController.addAction(no)
        self.present(alertController, animated: true, completion: nil)
    }
    func editAlert (alertTitle : String ,textFeildName : String, placeholder : String, text : String) {
        alertController = UIAlertController(title: alertTitle, message: textFeildName, preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            if placeholder == "Name" {
                textField.placeholder = placeholder
                textField.text = text
                
            } else {
                textField.placeholder = placeholder
                textField.text = text
                textField.keyboardType = UIKeyboardType.numberPad
            }
            textField.addTarget(self, action: #selector(self.didChangeTextFieldText(_:)), for: .editingChanged)
        }
        saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { alert -> Void in
            let firstTextField = self.alertController.textFields![0] as UITextField
            if let value = firstTextField.text {
                if firstTextField.placeholder == "Name" {
                    self.editProfile(userName : value, Phone : AppStateManager.sharedInstance.loggedInUser.mobile_number ?? "")
                } else {
                    self.editProfile(userName : AppStateManager.sharedInstance.loggedInUser.user_name ?? "", Phone : value)
                }
            }
        })
        cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        saveAction.isEnabled = false
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func didChangeTextFieldText(_ textField: UITextField) {
        if !textField.text!.isBlank() {
            saveAction.isEnabled = true
        } else {
            saveAction.isEnabled = false
        }
    }
    
}
extension SettingsViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return titleArray.count
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "SettingsHeaderTableViewCell") as! SettingsHeaderTableViewCell
        
        return headerCell.contentView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3{
            if (AppStateManager.sharedInstance.loggedInUser.isExternalLogin){
                return 0
            }
            return 44
        }
        if indexPath.row == 8 {
            return 0
        }
        return 44
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
            cell.selectedIndex = indexPath.row
            cell.delegate = self
            cell.title.text = titleArray[indexPath.row]
            
            if indexPath.row == 0 {
                cell.detail.text = AppStateManager.sharedInstance.loggedInUser.user_name
            } else if indexPath.row == 1 {
                if AppStateManager.sharedInstance.loggedInUser.mobile_number == ""{
                    cell.detail.text = "N/A" 
                }
                else{
                    cell.detail.text = AppStateManager.sharedInstance.loggedInUser.mobile_number
                }
                
            } else if indexPath.row == 2 {
                cell.detail.text = AppStateManager.sharedInstance.loggedInUser.email
            }
            if !(indexPath.row == 4) {
                cell.btnToggle.isHidden  = true
                cell.detail.isHidden = false
            } else {
                cell.btnToggle.isHidden  = false
                cell.btnToggle.addTarget(self, action: #selector(onBtnNotification(_:)), for: .touchUpInside)
                cell.detail.isHidden = true
            }
            return cell
        }
            
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Settings2TableViewCell") as! Settings2TableViewCell
            cell.selectedIndex = indexPath.row
            cell.title.text = titleArray[indexPath.row]
            if indexPath.row == 3{
                if (AppStateManager.sharedInstance.loggedInUser.isExternalLogin){
                    cell.isHidden = true
                }
            }
            if indexPath.row == 7 {
                cell.detail.isHidden = false
                let imagesArray = AppStateManager.sharedInstance.loggedInUser.arrFiles
                var imagesTotalSize = 0
                for image in imagesArray{
                    imagesTotalSize = imagesTotalSize + (image.fileData?.count ?? 0)
                }
                let sizeImages = (Double(imagesTotalSize) / 1000.0)/1024
                let roundedValue1 = NSString(format: "%.2f", sizeImages)
                cell.detail.text = "\((roundedValue1)) MB"
            } else {
                cell.detail.isHidden = true
            }
            return cell
        }
    }
    @objc func onBtnNotification(_ sender: UISwitch){
        if sender.isOn{
            try! Global.APP_REALM?.write(){
                print("Realm Writed")
                let user = AppStateManager.sharedInstance.loggedInUser
                user?.allowLocalNotification = true
                Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
            }
        }else{
            try! Global.APP_REALM?.write(){
                print("Realm Writed")
                let user = AppStateManager.sharedInstance.loggedInUser
                user?.allowLocalNotification = false
                Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
            }
        }
    }
    func showEditView(){
        UIView.animate(withDuration: 0.3) {
            self.editMAinView.isHidden = false
        }
    }
    func removeEditView(){
        self.view.endEditing(true)
        editView.textField.resetError()
        UIView.animate(withDuration: 0.3) {
            self.editMAinView.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            editView.titleLabel.text = titleArray[indexPath.row]
            editView.textField.textField.placeholder = titleArray[indexPath.row]
            editView.textField.textField.keyboardType = .default
            editView.textField.textField.text = AppStateManager.sharedInstance.loggedInUser.user_name ?? ""
            editView.setUpNameValidation()
            editView.delegate = self
            showEditView()
            //            editView.showAsPopup(Options(), with: 280.0)
            
        case 1:
            editView.titleLabel.text = titleArray[indexPath.row]
            editView.textField.textField.placeholder = titleArray[indexPath.row]
            editView.textField.textField.keyboardType = .numberPad
            editView.textField.textField.text = AppStateManager.sharedInstance.loggedInUser.mobile_number ?? ""
            editView.setupPhoneValidation()
            editView.delegate = self
            showEditView()
            //            editView.showAsPopup(Options(), with: 280.0)
            
        case 5:
            let dest = PrivacyPolicyViewController(nibName: "PrivacyPolicyViewController", bundle: nil)
            dest.selectedTitle = titleArray[indexPath.row]
            self.navigationController?.pushViewController(dest, animated: true)
        case 6:
            let dest = PrivacyPolicyViewController(nibName: "PrivacyPolicyViewController", bundle: nil)
            dest.selectedTitle = titleArray[indexPath.row]
            self.navigationController?.pushViewController(dest, animated: true)
        case 3:
            if AppStateManager.sharedInstance.loggedInUser.isSocialLogin != 1{
                let dest = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
                self.navigationController?.pushViewController(dest, animated: true)
            }
        case 4:
            print(indexPath.row)
        case 7:
            let alertController = UIAlertController(title: nil, message: "Are you sure you want to clear cache?", preferredStyle: .alert)
            let no = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction) in
            }
            let yes = UIAlertAction(title: "Yes", style: .destructive) { (action:UIAlertAction) in
                try! Global.APP_REALM?.write(){
                    print("Realm Writed")
                    let user = AppStateManager.sharedInstance.loggedInUser
                    let arrayFiles = List<AttachmentsModel>()
                    user?.arrFiles = arrayFiles
                    AppStateManager.sharedInstance.loggedInUser = user
                    Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
                    self.tableView.reloadData()
                }
            }
            alertController.addAction(yes)
            alertController.addAction(no)
            self.present(alertController, animated: true, completion: nil)
        case 9:
            logoutAlert()
        case 8:
            Utility.showAlert(title: Strings.ERROR.localized, message: "This feature will be implemented later.")
        default:
            print("default")
        }
    }
}
extension SettingsViewController : SettingsDelegate {
    func toggleTapped(sender: UIButton, index: Int) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "toggleoff"), for: .normal)
        } else {
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "toggleon"), for: .normal)
        }
    }
}
extension SettingsViewController: EditProfileDelegate{
    func saveTapped(value: String, isName: Bool) {
        removeEditView()
        if (isName){
            if value == AppStateManager.sharedInstance.loggedInUser.user_name{
                return
            }
            self.editProfile(userName : value, Phone : AppStateManager.sharedInstance.loggedInUser.mobile_number ?? "")
        } else {
            if value == AppStateManager.sharedInstance.loggedInUser.mobile_number{
                return
            }
            self.editProfile(userName : AppStateManager.sharedInstance.loggedInUser.user_name ?? "", Phone : value)
        }
    }
    func cancelTapped() {
        removeEditView()
    }
}

//MARK:- Services
extension SettingsViewController{
    private func editProfile(userName : String, Phone : String){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["user_name": userName,
                                      "email": AppStateManager.sharedInstance.loggedInUser.email ?? "",
                                      "mobile_number": Phone]
        print(parameters)
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                if let resultString = result["message_to_show"] as? String{
                    Utility.showAlert(title: Strings.SUCCESS.localized, message: resultString)
                    let currentToken = token(value : AppStateManager.sharedInstance.loggedInUser.Token!)
                    try! Global.APP_REALM?.write(){
                        print("Realm Writed")
                        let user = AppStateManager.sharedInstance.loggedInUser
                        user?.Token = currentToken
                        user?.user_name = userName
                        user?.mobile_number = Phone
                        Global.APP_REALM?.add(user ?? UserLogin(), update: .all)
                    }
                    self.tableView.reloadData()
                }else {
                    if result["Result"] != nil {
                        Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
                    } else {
                        Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                    }
                }
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.editProfile(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}

