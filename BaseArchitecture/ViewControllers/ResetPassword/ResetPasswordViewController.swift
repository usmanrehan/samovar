//
//  ResetPasswordViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 28/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
//import SwiftValidator
import Alamofire

class ResetPasswordViewController: BaseViewController {
    @IBOutlet weak var passwordField: IGTextField!
    @IBOutlet weak var confPassField: IGTextField!
    
    var email : String!
    let validator = Validator()
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        setUpValidations()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = Strings.RESET_PASSWORD.localized
        navigationController?.navigationBar.topItem?.title = Strings.RESET_PASSWORD.localized
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        self.resetValidators()
        validator.validate(delegate: self)
    }
}

//MARK:- Helper Methods
extension ResetPasswordViewController{
    func setUpValidations(){
        validator.registerField(
            textField: passwordField.textField,
            errorLabel: passwordField.errorLabel,
            rules:[RequiredRule(message: Strings.FIELD_REQUIRED.localized),
                   MinLengthRule(length: 6, message: Strings.PASSWORD_TOO_SHORT.localized)
            ]
        )
        validator.registerField(
            textField: confPassField.textField,
            errorLabel: confPassField.errorLabel,
            rules: [
                RequiredRule(message: Strings.FIELD_REQUIRED.localized),
                ConfirmationRule(
                    confirmField:passwordField.textField,
                    message: Strings.PWD_NOT_MATCH.localized
                )
            ]
        )
    }
    func resetValidators(){
        self.passwordField.resetError()
        self.confPassField.resetError()
    }
}

//MARK:- Validation Delegate
extension ResetPasswordViewController: ValidationDelegate {
    func validationSuccessful() {
        if INTERNET_IS_RUNNING{
            resetCode()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
        }
    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        for (_, error) in errors {
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:- Services
extension ResetPasswordViewController{
    private func resetCode(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.INTERNECT_CONNECTION_SEEMS_TO_BE_OFFLINE.localized)
            return
        }
        Utility.startLoading()
        let parameters: Parameters = ["Email": self.email!,
                                      "NewPassword": passwordField.textField.text!,
                                      "Password": confPassField.textField.text!]
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                print(result)
                Utility.showAlert(title: Strings.SUCCESS.localized, message: Strings.PWD_CHANGED_SUCCESS.localized)
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: SignInViewController.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
            else if(response.status_code == 404) {
                Utility.showAlert(title: Strings.error.localized, message: Strings.ENTERED_CODE_INVALID.localized)
            }
            else{
                
                if result["Result"] != nil {
                    Utility.showAlert(title: Strings.error.localized, message: Utility.getErrorMessage(result))
                } else {
                    Utility.showAlert(title: Strings.error.localized, message: Utility.getErrorMessage1(result))
                }
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: Strings.SOMETHING_WENT_WRONG.localized)
            Utility.stopLoading()
        }
        APIManager.instance.resetForgotPassword(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
    }
}
