
import UIKit
import Device
import NVActivityIndicatorView
import RESideMenu
import SkeletonView

protocol ACBaseViewControllerSearchDelegate {
    func searchWith(term: String)
    func cancelSearch()
}


class BaseViewController: UIViewController, NVActivityIndicatorViewable {

    var searchButton: UIBarButtonItem!
    var searchDelegate: ACBaseViewControllerSearchDelegate?
    var searchBar = UISearchBar()
    
    var leftSearchBarButtonItem : UIBarButtonItem?
    var rightSearchBarButtonItem : UIBarButtonItem?
    var cancelSearchBarButtonItem: UIBarButtonItem?
    
    override func viewDidLoad() {
       super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
       navigationItem.setHidesBackButton(true, animated: false)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Loaders and error Messages
extension BaseViewController {
    func startSkeletonAnimation(){
        view.startSkeletonAnimation()
    }
    func stopSkeletonAnimation(){
        view.stopSkeletonAnimation()
    }
    
}

//MARK:- NavigationBar changes
extension BaseViewController {
    func setupColorNavBar(){
        let font = FontName.semiBold.font(withSize: 18)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: font,NSAttributedStringKey.foregroundColor:Constants.appTextColor ]
       // navigationController?.navigationBar.barStyle = .black
        
        
        
        UIApplication.shared.statusBarStyle = .default
        
        navigationItem.leftBarButtonItem = nil
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
       // navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor =  UIColor.white
        navigationController?.navigationBar.tintColor = Constants.appTextColor//UIColor.black
    }
    
    
    
    func setupTransparentNavBar(){
        let font = FontName.ralewayMedium.font(withSize: 18)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: font,NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        
        UIApplication.shared.statusBarStyle = .default
        
        navigationItem.leftBarButtonItem = nil
        
        navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
    }
}

//MARK:- Views Addition
extension BaseViewController {
    
    func addBackButtonToNavigationBar(){
        leftSearchBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "back_icon"),
                                                   style: UIBarButtonItemStyle.plain,
                                                   target: self,
                                                   action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftSearchBarButtonItem;
        
    }
    func addCrossButton(){
        leftSearchBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "cross"),
                                                   style: UIBarButtonItemStyle.plain,
                                                   target: self,
                                                   action: #selector(crossTapped))
        navigationItem.leftBarButtonItem = leftSearchBarButtonItem;
    }
    func addFavButton(){
        rightSearchBarButtonItem =  UIBarButtonItem(image: #imageLiteral(resourceName: "star"),
                                                   style: UIBarButtonItemStyle.plain,
                                                   target: self,
                                                   action: #selector(favTapped(sender :)))
        navigationItem.rightBarButtonItem = rightSearchBarButtonItem;
    }
    @objc func crossTapped(){
       self.dismiss(animated: true, completion: nil)
    }
    @objc func favTapped(sender : UIBarButtonItem){
        
    }
    func setFavButton (value : Bool){
        if value {
             navigationItem.rightBarButtonItem?.image =  UIImage.init(named: "heartfill")
            navigationItem.rightBarButtonItem?.tintColor = UIColor.init(displayP3Red: 240/255.0, green: 203/255.0, blue: 46/255.0 , alpha: 1)
        } else {
             navigationItem.rightBarButtonItem?.image = UIImage.init(named: "heart")
            navigationItem.rightBarButtonItem?.tintColor = UIColor.init(displayP3Red: 49/255.0, green: 56/255.0, blue: 96/255.0 , alpha: 1)
        }
    }
    func addSkipButton(){
        rightSearchBarButtonItem = UIBarButtonItem(title: "Skip", style: .plain, target: self, action: #selector(skipTapped))
        rightSearchBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor :Constants.appTextColor,NSAttributedStringKey.font:UIFont.init(name:"Poppins-Regular", size:17.0)!], for: UIControlState.normal)
        navigationItem.rightBarButtonItem = rightSearchBarButtonItem
    }
   
    @objc func skipTapped(){
//        HomeViewController
//        let signInVC = SavedImagesViewController(nibName: "SavedImagesViewController", bundle: nil)
//        self.navigationController?.pushViewController(signInVC, animated: false)
        
//        let carpetsView = HomeViewController(nibName: "HomeViewController", bundle: nil)
         let carpetsView = HomeOverlayController()
        self.navigationController?.pushViewController(carpetsView, animated: false)
        
//        let signInVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
//        self.navigationController?.pushViewController(signInVC, animated: false)

    }
    
    func addMenuButtonToNavigationBar(){
        leftSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "side_menu"),
                                                   style: UIBarButtonItemStyle.plain,
                                                   target: self,
                                                   action: #selector(showSideMenu))
        
        navigationItem.leftBarButtonItem = leftSearchBarButtonItem;
    }
    
    
    func addSearchButtonToNavigationBar(){
        rightSearchBarButtonItem =  UIBarButtonItem(image: UIImage.init(named: "search"),
                                                    style: UIBarButtonItemStyle.plain,
                                                    target: self,
                                                    action: #selector(doSearch))
        navigationItem.rightBarButtonItem = rightSearchBarButtonItem;
    }
}

//MARK:- Button Handlers
extension BaseViewController {
    @objc func goBack(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func doSearch(){
        showSearchbar()
    }
    
    @objc func showSideMenu(){
        sideMenuViewController.presentLeftMenuViewController()
    }
}

//MARK:- Searchbar Handlers
extension BaseViewController {
    func activateInitialSetupUI(){
        searchBar.delegate = self
        searchBar.placeholder = "Search"
        searchBar.searchBarStyle = .minimal
        searchBar.showsCancelButton = true
    }
    
    func showSearchbar(){
        searchBar.alpha = 0
        searchBar.isHidden = false
        searchBar.tintColor = UIColor.white
        searchBar.barTintColor = UIColor.white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.white
        
        searchBar.setImage( UIImage.init(named: "search"), for: UISearchBarIcon.search, state: UIControlState.normal)
        
        
        navigationItem.titleView = searchBar
        navigationItem.setRightBarButton(nil, animated: true)
        navigationItem.setLeftBarButton(nil, animated: true)
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar.alpha = 1;
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar(){
        
        navigationItem.setRightBarButton(rightSearchBarButtonItem, animated: true)
        navigationItem.setLeftBarButton(leftSearchBarButtonItem, animated: true)
        
        let navigationTitlelabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        navigationTitlelabel.center = CGPoint(x: 160, y: 284)
        navigationTitlelabel.textAlignment = NSTextAlignment.center
        navigationTitlelabel.textColor  = UIColor.white
        navigationTitlelabel.text = "Car Stock"
        navigationTitlelabel.font = FontName.ralewayLight.font(withSize: 18)
        
        searchBar.text = ""
        
        navigationController!.navigationBar.topItem!.titleView = navigationTitlelabel
        
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar.alpha = 0;
        }, completion: { finished in
            self.searchBar.isHidden = true
        })
    }
    
}

//MARK:- UISearchBarDelegate
extension BaseViewController: UISearchBarDelegate{


    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        if(searchDelegate != nil){
            searchDelegate?.searchWith(term: searchBar.text!)
        }
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        navigationItem.setHidesBackButton(true, animated: false)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if(searchDelegate != nil){
            searchDelegate?.cancelSearch()
        }

        hideSearchBar()
    }
    
}
