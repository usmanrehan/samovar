//
//  ShareVC.swift
//  
//
//  Created by Abdul  Karim Khan on 06/05/2020.
//

import UIKit

class ShareVC: BaseViewController {

    var images = [UIImage]()
    var shouldResumeARRendering: ((Bool)->Void)?
    
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait        
    }
    
    @IBAction func onBtnDismiss(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.shouldResumeARRendering?(true)
        }
    }
    @IBAction func onBtnSocail(_ sender: UIButton) {
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: self.images, applicationActivities: nil)
            Utility.topViewController()?.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func onBtnMail(_ sender: UIButton) {
        self.openEmail(email: "samovar@ingic.com")
    }
    
    private func openEmail(email: String) {
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
                
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
