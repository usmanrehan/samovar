//
//  ForgotOneViewController.swift
//  BaseArchitecture
//
//  Created by zaidtayyab on 28/02/2019.
//  Copyright © 2019 Ingic. All rights reserved.
//

import UIKit
//import SwiftValidator
import Realm
import RealmSwift
import Alamofire


class ForgotOneViewController: BaseViewController {

    @IBOutlet weak var emailField: IGTextField!
    let validator = Validator()
    override func viewDidLoad() {
        AppDelegate.shared.orientationLock = .portrait
        super.viewDidLoad()
        setupColorNavBar()
        addBackButtonToNavigationBar()
        setUpValidation()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "EMAIL VERIFICATION"
    }
    func setUpValidation(){
        self.emailField.textField.keyboardType = .emailAddress
        validator.registerField(textField: emailField.textField,errorLabel: emailField.errorLabel, rules: [RequiredRule(message: NSLocalizedString("This field is required", comment: "")), EmailRule(message: NSLocalizedString("Please Enter a valid Email Address", comment: ""))])
    }
    func resetValidators(){
        self.emailField.resetError()
    }
    @IBAction func continueTapped(_ sender: Any) {
//        ForgotTwoViewController
        self.resetValidators()
        
        validator.validate(delegate: self)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func checkEmail(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        
        Utility.startLoading()
        //            self.startAnimating()
        
        let parameters: Parameters = ["email":emailField.textField.text!]
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                print(result)
                Utility.showAlert(title: Strings.SUCCESS.localized, message: "Code sent successfully")
                let forgotVC = ForgotTwoViewController(nibName: "ForgotTwoViewController", bundle: nil)
                forgotVC.email = self.emailField.textField.text!
                self.navigationController?.pushViewController(forgotVC, animated: true)
            }
            else if (response.status_code == 403){
                Utility.showAlert(title: Strings.ERROR.localized, message: "Provided Email Address is not registered")
            }
            else{
                
                if result["Result"] != nil {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
                } else {
                    Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
                }
                
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.ERROR.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        APIManager.instance.verifyEmail(parameters:parameters, success: successClosure, failure: failureClosure, errorPopup: true)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ForgotOneViewController: ValidationDelegate {
    
    func validationSuccessful() {
        
        if INTERNET_IS_RUNNING{
//            Utility.startLoading()
            self.forgetPassword()
        }else{
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
        }
//        let forgotVC = ForgotTwoViewController(nibName: "ForgotTwoViewController", bundle: nil)
//        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    
    
    func validationFailed(errors:[UITextField:ValidationError]) {
        // turn the fields to red
        
        for (_, error) in errors {
            // if let field = field as? UITextField {
            // field.layer.borderColor = UIColor.red.cgColor
            //   field.layer.borderWidth = 1.0
            // }
            //   DDLogError(error.errorMessage)
            
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
}

//MARK:-
extension ForgotOneViewController{
    func forgetPassword(){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        
        Utility.startLoading()
        //            self.startAnimating()
        
        
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                let alertController = UIAlertController(title: Strings.SAMOVAR.localized, message:  Utility.getErrorMessage(result), preferredStyle: .alert)
                let yes = UIAlertAction(title: "OK", style: .destructive) { (action:UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(yes)
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        
        APIManager.instance.forgetPassword(id: "\(self.emailField.textField.text ?? "")", success: successClosure, failure: failureClosure, errorPopup: true)
        }
}
