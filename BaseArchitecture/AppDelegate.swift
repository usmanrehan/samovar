
import UIKit
import RESideMenu
import IQKeyboardManagerSwift
//import FacebookCore
//import FacebookLogin
import FBSDKLoginKit

import GoogleSignIn
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Realm
import RealmSwift
import UserNotifications
import ObjectMapper

struct NotificationPayload {
    var NotificationType = ""
    var ReferenceId = ""
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    static let shared: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var window: UIWindow?
    var isFirstTime = true
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    var subCategories = GetSubCategoriesModel()
    
    let notificationCenter = UNUserNotificationCenter.current()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        setupUX()
        startNetworkListener()
        
        window?.makeKeyAndVisible();
        
        IQKeyboardManager.shared.enable = true
        //        IQKeyboardManager.shared.disabledToolbarClasses = [SettingsViewController.self]
        //        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [SettingsViewController.self]
        //        IQKeyboardManager.shared.igno
        //        migrateRealm(version: 0)
        //        FirebaseApp.configure()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = AppConfig.shared.googleClientId
        self.setupPushNotifications(application: application)

        return true
    }
    
    func migrateRealm(version : Int)  {
        
        let config = Realm.Configuration(
            schemaVersion: UInt64(version),  // Must be greater than previous version
            migrationBlock: { migration, oldSchemaVersion in
                print("Realm migration did run")  // Log to know migration was executed
        })
        
        // Make sure to set the default configuration
        Realm.Configuration.defaultConfiguration = config
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return  (GIDSignIn.sharedInstance()?.handle(url) ?? false) || ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        guard let controller = Utility.topViewController() as? ARViewer else {return}
        controller.sceneView.session.pause()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        guard let controller = Utility.topViewController() as? ARViewer else {return}
        controller.sceneView.session.run(controller.configuration)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

//MARK:- Helper Methods
extension AppDelegate {
    func setupUX(){
        if AppStateManager.sharedInstance.isUserLoggedIn() {
            loadHomePage()
        }else{
            loadUserOnBoarding()
        }
    }
    
    func loadUserOnBoarding(){
        let navigationController = UINavigationController()
        let landingPage = PreferenceViewController(nibName: "PreferenceViewController", bundle: nil)
        navigationController.viewControllers = [landingPage]
        window?.rootViewController = navigationController
    }
    
    
    func loadHomePage(){
        let landingPage = HomeOverlayController()
        let navigationController = UINavigationController()
        //        let landingPage = HomeViewController(nibName: "HomeOverlayController", bundle: nil)
        navigationController.viewControllers = [landingPage]
        window?.rootViewController = navigationController
    }
    
    func loadLoginUser(){
        let navigationController = UINavigationController()
        let landingPage = SignInViewController(nibName: "SignInViewController", bundle: nil)
        navigationController.viewControllers = [landingPage]
        window?.rootViewController = navigationController
    }
    
    
    fileprivate func startNetworkListener(){
        
        if !(AppStateManager.reachability?.isReachable)!{
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
        }
        
        AppStateManager.reachability?.startListening()
        AppStateManager.reachability?.listener = { status in
            INTERNET_IS_RUNNING =  (AppStateManager.reachability?.isReachable)!
            switch status {
                
            case .reachable(.ethernetOrWiFi):
                if self.isFirstTime {
                    self.isFirstTime = false
                    return
                }
                //                Utility.hideInternetErrorWith(message: "You are online!")
                
                
            case .reachable(.wwan):
                if self.isFirstTime {
                    self.isFirstTime = false
                    return
                }
                //                Utility.hideInternetErrorWith(message: "You are online!")
                print("")
            case .notReachable:
                Utility.showAlert(title: Strings.ERROR.localized, message: ErrorMessage.network.noNetwork)
                
            case .unknown :
                print("-> It is unknown whether the network is reachable")
                
            }
        }
    }
    
    
}


//MARK:- Push notifications
extension AppDelegate{
    private func setupPushNotifications(application: UIApplication){
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        self.notificationCenter.delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        application.registerForRemoteNotifications()
    }
}
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Constants.deviceToken = fcmToken
        self.refreshDeviceToken()
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if !AppStateManager.sharedInstance.isUserLoggedIn() {return}
        if !AppStateManager.sharedInstance.loggedInUser.allowLocalNotification {return}
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        if !AppStateManager.sharedInstance.isUserLoggedIn() {return}
        completionHandler()
        let userInfo = response.notification.request.content.userInfo
        let NotificationType  = userInfo["NotificationType"] as? String ?? ""
        let ReferenceId = userInfo["ReferenceId"] as? String ?? ""
        let payload = NotificationPayload(NotificationType: NotificationType, ReferenceId: ReferenceId)
        self.handleReceivedNotifications(payload: payload)
    }
    
}


//MARK:- Services
extension AppDelegate{
    func refreshDeviceToken(){
        if !AppStateManager.sharedInstance.isUserLoggedIn() {return}
        
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        
        //            self.startAnimating()
        
        
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
//                Utility.showAlert(title: Strings.SUCCESS.localized, message: Utility.getErrorMessage(result))
//                guard let userProfile = result["response_data"] as? [[String:Any]] else {return}
                
                
            }
            else{
//                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage1(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
        }
        
        let user_id = AppStateManager.sharedInstance.loggedInUser.userId
        
        let params: [String:Any] = ["device_token": Constants.deviceToken,
                                    "user_id": user_id,
                                    "platform_id": 1]
        
        print(params)
        APIManager.instance.refreshDeviceToken(parameters: params, success: successClosure, failure: failureClosure, errorPopup: true)
    }
    func getProductById(productId: Int?){
        if !(INTERNET_IS_RUNNING) {
            Utility.showAlert(title: Strings.ERROR.localized, message: "Internet connection seems to be offline")
            return
        }
        
        Utility.startLoading()
        //            self.startAnimating()
        
        
        let successClosure: DefaultAPISuccessClosure = {
            (result) in
            Utility.stopLoading()
            print(result)
            let response = GeneralResponse(value : result)
            if(response.status_code == 200) {
                guard let subCategories = result["response_data"] as? [[String:Any]] else {return}
                self.subCategories = Mapper<GetSubCategoriesModel>().map(JSONObject: subCategories.first) ?? GetSubCategoriesModel()
                let dest = CarpetsDetailViewController(nibName: "CarpetsDetailViewController", bundle: nil)
                dest.categoryDetail = self.subCategories
                Utility.topViewController()?.navigationController?.pushViewController(dest, animated: true)
            }
            else{
                Utility.showAlert(title: Strings.ERROR.localized, message: Utility.getErrorMessage(result))
            }
            
        }
        let failureClosure: DefaultAPIFailureClosure = {
            (error) in
            print(error)
            Utility.showAlert(title: Strings.error.localized, message: "Something went wrong. Please try again later.")
            Utility.stopLoading()
        }
        
        let user = AppStateManager.sharedInstance.loggedInUser
        
        APIManager.instance.getProductById(id: "\(productId ?? 0)", success: successClosure, failure: failureClosure, errorPopup: true)
    }
    
}

//MARK:- Helper Method
extension AppDelegate{
    private func handleReceivedNotifications(payload: NotificationPayload){
        let notificationType = payload.NotificationType
        let referenceId = Int(payload.ReferenceId)
        switch notificationType {
        case "2":
            let VC = CarpetsViewController(nibName: "CarpetsViewController", bundle: nil)
            VC.categoryId = referenceId
            VC.VCType = .detail
            Utility.topViewController()?.navigationController?.pushViewController(VC, animated: true)
        case "1":
            self.getProductById(productId: referenceId)
        default:
            print(0)
        }
    }
}

