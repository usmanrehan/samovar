
import UIKit

enum Background{
    
    case blur
    
    case light
    
    case dark
    
}

struct Options{
    
    var backgroundType : Background? = .light
    
    var backgroundTapDismisal: Bool? = true
    
    var cornerRadius: CGFloat? = 5.0
    
    init() {}
    
}

extension  UIView {
    
    fileprivate var parentWindow : UIWindow!{
        
        get{
            return UIApplication.shared.windows.first
        } 
        
    }
    func removePopUp(){
        self.dismiss()
    }
    func showAsPopup(_ options: Options? = Options(), with height: CGFloat? = -1)
    {
        
       let HEIGHT = height == -1 ? ( CGFloat(self.subviews[0].subviews.map( { $0.frame.size.height } ).max()!) +  CGFloat(self.subviews[0].subviews.map( { $0.frame.origin.y } ).max()!)) : height
        
        self.handleBackgroundTap((options?.backgroundTapDismisal!)!)
        
        let VERTICAL_SPACING : CGFloat = 40.0
        
        let width = UIScreen.main.bounds.width - VERTICAL_SPACING
        
        let x = VERTICAL_SPACING / 2
        
        let y = ( UIScreen.main.bounds.height / 2 ) - ( HEIGHT! / 2 )
        
        let frame = CGRect.init(x: x , y: y, width: width, height: HEIGHT!)
        
        self.frame = frame
        
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
//        self.bindKeyboardEvent()
        
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.layer.cornerRadius = (options?.cornerRadius!)!
            
            self.layer.masksToBounds = true
            
            self.handleBackground(bgType: (options?.backgroundType!)!)
            
            self.parentWindow?.addSubview(self)
            
            self.parentWindow.bringSubview(toFront: self)
            
        })
        
    }
    
    fileprivate func handleBackgroundTap(_ status: Bool){
        
        if status {
            
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            
            tapRecognizer.cancelsTouchesInView = false
            
            for subView in (UIApplication.topViewController()?.view.subviews)! {
                subView.isUserInteractionEnabled = false
            }
            
            UIApplication.topViewController()?.view.addGestureRecognizer(tapRecognizer)
            
        }
        else{
            
            UIApplication.topViewController()?.view.isUserInteractionEnabled = false
            
        }
        
    }
    
    
    @objc fileprivate func handleTap(_ sender: UITapGestureRecognizer) {
        
        self.dismiss()
        
    }
    
    
    func dismiss(){
        
        for subView in (UIApplication.topViewController()?.view.subviews)! {
            subView.isUserInteractionEnabled = true
        }
        
        if  (self.parentWindow?.subviews.count as! Int) >= 2
        {
            UIView.animate(withDuration: 0.25, animations: {
                
                self.parentWindow?.subviews.last?.removeFromSuperview()
                
                self.resetValues()
            })
        }
    }
    
    
    fileprivate func resetValues(){
        
        UIApplication.topViewController()?.view.alpha = 1.0
        
        UIApplication.topViewController()?.view.isUserInteractionEnabled = true
        
    }
    
    
    fileprivate  func bindKeyboardEvent(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
    }
    
    
    
    @objc func keyboardNotification(notification: NSNotification) {
        
        
        if let userInfo = notification.userInfo {
            
            let KEYBOARD_HEIGHT : CGFloat = 150.0
            
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let endFrameY = endFrame?.origin.y ?? 0
            
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            if endFrameY >= UIScreen.main.bounds.size.height {
                
                self.frame = CGRect.init(x: self.frame.origin.x , y: self.frame.origin.y + KEYBOARD_HEIGHT, width: self.frame.size.width, height: self.frame.size.height)
                
            }
                
                
            else {
                
                if let _ = Thread.callStackSymbols.map({ $0.contains("IQKeyboard") }).first(where: { $0 == true }){
                    
                    
                }
                else{
                    self.frame = CGRect.init(x: self.frame.origin.x , y: self.frame.origin.y - KEYBOARD_HEIGHT, width: self.frame.size.width, height: self.frame.size.height)
                    
                }
                
            }
            
            
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.layoutIfNeeded() },
                           completion: nil)
        }
        
        
        
    }
    
    
    fileprivate func handleBackground(bgType: Background){
        
        switch bgType {
            
        case .blur:
            
            break
            
        case .dark:
            break
            
        case .light:
            UIApplication.topViewController()?.view.alpha = 0.7
            break
        }
        
    }
}
