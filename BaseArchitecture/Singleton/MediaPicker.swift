
import UIKit
import MobileCoreServices
import Photos

enum MediaType:Int {
    case image = 0
    case video
    case all
}

enum SourceType:Int {
    case gallery = 0
    case camera
}

typealias onMediaSelection = (_ video: URL?, _ image: UIImage?) -> Void
typealias onCancel = () -> Void


class MediaPickerHandler: NSObject {
    fileprivate var instance:MediaPickerHandler?
    
    fileprivate var success:onMediaSelection?
    fileprivate var cancel:onCancel?
    
    fileprivate var controller:UIViewController!
    fileprivate var mediaType:MediaType = MediaType.all
    fileprivate var source:SourceType!
    
    fileprivate override init() { super.init() }
    
    init(controller:UIViewController, type: MediaType, source:SourceType, onSuccess:onMediaSelection?, onCancel:onCancel?) {
        super.init()
        
        instance = self
        
        self.controller = controller
        self.mediaType = type
        self.source = source
        
        self.success = onSuccess
        self.cancel = onCancel
    }
    
    deinit {
        instance = nil
    }
}

extension MediaPickerHandler {
    func show() {
        if source == .camera {
            PersmissionsHandler.requestCameraAccess(onCompletion: { (isGranted) in
                if isGranted {
                    self.openPicker(sourceType: .camera)
                }
            })
        } else {
            PersmissionsHandler.requestPhotosAccess(onCompletion: { (isGranted) in
                if isGranted {
                    self.openPicker(sourceType: .photoLibrary)
                }
            })
        }
    }
    
    fileprivate func openPicker (allowEditing: Bool = true, sourceType: UIImagePickerControllerSourceType) {
        DispatchQueue.main.async {
            let imagepicker = UIImagePickerController()
            imagepicker.allowsEditing = allowEditing
            imagepicker.sourceType = sourceType
            imagepicker.delegate = self
            imagepicker.mediaTypes = self.getMediaTypes()
            
            //imagepicker.navigationBar.barTintColor = Colors.dark
            //imagepicker.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Colors.systemWhite]
            
            self.instance?.controller.present(imagepicker, animated: true, completion: nil)
        }
    }
    
    fileprivate func getMediaTypes() -> [String] {
        var mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        
        if mediaType == .image {
            mediaTypes = [kUTTypeImage as String]
        } else if mediaType == .video {
            mediaTypes = [kUTTypeMovie as String]
        }
        
        return mediaTypes
    }
}

extension MediaPickerHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var video:URL?
        var image:UIImage?
        
        let mediaType = info[UIImagePickerControllerMediaType]
        if mediaType as! String != kUTTypeMovie as String {
            image = info[UIImagePickerControllerOriginalImage] as? UIImage
            /*
             if Device.is_iPhone {
             image = info[UIImagePickerControllerEditedImage] as? UIImage
             image = Utils.rotateImageToProperOrientation(imageSource: image!, maxResolution: image!.size.width)
             } else {
             image = info[UIImagePickerControllerOriginalImage] as? UIImage
             }
             */
        }
        else {
            video = info[UIImagePickerControllerMediaURL] as? URL
        }
        
        picker.dismiss(animated: true, completion: nil)
        instance?.success?(video, image)
        instance = nil
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        instance?.cancel?()
        instance = nil
    }
}
