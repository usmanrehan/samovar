import UIKit
import RealmSwift
import Alamofire



class AppStateManager: NSObject {
    //MARK:- Singleton
    static let sharedInstance = AppStateManager()
    
    static var reachability = NetworkReachabilityManager()
    var realm: Realm!
    var loggedInUser: UserLogin!
    
//    var isguest : Bool!
    
    
    //MARK:- Properties
    var isExternalLogin : Bool!
    func isUserLoggedIn() -> Bool {
        if (self.loggedInUser) != nil {
            if self.loggedInUser.isInvalidated {
                return false
            }
            return true
        }
        return false
    }
    var isGuestUser: Bool!{
        get{
            guard let _ = self.loggedInUser else {
                    return self.loggedInUser == nil ?  true : false
            }
            return false
        }
    }
    
    //MARK:- Class Methods
    override init() {
           super.init()
           if !(self.realm != nil){
               self.realm = try! Realm()
           }
           self.loggedInUser = self.realm.objects(UserLogin.self).first
       }
    
    
    //MARK:- Helper Methods
    func markUserLogout(){
        try! Global.APP_REALM?.write() {
            Global.APP_REALM?.deleteAll()
            self.loggedInUser = nil
        }
        self.changeRootViewController()
    }
    
    func changeRootViewController(){
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         appDelegate.setupUX()
    }
}
