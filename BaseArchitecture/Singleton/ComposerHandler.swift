
import UIKit
import MessageUI

// MARK:- Singleton
class ComposerHandler: NSObject {
    static let shared: ComposerHandler = {
        return ComposerHandler()
    }()
    
    private override init() { super.init() }
}

// MARK:- MFMailComposeViewControllerDelegate
extension ComposerHandler : MFMailComposeViewControllerDelegate{
    func sendEmail(to emails:[String], subject:String = "", body:String = "") {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(emails)
            mail.setSubject(subject)
            mail.setMessageBody(body, isHTML: false)
            
            UIApplication.topViewController()?.present(mail, animated: true)
        } else {
            Utility.showAlert(title: "Mail not configured!".localized(), message: "Please configure your Mail.".localized())
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

// MARK:- MFMessageComposeViewControllerDelegate
extension ComposerHandler : MFMessageComposeViewControllerDelegate{
    func sendMessage(numbers: [String], body:String = "") {
        let controller = MFMessageComposeViewController()
        controller.body = body
        controller.recipients = numbers
        controller.messageComposeDelegate = self
        UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        UIApplication.topViewController()?.dismiss(animated: true, completion: { })
    }
}

// MARK:- UIActivityViewController
extension ComposerHandler {
    func openActivity(message:String) {
        let controller = UIActivityViewController (activityItems: [message], applicationActivities: nil)
        UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
    }
}

// MARK:- Make phone call
extension ComposerHandler {
    func makeCall(to number:String) {
        guard let number = URL(string: "tel://" + number) else { return }
        UIApplication.shared.open(number, options: [:], completionHandler: nil)
    }
}
