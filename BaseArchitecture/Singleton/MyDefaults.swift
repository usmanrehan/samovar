
import Foundation

class MyDefaults : NSObject {
    
    static let shared = MyDefaults()
    private override init() { super.init() }
    
    private let kDeviceToken = "kDeviceToken"
    private let kSelectedLat = "kSelectedLat"
    private let kSelectedLng = "kSelectedLng"
    private let kSelectedAddress = "kSelectedAddress"
    private let kFirebaseToken = "kFirebaseToken"

    var deviceToken: Data? {
        set {
            UserDefaults.standard.set(newValue, forKey: kDeviceToken)
        }
        get {
            return UserDefaults.standard.data(forKey: kDeviceToken)
        }
    }
    
    var firebaseToken: String {
        set {
            UserDefaults.standard.set(newValue, forKey: kFirebaseToken)
        }
        get {
            return UserDefaults.standard.string(forKey: kFirebaseToken) ?? ""
        }
    }
    
    var tokenString: String {
        if let token = self.deviceToken {
            let tokenParts = token.map { data -> String in
                return String(format: "%02.2hhx", data)
            }
            let token = tokenParts.joined()
            return token
        }
        return ""
    }
    
    var selectedLat: Double {
        set {
            UserDefaults.standard.set(newValue, forKey: kSelectedLat)
        }
        get {
            return UserDefaults.standard.double(forKey: kSelectedLat)
        }
    }
    
    var selectedLng: Double {
        set {
            UserDefaults.standard.set(newValue, forKey: kSelectedLng)
        }
        get {
            return UserDefaults.standard.double(forKey: kSelectedLng)
        }
    }
    
    var selectedAddress: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: kSelectedAddress)
        }
        get {
            return UserDefaults.standard.string(forKey: kSelectedAddress)
        }
    }
}
