//
//  PermissionHandler.swift
//  Aaloo
//
//  Created by Bolwala on 6/30/18.
//  Copyright © 2018 Bolwala. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import Contacts
import CoreLocation
import MediaPlayer
import Photos

import UIKit
import Permission

class PersmissionsHandler: NSObject {
    private override init() { super.init() }
    
    static func requestCameraAccess(onCompletion: @escaping ((_ success: Bool) -> Void)) {
        let permission = Permission.camera
        permission.presentDeniedAlert = true
        permission.presentDisabledAlert = true
        
        /*
         let deniedAlert = permission.deniedAlert
         let disableAlert = permission.disabledAlert
         
         deniedAlert.title    = "Permisson Denied"
         deniedAlert.message  = "Please allow access to your camera"
         deniedAlert.cancel   = "Cancel"
         deniedAlert.settings = "Settings"
         
         disableAlert.title    = "Permisson Disable"
         disableAlert.message  = "Please allow access to your camera"
         disableAlert.cancel   = "Cancel"
         disableAlert.settings = "Settings"
         */
        
        permission.request { (status) in
            switch status {
            case .authorized:
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    onCompletion(true)
                } else {
                    onCompletion(false)
                }
                
            default:
                onCompletion(false)
            }
        }
    }
    
    static func requestPhotosAccess(onCompletion: @escaping ((_ success: Bool) -> Void)) {
        let permission = Permission.photos
        permission.presentDeniedAlert = true
        permission.presentDisabledAlert = true
        
        permission.request { (status) in
            switch status {
            case .authorized:
                onCompletion(true)
            default:
                onCompletion(false)
            }
        }
    }
    
    static func requestMediaLibraryAccess(onCompletion: @escaping ((_ success: Bool) -> Void)) {
        let permission = Permission.mediaLibrary
        permission.presentDeniedAlert = true
        permission.presentDisabledAlert = true
        
        permission.request { (status) in
            switch status {
            case .authorized:
                onCompletion(true)
            default:
                onCompletion(false)
            }
        }
    }
    
    static func requestMicrophoneAccess(onCompletion: @escaping ((_ success: Bool) -> Void)) {
        let permission = Permission.microphone
        permission.presentDeniedAlert = true
        permission.presentDisabledAlert = true
        
        permission.request { (status) in
            switch status {
            case .authorized:
                onCompletion(true)
            default:
                onCompletion(false)
            }
        }
    }
    
    static func requestLocationAccess(onCompletion: @escaping ((_ success: Bool) -> Void)) {
        let permission = Permission.locationWhenInUse
        permission.presentDeniedAlert = true
        permission.presentDisabledAlert = true
        
        permission.request { (status) in
            switch status {
            case .authorized:
                onCompletion(true)
            default:
                onCompletion(false)
            }
        }
    }
}
