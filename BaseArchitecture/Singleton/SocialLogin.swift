
import UIKit
import FacebookLogin
import FacebookCore
//import GoogleSignIn

class SocialLogin: NSObject {   //, GIDSignInDelegate, GIDSignInUIDelegate {

    static let shared = SocialLogin()
    private override init() {}

    private var googleCompletion: ((_ status: SocialLoginStatus, _ message: String, _ data: UserSocialData?) -> Void)?
    private var presentedController: UIViewController?

    func loginWithFacebook(_ fromController: UIViewController, completion: @escaping (_ status: SocialLoginStatus, _ message: String, _ data: UserSocialData?) -> Void) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [ReadPermission.publicProfile, ReadPermission.email], viewController: fromController) { (result) in
            switch result {
            case .failed(let error):
                completion(SocialLoginStatus.error, error.localizedDescription, nil)
            case .cancelled:
                completion(SocialLoginStatus.canceled, "User cancelled the process", nil)
            case .success( _, _, let token):

                self.getFacebookProfile(token: token, completion: completion)
            }
        }
    }

    private func getFacebookProfile(token: AccessToken, completion: @escaping (_ status: SocialLoginStatus, _ message: String, _ data: UserSocialData?) -> Void) {
        GraphRequest(graphPath: "/me", parameters: ["fields": "email, name"], accessToken: token, httpMethod: GraphRequestHTTPMethod.GET, apiVersion: GraphAPIVersion.defaultVersion).start { (httpURLResponse, requestResult) in
            switch requestResult {
            case .failed(let error):
                completion(SocialLoginStatus.error, error.localizedDescription, nil)
                break
            case .success(let graphResponse):
                if let responseDictionary = graphResponse.dictionaryValue {
                    completion(SocialLoginStatus.success, "", UserSocialData(fbData: responseDictionary, token: ""))
                } else {
                    completion(SocialLoginStatus.error, "Response object is nil", nil)
                }
            }
        }
    }

}


