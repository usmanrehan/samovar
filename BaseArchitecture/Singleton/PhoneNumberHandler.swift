
import Foundation
import PhoneNumberKit

class PhoneNumberHandler {
    
    private init() { }
    static let shared: PhoneNumberHandler = {
        return PhoneNumberHandler()
    }()
    
    lazy var phoneNumberKit: PhoneNumberKit = {
        return PhoneNumberKit()
    }()
    
    func validateNumber(_ number: String)-> Bool{
        
        do {
            _ = try phoneNumberKit.parse(number)
            return true
        }
        catch {
            return false
        }
    }
    
    func getFormattedNumber(_ number: String)-> String? {
        do {
            let phoneNumber = try phoneNumberKit.parse(number)
            return phoneNumberKit.format(phoneNumber, toType: .international)
        }
        catch {
            return nil
        }
    }
}
